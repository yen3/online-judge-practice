#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
using namespace std;

void BFS(vector<int>& u, const vector<vector<int> >& m);
void bfs(int index, vector<int>& u, vector<int>& v, const vector<vector<int> >& m);
    
void BFS(vector<int>& u, const vector<vector<int> >& m){
    vector<int> v(m.size(),0);
    for(unsigned int i=0;i<m.size();i++){
        if(v[i]==0){
            bfs(i, u, v, m);	
	}
    }
}

void bfs(int index, vector<int>& u, vector<int>& v, const vector<vector<int> >& m){
    vector<int> p;
    p.push_back(index);
    v[index]=1;

    for(unsigned int i=0;i!=p.size();i++){
        for(unsigned int j=0;j<m.size();j++){
	    if(m[i][j]==1 && v[j]==0){
	        p.push_back(j);
		v[j]=1;
	    }
	}
    }
    copy(p.begin(), p.end(), back_inserter(u));
}

void input_graph(vector<vector<int> >& m){
    int e,v;
    cin >> e >> v;
    m = vector<vector<int> >(v, vector<int>(v, 0));
    int i_m,i_n;
    for(unsigned int i=0;i<e;i++){
        cin >> i_m >> i_n;
	m[i_m-1][i_n-1]=1;
    }
}

int main(){
    vector<vector<int> > m;
    vector<int> u;

    input_graph(m);
    BFS(u, m);

    copy(u.begin(), u.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
}
