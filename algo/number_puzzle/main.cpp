#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <utility>

// C++ Boost http://www.boost.org
#include <boost/shared_ptr.hpp>
#include <boost/assign/std/vector.hpp>

// namespace NumberPuzzle
#include "utility.h"
#include "puzzle.h"
#include "puzzle_step_list.h"

namespace NumberPuzzle{

std::pair<bool, std::string>
NumberPuzzle(CalculatePredictCost& calPredictCost,
             MapLocation& mapLocation,
             const std::string& init_state,
             const std::string& final_state,
             std::vector<int>& path,
             bool time_control){
    
    // Initial Step List 
    PuzzleStepList step_list(PuzzleStep_sp(new PuzzleStep(init_state, MapLocation::NONE, 0, calPredictCost.predict(init_state))));
    for(int times;;times++){
        /* Produce next step list */
        PuzzleStep_sp current_step = step_list.getNextElement();

        //Test Code
        if(times%10000==0){
            std::cout << times << std::endl;
            std::cout << *current_step << std::endl;
        }


        /* return the answer or the tempoary answer */
        if(current_step->isFinalStep(final_state)){
            //Test Code
            std::cout << times << std::endl;
            std::cout << *current_step << std::endl;
            std::cout << step_list.get_total_size() << std::endl;
 
            current_step->getPath(path);
            return std::make_pair(true, std::string());
        }
        if(time_control && times==45000){
            //Test Code
            std::cout << "test " << times << std::endl;
            std::cout << *current_step << std::endl;
 
            current_step->getPath(path);
            return std::make_pair(false, std::string(current_step->getState()));
        }

        /* add to Open List */
        boost::shared_ptr<std::vector<PuzzleStep_sp> > next_step_list =
                  produceNextPuzzleSteps(current_step, calPredictCost, mapLocation);
        for(std::size_t i=0; i< next_step_list->size(); ++i){
            if(step_list.addStep((*next_step_list)[i])){
                current_step -> addChildStep((*next_step_list)[i]);
            }
        }
    }
}


/** Interface
 *
 * */
boost::shared_ptr<std::vector<int> >
NumberPuzzle(const std::size_t puzzle_size,
             const std::string& init_state,
             const std::string& final_state,
             bool real_time_a=false){
    // set enviornment
    CalculatePredictCost calPredictCost(puzzle_size, final_state);   // Initial Calculater
    MapLocation mapLocation(puzzle_size, puzzle_size);               // Initial MapLocation
    
    // set answewr
    boost::shared_ptr<std::vector<int> >  path(new std::vector<int>);
    
    // start
    std::string run_state(init_state);
    for(std::pair<bool, std::string> result(false, init_state);
        !result.first;
        result = NumberPuzzle(calPredictCost, mapLocation, result.second, final_state, *path, real_time_a)){
        
        std::cout << "test2" << std::endl;    
    }
    
    return path;
}


/**
 *   test for the answer is correct 
 *
 * */
bool test_answers(unsigned int size, const std::vector<int>& path, 
                  const std::string& init_state, const std::string& final_state, MapLocation& map_loc){
    std::string path_result(init_state);
    int step = path_result.find(static_cast<char>(0));
    for(std::size_t i=0, step=path_result.find(static_cast<char>(0)), next_step=-1;
        i<path.size();
        i++){
        next_step = map_loc(step, path[i]);
        std::swap(path_result[step], path_result[next_step]);
        step = next_step;
       
        for(std::size_t i=0; i<path_result.length();i++){
            if(i!=0 && i%size==0) std::cout << std::endl;
            std::cout << std::setw(2) << static_cast<int>(path_result[i]) << " ";
        }
        std::cout << std::endl << std::endl;
    }
    return path_result == final_state;
}

}

std::string generate_state(const std::vector<int>& state_array){
    std::string s("");
    for(std::size_t i=0; i<state_array.size(); ++i){
        s += static_cast<unsigned char>(state_array[i]);
    }
    return s;
}

int main(int argc, char** argv){
    using namespace std;
    using namespace NumberPuzzle;
    using namespace boost::assign;

    int puzzle_size = (argc>1)? std::atoi(argv[1]):3;
    vector<int> init;
    vector<int> final;
    if(puzzle_size == 4){
        init += 1, 0, 3, 4, 6, 2, 11, 10, 5, 8, 7, 9, 14 ,12 ,15, 13;
        //init += 0, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1;
        //init += 4, 13, 6, 3, 7, 9, 0, 10, 12, 14, 5, 15, 2, 8, 11, 1;
        final += 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0;
    }
    else if(puzzle_size == 3){
        init +=  3, 1, 2, 0, 4, 7, 8, 5, 6;
        final += 1, 2, 3, 4, 5, 6, 7, 8, 0; 
    }
    else if(puzzle_size == 5){
        init += 1,  0,  3,  4,  6,  2, 11, 10,  5,  8,  7,  9, 14, 12, 15, 13, 18, 16, 17, 19, 20, 24, 23, 22, 21;
        final += 1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,  0; 
    }
    else{
        puzzle_size = 4 ;
        init += 1, 0, 3, 4, 6, 2, 11, 10, 5, 8, 7, 9, 14 ,12 ,15, 13;
        final += 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0;
    }
    
    std::string init_state = generate_state(init);
    std::string final_state = generate_state(final);

    // Print Test Code
    //std::cout << "Initial State: ";
    //for(std::size_t i=0; i<init_state.length(); ++i){
        //std::cout << std::setw(2) << static_cast<unsigned int>(init_state[i]) << " ";
    //}
    //std::cout << std::endl;
    //std::cout << "Final State:   ";
    //for(std::size_t i=0; i<final_state.length(); ++i){
        //std::cout << std::setw(2) << static_cast<unsigned int>(final_state[i]) << " ";
    //}
    //std::cout << std::endl << std::endl;
    
    // function start
    boost::shared_ptr<std::vector<int> > path =
             NumberPuzzle::NumberPuzzle(puzzle_size,
                                        init_state,
                                        final_state,
                                        true/*(puzzle_size==5)*/);

    // print result
    for(std::size_t i=0; i<path->size(); ++i){
        std::cout << std::setw(2) << (*path)[i];
    }
    std::cout << std::endl;
   
    // Test for Answers
    //NumberPuzzle::MapLocation map_loc(puzzle_size, puzzle_size);
    //if(NumberPuzzle::test_answers(puzzle_size, *path, init_state, final_state, map_loc)==true) std::cout << "Testing Success for the answer!" << std::endl;
    //else std::cout << "Wrong Answer!" << std::endl;
}
