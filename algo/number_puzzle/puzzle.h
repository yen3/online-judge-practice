#ifndef _NUMPUZZLE_PUZZLE_
#define _NUMPUZZLE_PUZZLE_

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include <iostream>
#include "utility.h"

namespace NumberPuzzle{

class PuzzleStep;
class CalculatePredictCost;

typedef boost::shared_ptr<PuzzleStep> PuzzleStep_sp;

bool compareCostPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2);
bool compareStateEqualPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2);
std::ostream& operator<<(std::ostream& os, PuzzleStep& step);
//void out_node_file(PuzzleStep_sp step);

boost::shared_ptr<std::vector<PuzzleStep_sp > >
produceNextPuzzleSteps(PuzzleStep_sp step,
                       CalculatePredictCost& cal_pred,
                       MapLocation& map_loc);
/** 
 * Evaluate each step's predict cost.
 *  
 * We use Heruistic Algorithm in text books
 * 
 * */
class CalculatePredictCost{
public:
    CalculatePredictCost(){};
    CalculatePredictCost(unsigned int s, const std::string& final_state);
    virtual int predict(const std::string& state);
protected:
    unsigned int size;
    std::vector<std::pair<unsigned int, unsigned int> > loc;
};

class AdvCalPredictCost: public CalculatePredictCost{
public:
    AdvCalPredictCost(){};
    AdvCalPredictCost(unsigned int s, const std::string& final_state);
    virtual int predict(const std::string& state);
private:
    std::vector<int> check_extra;
};

class PuzzleStep{
    friend class PuzzleStepList;
    friend bool compareStateEqualPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2);
    friend bool compareCostPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2);
    friend std::ostream& operator<<(std::ostream& os, PuzzleStep& step);
    friend boost::shared_ptr<std::vector<PuzzleStep_sp > > produceNextPuzzleSteps(PuzzleStep_sp step, CalculatePredictCost& cal_pred, MapLocation& map_loc);
public:
    PuzzleStep(const std::string& step, int dir, unsigned int rc, unsigned int pc,
                                   PuzzleStep_sp p=PuzzleStep_sp()):
        state(step), direction(dir), real_cost(rc), predict_cost(pc), total_cost(rc+pc),parent(p){
        }

    inline bool equalState(const PuzzleStep& step){ return state == step.state; }
    inline const std::string& getState(){ return state; }
    inline const int getTotalCost(){ return total_cost; }
    inline const int getRealCost(){ return real_cost;}
    inline const int getPredictCost(){ return predict_cost;}
    inline const int getDirection(){ return direction; }
    inline bool isFinalStep(const std::string& final_step){   return state == final_step; }

    inline void addChildStep(PuzzleStep_sp step){  child.push_back(step);  }
    //boost::shared_ptr<std::vector<int> > getPath(); 
    void getPath(std::vector<int>& path); 
private:
    std::string state;
    int direction;
    unsigned int real_cost;
    unsigned int predict_cost;
    unsigned int total_cost;

    PuzzleStep_sp parent;
    std::vector<PuzzleStep_sp > child; 
};

}

#endif
