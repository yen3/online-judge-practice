#include "puzzle_step_list.h"
#include "puzzle.h"
#include <vector>
#include <boost/shared_ptr.hpp>
#include <algorithm>
#include <functional>
#include <string>
#include <map>


namespace NumberPuzzle{

void PuzzleStepList::revaluate_real_cost(PuzzleStep_sp step, int rc){
    step->real_cost = rc;
    step->total_cost = step->real_cost + step->predict_cost;
    if(!step->child.empty()){
        for(std::size_t i=0;i<step->child.size();++i){
            revaluate_real_cost(step->child[i], step->real_cost+1);
        } 
    }
}

// -1 is not founded
// 0 is founded and not better
// 1 is founded and better
int PuzzleStepList::check_list(PuzzleStep_sp step){
    if(step_list.count(step->state) == 0) return -1;   // Not Find. 
    
    // Find and Compare
    if(step->real_cost < step_list[step->state].step->real_cost){
        // Remove worse node from parent
        std::vector<PuzzleStep_sp>& list = (step_list[step->state].isOpenList())? open_list: close_list;
        int i = std::distance(list.begin(),
                              std::find_if(list.begin(),
                                           list.end(),
                                           std::bind1st(std::ptr_fun(compareStateEqualPuzzleStep_sp), step)));
        
        list[i]->parent->child.erase(find(list[i]->parent->child.begin(),
                                          list[i]->parent->child.end(),
                                          list[i]));
        if(!list[i]->child.empty()){
            std::copy(list[i]->child.begin(), list[i]->child.end(), step->child.begin());
            for(std::size_t i=0;i<step->child.size(); ++i){
                step->child[i]->parent = step; 
            }
            revaluate_real_cost(step, step->real_cost+1);
        }
        
        // Replace to New Step
        step_list[step->state].step = step;
        list[i] = step;
    
        return 1; 
    }
    return 0;
}

bool PuzzleStepList::addStep(PuzzleStep_sp step){
    int check_result = check_list(step);
    if(check_result == -1){
        open_list.push_back(step);
        std::push_heap(open_list.begin(), open_list.end(), compareCostPuzzleStep_sp);

        step_list.insert(std::make_pair(step->state, PuzzleStepState(step)));
        return true;
    }
    else if(check_result == 1){
        std::make_heap(open_list.begin(), open_list.end(), compareCostPuzzleStep_sp);
        return true;
    }
    else
        return false;
}

PuzzleStep_sp PuzzleStepList::getNextElement(){
    if(!open_list.empty()){
        // Pop from Open List
        std::pop_heap(open_list.begin(), open_list.end(), compareCostPuzzleStep_sp);
        PuzzleStep_sp next_element = open_list[open_list.size()-1];
        open_list.pop_back();
       
        // Push to Close List
        close_list.push_back(next_element);
        step_list[next_element->state].setCloseList();
        
        return next_element;
    }
    return PuzzleStep_sp();
}


}

