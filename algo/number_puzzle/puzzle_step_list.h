#ifndef _NUMPUZZLE_PUZZLE_STEP_LIST
#define _NUMPUZZLE_PUZZLE_STEP_LIST

#include "puzzle.h"
#include <vector>
#include <map>
#include <string>

namespace NumberPuzzle{


// for search
class PuzzleStepState{
public:
    PuzzleStepState(){};
    PuzzleStepState(const PuzzleStepState& pss):step(pss.step), list(pss.list){}
    PuzzleStepState(PuzzleStep_sp s, bool l=true):step(s), list(l){}
    void setCloseList(){ list = false; }
    bool isOpenList(){ return list; }

    PuzzleStep_sp step;
    bool list;   // true is open_list, false is close_list
};

class PuzzleStepList{
public:
    PuzzleStepList(PuzzleStep_sp step):open_list(1, step){
        step_list.insert(std::make_pair(step->state, PuzzleStepState(step)));
    }
    
    bool addStep(PuzzleStep_sp step);
    std::size_t get_total_size(){ return open_list.size() + close_list.size(); }
    PuzzleStep_sp getNextElement();
private:
    int check_list(PuzzleStep_sp step);
    void revaluate_real_cost(PuzzleStep_sp step, int rc);
    std::vector<PuzzleStep_sp> open_list;
    std::vector<PuzzleStep_sp> close_list;

    std::map<std::string, PuzzleStepState> step_list;
};

}
#endif

