#include "utility.h"
#include <cstdlib>

namespace NumberPuzzle{

int MapLocation::operator()(const int& location, const int& direction){
    int x = location % w;
    int y = location / w;
    
    if(direction == UP)           return (static_cast<int>(w*(y-1)+x)>=0)? w*(y-1)+x: NONE;        
    else if(direction == DOWN)    return (static_cast<int>(w*(y+1)+x)<w*h)? w*(y+1)+x: NONE;
    else if(direction == LEFT)    return (static_cast<int>(x-1)>=0)? location-1: NONE;
    else if(direction == RIGHT)   return (static_cast<int>(x+1)<w)? location+1: NONE;
    else return NONE;
}

int MapLocation::reverse_direction(const int& direction){
    if(direction == UP)           return DOWN;        
    else if(direction == DOWN)    return UP; 
    else if(direction == LEFT)    return RIGHT; 
    else if(direction == RIGHT)   return LEFT; 
    else return NONE;
}


}
