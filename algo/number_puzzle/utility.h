#ifndef _NUMPUZZLE_UTILITY_
#define _NUMPUZZLE_UTILITY_
#include <cstdlib>

namespace NumberPuzzle{
class MapLocation{
public:
    MapLocation(){};
    MapLocation(int weight, int height):w(weight), h(height){};
    int operator()(const int& cor, const int& direction);
    static int reverse_direction(const int& direction);

    static const int NONE = -1;
    static const int UP = 1;
    static const int DOWN = 2;
    static const int LEFT = 3;
    static const int RIGHT = 4;
private:
    std::size_t w;  /* x: width  */
    std::size_t h;  /* y: height */
};

}

#endif
