#include "puzzle.h"
#include "utility.h"

#include <boost/shared_ptr.hpp>
#include <boost/multi_array.hpp>
#include <boost/array.hpp>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <utility>

namespace NumberPuzzle{

/*
unsigned int generate_node_num(){
    static unsigned int n = 0;
    return n++;
}
*/

std::ostream& operator<<(std::ostream& os, PuzzleStep& step){
//    os << "Node: " << step.node_num << std::endl;
    os << "State: " << std::endl;
    int line_size = sqrt(step.state.size());
    for(std::size_t i=0;i<step.state.size();++i){
        if(i%line_size==0 && i!=0) os << std::endl;
        os << std::setw(2) << static_cast<unsigned int>(step.state[i]) << " ";
    }
    os << std::endl;
    
    if(step.direction == MapLocation::NONE)   os << "Dir: NONE" << std::endl;   
    else if(step.direction == MapLocation::UP)   os << "Dir: UP" << std::endl;   
    else if(step.direction == MapLocation::DOWN)   os << "Dir: DOWN" << std::endl;   
    else if(step.direction == MapLocation::LEFT)   os << "Dir: LEFT" << std::endl;   
    else if(step.direction == MapLocation::RIGHT)   os << "Dir: RIGHT" << std::endl;   
    
    os << "Real Cost: " << std::setw(4) << step.real_cost << " , Predict Cost: " << std::setw(4) << step.predict_cost << ", Total Cost: " << std::setw(4) << step.real_cost + step.predict_cost << std::endl;
    //if(step.parent!=NULL) os << "Parent: " << step.parent->node_num << std::endl;
    os << "/--end this step--/" << std::endl;
    return os;
}

/*
void out_node_file(PuzzleStep_sp step){
    static std::ofstream outfile("test_node.out");
    outfile << *step << std::endl;
}
*/

bool compareCostPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2){
    if(u1->getTotalCost() == u2->getTotalCost()){
        if(u1->predict_cost == u2->predict_cost)   return u1->real_cost > u2->real_cost;
        else    return u1->predict_cost > u2->predict_cost;
    }
    else return u1->getTotalCost() > u2->getTotalCost();
}

bool compareStateEqualPuzzleStep_sp(PuzzleStep_sp u1, PuzzleStep_sp u2){
    return u1->state == u2->state;
} 

boost::shared_ptr<std::vector<PuzzleStep_sp > >
produceNextPuzzleSteps(PuzzleStep_sp step, CalculatePredictCost& cal_pred, MapLocation& map_loc){
    int move_target = step -> state.find(static_cast<char>(0));
    int reverse_direction = MapLocation::reverse_direction(step -> direction);
    
    boost::shared_ptr<std::vector<PuzzleStep_sp > > next_list(new std::vector<PuzzleStep_sp >);
    for(std::size_t i=1; i<=4; ++i){
        if(i==reverse_direction) continue;
        int next_step = map_loc(move_target, i);

        if(!(next_step == MapLocation::NONE)){
            std::string next_state(step->state);
            std::swap(next_state[move_target], next_state[next_step]);
            next_list -> push_back(PuzzleStep_sp(new PuzzleStep(next_state, i, step->real_cost+1, cal_pred.predict(next_state), step)));
        }
    }
    return next_list;
}
    
CalculatePredictCost::CalculatePredictCost(unsigned int s, const std::string& final_state):
    size(s),
    loc(std::vector<std::pair<unsigned int, unsigned int > >(final_state.length(), std::make_pair(0, 0))){
    
    for(std::size_t i=0;i<final_state.length();++i){
        loc[final_state[i]].first = i%size;  /* fst: x */ 
        loc[final_state[i]].second = i/size;  /* snd: y */
    } 
}

int CalculatePredictCost::predict(const std::string& state){
    int result=0;
    for(std::size_t i=0; i<state.length(); ++i){
        if(state[i]!=0) result += std::labs(loc[state[i]].first - (i%size)) + std::labs(loc[state[i]].second - (i/size)); 
    }
    return result;
}

AdvCalPredictCost::AdvCalPredictCost(unsigned int s, const std::string& final_state):CalculatePredictCost(s, final_state){
    for(int i=0, n=s; i<s-1; n+=s-1, i++)    check_extra.push_back(s);
}


int AdvCalPredictCost::predict(const std::string& state){
    int result=0;
    static boost::multi_array<int, 2> state_loc(boost::extents[loc.size()][2]);
    for(std::size_t i=0; i<state.length(); ++i){
        if(state[i]!=0){
            state_loc[state[i]][0]= i%size;
            state_loc[state[i]][1]= i/size;
        } 
    }

    // h2
    for(std::size_t i=1;i<loc.size();i++){
        result += std::labs(loc[i].first-state_loc[i][0]) + std::labs(loc[i].second-state_loc[i][1]);
    }
    
    // k(node)
    for(std::size_t i=0; i<check_extra.size(); i++){
        for(std::size_t j=i+1; j<check_extra.size(); j++){
             if((std::labs(state_loc[check_extra[i]][0]-state_loc[check_extra[j]][0]) +
                std::labs(state_loc[check_extra[i]][1]-state_loc[check_extra[j]][1]))==1){
                 std::cout << "k" << std::endl;
                 result += 2; 
             }
        }
    }
    
    return result;
}

void PuzzleStep::getPath(std::vector<int>& path){
    path.insert(path.end(), real_cost, 0);
    PuzzleStep *step = this;
    int *temp_path = new int[real_cost];
    for(int i=path.size()-1; i>=path.size()-real_cost && step!=NULL && step->direction!=-1; i--,step=(step->parent).get()){
        path[i] = step->direction; 
    }

}

}

