#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
using namespace std;

bool comp(const pair<int, int>& x, const pair<int, int>& y){
    return x<y;
}

void produce_as(vector<pair<int, int> >& u){
    /* sort the activity */
    sort(u.begin(), u.end(), comp);

    /* delete cover other acitivly */
    /*
    vector<bool> c(u.size(), true);
    for(unsigned int i=0;i<u.size();i++){
	for(unsigned int j=0;j<u.size();j++){
	    if(c[j] && i!=j){
	        if(u[i].first<=u[j].first && u[i].second>=u[j].second){
		    c[i] = false;
		    break;
		}    
	    }
	}
    }
   */
    for(vector<pair<int, int> >::iterator iter=u.begin();iter!=u.end();++iter){
        for(vector<pair<int, int> >::iterator comp_iter=u.begin();comp_iter!=u.end();++comp_iter){
            if(iter!=comp_iter){
                if(iter->first <= comp_iter->first && iter->second>=comp_iter->second){
                    u.erase(iter);
                    iter--;
                    break;
                }
            }
        }
    }

    for(unsigned int i=0;i<u.size();i++){
	cout << u[i].first << " "<< u[i].second << endl;
    }
}

void activity_selection(vector<pair<int, int> >& s, const vector<pair<int, int> >& u){
    s = vector<pair<int, int> >(u.size()); // (s(i), s(p_i))

    s[0] = make_pair(1,0);
    for(unsigned int i=1;i<s.size();i++){
        for(int j=i-1;j>=0;j--){
            if(u[j].second<u[i].first){
                s[i] = make_pair(1+s[j].first, j);
                break;
            }
        }       
    }
    cout << endl;
    for(unsigned int i=0;i<s.size();i++){
        cout << s[i].first << " " << s[i].second << endl;
    }
}

void print_result(const vector<pair<int, int> >& s, const vector<pair<int, int> >& u){
    vector<int> index;
    index.push_back(s.size()-1);
    for(int i=s.size()-1;i>0;){
        index.push_back(s[i].second);
        i = s[i].second;
    }

    cout << endl;
    for(unsigned int i=0;i<index.size();i++){
        cout << "( " << u[i].first << ", " << u[i].second << ")" << endl;
    }
}

void input_data(vector<pair<int, int> >& u){
    int size;
    cin >> size;

    int m, n;
    for(unsigned int i=0;i<size;i++){
        cin >> m >> n;
        u.push_back(make_pair(m,n));
    }
}


int main(){
    vector<pair<int, int> > u, s;
    input_data(u);

    produce_as(u);
    activity_selection(s,u);
    print_result(s, u);

}
