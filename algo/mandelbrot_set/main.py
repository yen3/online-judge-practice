from __future__ import print_function
from multiprocessing import Pool
import matplotlib.pyplot as plt

MAX_ITERATION_STEPS = 3000


def p(c):
    z = 0
    for i in xrange(0, MAX_ITERATION_STEPS):
        z = z ** 2 + c
        if(abs(z) > 2):
            return False

    return True


def float_list_dist(begin, end, size):
    dist = (end - begin)/size
    return [begin + i * dist for i in range(size)]


def generate_list():
    REAL_BEGIN = -2.0
    REAL_END = 0.5
    IMG_BEGIN = 0.0
    IMG_END = 1.0

    REAL_POINT_SIZE = 2000
    IMG_POINT_SIZE = 2000

    return [complex(r, i) for r in float_list_dist(REAL_BEGIN, REAL_END, REAL_POINT_SIZE)
            for i in float_list_dist(IMG_BEGIN, IMG_END, IMG_POINT_SIZE)]


def main():
    proc = Pool(processes=4)
    point_list = generate_list()
    check_list = proc.map(p, point_list)

    plot_point_list = [point for point, c in zip(point_list, check_list) if c]
    plt.plot([point.real for point in plot_point_list], [point.imag for point in plot_point_list], 'k,')
    plt.plot([point.real for point in plot_point_list], [-point.imag for point in plot_point_list], 'k,')
    plt.axis('equal')
    plt.show()



if __name__ == '__main__':
    main()
