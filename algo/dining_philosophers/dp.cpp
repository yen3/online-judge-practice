/* copy-and-paste from https://doc.rust-lang.org/book/dining-philosophers.html */
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <memory>
#include <chrono>

typedef std::vector<std::mutex> Table;

#define __THREAD_SAFE_PRINT__

#if defined(__THREAD_SAFE_PRINT__)
// the print_mutex is optional, just for the readable output 
std::recursive_mutex print_mutex;

#define SAFE_PRINT(s)         \
    do{                       \
        print_mutex.lock();   \
        s;                    \
        print_mutex.unlock(); \
    }while(0)

#else  /* __THREAD_SAFE_PRINT__ */
#define SAFE_PRINT(s)        s;

#endif /* __THREAD_SAFE_PRINT__ */

class Philosopher{
public:
    Philosopher(std::size_t p,
                std::size_t left,
                std::size_t right,
                std::shared_ptr<Table> table):
        p_(p), left_(left), right_(right), table_(table) {
    }

    void eat() const{
        std::lock_guard<std::mutex> left_fork(table_->at(left_));
        std::lock_guard<std::mutex> right_fork(table_->at(right_));

        SAFE_PRINT(std::cout << p_ << ": is eating." <<  std::endl);

        std::this_thread::sleep_for(std::chrono::seconds(1));

        SAFE_PRINT(std::cout << p_ << ": is done." <<  std::endl);
    }

private:
    std::size_t p_;
    std::size_t left_;
    std::size_t right_;
    std::shared_ptr<Table> table_;
};


void diningPhilosophers(const std::size_t p_size = 5) {
    // create new table
    std::shared_ptr<Table> table(new Table(p_size));

    // create philosophers
    std::vector<std::shared_ptr<Philosopher>> ps;
    for(std::size_t i=0, left=0, right=0; i< p_size; ++i){
        left = (i != p_size - 1)? i: 0;
        right = (i != p_size - 1)? i+1: i;

        ps.push_back(std::shared_ptr<Philosopher>(
                    new Philosopher(i, left, right, table)));
    }

    // start to eat 
    std::vector<std::thread> threads;
    for(auto& p : ps){
        threads.push_back(
                std::thread([](std::shared_ptr<Philosopher> p){ p-> eat(); },
                p));
    }

    // waiting for all philosophers finished 
    for_each(threads.begin(), threads.end(),
            [](std::thread& t){ t.join(); });
}


int main(int argc, char const* argv[]) {
    diningPhilosophers(5);
    std::cout << "Finished" << std::endl;

    diningPhilosophers(10);
    std::cout << "Finished" << std::endl;
    return 0;
}
