#include <iostream>
#include <utility>
#include <vector>
#include <cassert>

void TopHeapify(std::vector<int>& vs, uint32_t index, uint32_t size) {
  while (1) {
    uint32_t child_index = index * 2 + 1;
    uint32_t child_max_index = child_index;

    if (child_index >= size) {
      break;
    } else if (child_index < size - 1) {
      child_max_index = (vs[child_index] > vs[child_index + 1])
                            ? (child_index)
                            : (child_index + 1);
    }

    if (vs[index] < vs[child_max_index]) {
      std::swap(vs[index], vs[child_max_index]);
      index = child_max_index;
    } else {
      break;
    }
  }
}

void HeapifyArray(std::vector<int>& vs) {
  for (int i = vs.size() / 2; i >= 0; --i) {
    TopHeapify(vs, i, vs.size());
  }
}

void HeapSort(std::vector<int>& vs) {
  HeapifyArray(vs);

  for (int i = vs.size() - 1; i >= 0; --i) {
    std::swap(vs[0], vs[i]);
    TopHeapify(vs, 0, i);
  }
}

int main() {
  std::vector<int> vs{2, 5, 1, 3, 4};
  HeapSort(vs);

  assert(vs[0] == 1);
  assert(vs[1] == 2);
  assert(vs[2] == 3);
  assert(vs[3] == 4);
  assert(vs[4] == 5);
}
