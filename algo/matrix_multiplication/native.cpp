#include <iostream>
#include <cstdlib>
#include <vector>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <utility>
using namespace std;

/* Assume u, v are 2^n square matrix */
/* r = u * v */
void mult(pair<int,int> lr, pair<int,int> lu, pair<int,int> lv, int size,
          vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v){
    
    static const int addr[8][2] = {{0,0}, {0,0}, {0,1}, {0,1}, {1,0}, {1,0}, {1,1}, {1,1}};
    static const int addu[8][2] = {{0,0}, {0,1}, {0,0}, {0,1}, {1,0}, {1,1}, {1,0}, {1,1}};
    static const int addv[8][2] = {{0,0}, {1,0}, {0,1}, {1,1}, {0,0}, {1,0}, {0,1}, {1,1}};

    if(size>2){
        for(unsigned int i=0;i<8;i++){
            cout << "r0 " <<(size>>1) * addr[i][0] << endl;
            mult(make_pair(lr.first+((size >> 1)*addr[i][0]), lr.second+((size >> 1)*addr[i][1])),
                 make_pair(lu.first+((size >> 1)*addu[i][0]), lu.second+((size >> 1)*addu[i][1])),
                 make_pair(lv.first+((size >> 1)*addv[i][0]), lv.second+((size >> 1)*addv[i][1])),
                 size/2,
                 r,
                 u,
                 v);
        }
    }
    else{
        
        for(unsigned int i=0;i<8;i++){
            r[lr.first + addr[i][0]][lr.second + addr[i][1]] += 
                       u[lu.first + addu[i][0]][lu.second + addu[i][1]] * 
                       v[lv.first + addv[i][0]][lv.second + addv[i][1]];
        }
    }
}

void mulitplication(vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v){
    mult(make_pair(0,0), make_pair(0,0), make_pair(0,0), r.size(), r, u, v);
}

void inputMatrix(vector<vector<int> >& m){
    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<m[i].size();j++){
            cin >> m[i][j];
        }
    }    
}

void makeRandMatrix(vector<vector<int> >& m){
    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<m[i].size();j++){
            m[i][j] = rand()%5+1;
        }
    }    
}

void printMatrix(vector<vector<int> >& m){
    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<m[i].size();j++){
            cout << setw(4) <<m[i][j] << " ";
        }
        cout << endl;
    }    
}

/*************************************************/

class Matrix{
    friend ostream& operator<<(ostream& out, const Matrix& m);
    public:
        Matrix(){};
        Matrix(int x, int y);
        Matrix(vector<vector<int> > u):m(u){};
 
        Matrix operator+(const Matrix&);
        Matrix operator-(const Matrix&);
        Matrix operator*(const Matrix&);
 
        void assignMatrix(const vector<vector<int> >& u);
    private:
        vector<vector<int> > mult(const vector<vector<int> >&);
    
        vector<vector<int> > m;
};

Matrix::Matrix(int x, int y=0):m(x,vector<int>((y==0)?x:y,0)){}

Matrix Matrix::operator+(const Matrix& rhs){
    if(m.size()== rhs.m.size() && m[0].size() == rhs.m[0].size()){
        Matrix rel(m.size(), m[0].size());
        for(unsigned int i=0;i<m.size();++i){
            for(unsigned int j=0;j<m[0].size();j++){    
                rel.m[i][j] = m[i][j] + rhs.m[i][j];
            }
        }
        return rel;
    }
    else exit(1);
}

Matrix Matrix::operator-(const Matrix& rhs){
    if(m.size()== rhs.m.size() && m[0].size() == rhs.m[0].size()){
        Matrix rel(m.size(), m[0].size());
        for(unsigned int i=0;i<m.size();++i){
            for(unsigned int j=0;j<m[0].size();j++){    
                rel.m[i][j] = m[i][j] - rhs.m[i][j];
            }
        }
        return rel;
    }
    else exit(1);
}

Matrix Matrix::operator*(const Matrix& rhs){
    return Matrix(mult(rhs.m));
}

vector<vector<int> > Matrix::mult(const vector<vector<int> >& n){
    vector<vector<int> > u(m.size(), vector<int>(n[0].size(), 0));
    
    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<n[0].size();j++){
           for(unsigned int k=0;k<m[0].size();k++){
               u[i][j]+=(m[i][k]*n[k][j]);           
           }
        }
    }
    return u;
}

void Matrix::assignMatrix(const vector<vector<int> >& u){
    m = u;
}

ostream& operator<<(ostream& out, const Matrix& m){
    for(unsigned int i=0;i<m.m.size();i++){
        for(unsigned int j=0;j<m.m[0].size();j++){
            cout << setw(4) << m.m[i][j] << " ";
        }
        cout << endl;
    }
    return out;
}


int main(){
    srand(time(NULL));
    vector<vector<int> > r(4, vector<int>(4, 0));
    vector<vector<int> > u(4, vector<int>(4, 0));
    vector<vector<int> > v(4, vector<int>(4, 0));        
  
    inputMatrix(u);
    inputMatrix(v);    

    mulitplication(r,u,v);

    printMatrix(u);   cout << endl;
    printMatrix(v);   cout << endl;
    printMatrix(r);   cout << endl;
    
    Matrix cu(u),cv(v);
    cout << cu*cv << endl;    
    
    cout << endl;
    system("pause");
}
