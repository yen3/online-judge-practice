#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <x86intrin.h>

#define MAX_SIZE 2048
#define INDEX2(x, y, matrix) (x * matrix->column + y)

#if defined(__cplusplus)
#define MEM_ASSUME_ALIGN(mem, align) \
  static_cast<int*>(__builtin_assume_aligned(mem, align))
#else
#define MEM_ASSUME_ALIGN(mem, align) __builtin_assume_aligned(mem, align)
#endif

int32_t horizontal_add(const __m128i* x) {
  __m128i sum1 = _mm_hadd_epi32(*x, *x);
  __m128i sum2 = _mm_hadd_epi32(sum1, sum1);
  return _mm_cvtsi128_si32(sum2);
}

int32_t muliAdd4(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);
  __m128i xmm_c = _mm_mullo_epi32(xmm_a, xmm_b);

  return horizontal_add(&xmm_c);
}

#if 0
int32_t accmulate4(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  __m128i xmm_c = _mm_add_epi32(xmm_a, xmm_b);

  return horizontal_add(&xmm_c);
}

int32_t accumulate4_2(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  return horizontal_add(&xmm_a) + horizontal_add(&xmm_b);
}


int32_t accmulate4_3(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  xmm_a += xmm_b;
  xmm_a = _mm_hadd_epi32(xmm_a, xmm_a);
  __m128i sum2 = _mm_hadd_epi32(xmm_a, xmm_a);

  return _mm_cvtsi128_si32(sum2);
}
#endif

void* allocate_mem(size_t size) {
#if _ISOC11_SOURCE
  const int MEM_ALIGN = 128;
  return aligned_alloc(MEM_ALIGN, size);
#elif _POSIX_C_SOURCE >= 200112L
  const int MEM_ALIGN = 128;
  void* m;
  posix_memalign(&m, MEM_ALIGN, size);
  return m;
#else
  return malloc(size);
#endif
}

typedef struct Matrix_t {
  int32_t value[MAX_SIZE * MAX_SIZE];
  uint32_t row;
  uint32_t column;
} Matrix;

void Matrix_mult_8(Matrix* __restrict__ c, Matrix* __restrict__ a,
                   Matrix* __restrict__ b) {
  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  int32_t* bx_v = MEM_ASSUME_ALIGN(bx->value, 64);
  int32_t* b_v = MEM_ASSUME_ALIGN(b->value, 64);
  int32_t* c_v = MEM_ASSUME_ALIGN(c->value, 64);
  int32_t* a_v = MEM_ASSUME_ALIGN(a->value, 64);

#define CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(matrix) \
  ((matrix->column & 63) || (matrix->row & 63))

  if (CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(a) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(b) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(c)) {
    __builtin_unreachable();
  }
#undef CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64

  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      bx_v[INDEX2(j, i, bx)] = b_v[INDEX2(i, j, b)];
    }
  }
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; k += 4) {
        /*c_v[INDEX2(i, j, c)] += a_v[INDEX2(i, k, a)] * bx_v[INDEX2(i, k,
         * bx)];*/
        c_v[INDEX2(i, j, c)] +=
            muliAdd4(a_v + INDEX2(i, k, a), bx_v + INDEX2(i, k, bx));
      }
    }
  }

  free(bx);
}

int main() {}
