#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <x86intrin.h>

#define MAX_SIZE 2048
#define MIN(x, y) (((x) > (y)) ? (y) : (x))

#define INDEX(x, y, row_size, column_size) (x * column_size + y)
#define INDEX2(x, y, matrix) (x * matrix->column + y)
#define M(matrix, x, y) matrix->value[INDEX2(x, y, matrix)]

typedef int a64int __attribute__ ((__aligned__(64)));

#if defined(__cplusplus)
#define MEM_ASSUME_ALIGN(mem, align) \
  static_cast<int*>(__builtin_assume_aligned(mem, align))
#else
#define MEM_ASSUME_ALIGN(mem, align) __builtin_assume_aligned(mem, align)
#endif

int32_t horizontal_add(const __m128i* x) {
  __m128i sum1 = _mm_hadd_epi32(*x, *x);
  __m128i sum2 = _mm_hadd_epi32(sum1, sum1);
  return _mm_cvtsi128_si32(sum2);
}

int32_t muliAdd4(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);
  __m128i xmm_c = _mm_mullo_epi32(xmm_a, xmm_b);

  return horizontal_add(&xmm_c);
}

#if 0
int32_t accmulate4(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  __m128i xmm_c = _mm_add_epi32(xmm_a, xmm_b);

  return horizontal_add(&xmm_c);
}

int32_t accumulate4_2(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  return horizontal_add(&xmm_a) + horizontal_add(&xmm_b);
}


int32_t accmulate4_3(int32_t* __restrict__ a, int32_t* __restrict__ b) {
  int* xa = MEM_ASSUME_ALIGN(a, 16);
  int* xb = MEM_ASSUME_ALIGN(b, 16);

  __m128i xmm_a = _mm_setr_epi32(xa[0], xa[1], xa[2], xa[3]);
  __m128i xmm_b = _mm_setr_epi32(xb[0], xb[1], xb[2], xb[3]);

  xmm_a += xmm_b;
  xmm_a = _mm_hadd_epi32(xmm_a, xmm_a);
  __m128i sum2 = _mm_hadd_epi32(xmm_a, xmm_a);

  return _mm_cvtsi128_si32(sum2);
}
#endif


typedef struct Matrix_t {
  int value[MAX_SIZE * MAX_SIZE];
  size_t row;
  size_t column;
} Matrix;

/*#define M(matrix, x, y)  matrix->value[x * matrix->column + y]*/

typedef void (*MatrixMultFun)(Matrix*, Matrix*, Matrix*);

void* allocate_mem(size_t size) {
  const int MEM_ALIGN = 64;
#if _ISOC11_SOURCE
  return aligned_alloc(MEM_ALIGN, size);
#elif _POSIX_C_SOURCE >= 200112L
  void* m;
  posix_memalign(&m, MEM_ALIGN, size);
  return m;
#else
  return malloc(size);
#endif
}

void Matrix_init(Matrix* m, size_t row, size_t column) {
  m->row = row;
  m->column = column;
  memset(m->value, 0, sizeof(m->value));
}

void Matrix_mult_6(Matrix* restrict c, Matrix* restrict a,
                   Matrix* restrict b) {
  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      M(bx, j, i) = M(b, i, j);
    }
  }
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; ++k) {
        M(c, i, j) += M(a, i, k) * M(bx, i, k);
      }
    }
  }

  free(bx);
}

void Matrix_mult_7(Matrix* restrict c, Matrix* restrict a,
                   Matrix* restrict b) {

  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  a64int* bx_v = __builtin_assume_aligned(bx->value, 64);
  a64int* b_v = __builtin_assume_aligned(b->value, 64);
  a64int* c_v = __builtin_assume_aligned(c->value, 64);
  a64int* a_v = __builtin_assume_aligned(a->value, 64);

#define CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(matrix) \
  ((matrix->column & 63) || (matrix->row & 63))

  if (CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(a) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(b) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(c)) {
    __builtin_unreachable();
  }
#undef CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64

  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      bx_v[INDEX2(j, i, bx)] = b_v[INDEX2(i, j, b)];
    }
  }
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; ++k) {
        c_v[INDEX2(i, j, c)] += a_v[INDEX2(i, k, a)] * bx_v[INDEX2(i, k, bx)];
      }
    }
  }

  free(bx);
}

int testFunction(int* input, int length) {
  input = __builtin_assume_aligned(input, 64);

  if (length & 63) __builtin_unreachable();

  int sum = 0;
  for (int i = 0; i < length; ++i) {
    sum += input[i];
  }
  return sum;
}

///////////////////////////////////////////////////////////////////////////////
// Test body
///////////////////////////////////////////////////////////////////////////////
long long measure_matrix_mult(MatrixMultFun mult_fun, Matrix* a, Matrix* b,
                              Matrix* c) {
  struct timeval time_start;
  struct timeval time_end;
  long long elapsed_time;

  Matrix_init(c, a->row, b->column);

  gettimeofday(&time_start, NULL);
  { (*mult_fun)(a, b, c); }
  gettimeofday(&time_end, NULL);

  elapsed_time = 1000 * (time_end.tv_sec - time_start.tv_sec) +
                 (time_end.tv_usec - time_start.tv_usec) / 1000;
  printf("%lld ms\n", elapsed_time);

  return elapsed_time;
}

void Matrix_mult_8(Matrix* __restrict__ c, Matrix* __restrict__ a,
                   Matrix* __restrict__ b) {
  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  int32_t* bx_v = MEM_ASSUME_ALIGN(bx->value, 64);
  int32_t* b_v = MEM_ASSUME_ALIGN(b->value, 64);
  int32_t* c_v = MEM_ASSUME_ALIGN(c->value, 64);
  int32_t* a_v = MEM_ASSUME_ALIGN(a->value, 64);

#define CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(matrix) \
  ((matrix->column & 63) || (matrix->row & 63))

  if (CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(a) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(b) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(c)) {
    __builtin_unreachable();
  }
#undef CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64

  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      bx_v[INDEX2(j, i, bx)] = b_v[INDEX2(i, j, b)];
    }
  }
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; k += 4) {
        /*c_v[INDEX2(i, j, c)] += a_v[INDEX2(i, k, a)] * bx_v[INDEX2(i, k,
         * bx)];*/
        c_v[INDEX2(i, j, c)] +=
            muliAdd4(a_v + INDEX2(i, k, a), bx_v + INDEX2(i, k, bx));
      }
    }
  }

  free(bx);
}

/* Perform c = a * b */
/* block multiplication with block size = 32 */
void Matrix_mult_9(Matrix* __restrict__ a, Matrix* __restrict__ b, Matrix* __restrict__ c) {
  const size_t block_row = 128;
  const size_t block_column = 128;

  for (size_t br = 0; br < a->row; br += block_row) {
    for (size_t bc = 0; bc < b->column; bc += block_column) {
      for (size_t i = 0; i < a->row; ++i) {
        size_t min_row = MIN(br + block_row, a->row);
        size_t min_col = MIN(bc + block_column, b->column);
        for (size_t j = br; j < min_row; ++j) {
          for (size_t k = bc; k < min_col; ++k) {
            M(c, i, j) += M(a, i, k) * M(b, k, j);
          }
        }
      }
    }
  }
}

int main(int argc, char* argv[]) {
  const size_t test_row = 1024;
  const size_t test_column = 1024;

  Matrix* a = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* b = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* c = (Matrix*)allocate_mem(sizeof(Matrix));

  Matrix_init(a, test_row, test_column);
  Matrix_init(b, test_row, test_column);
  Matrix_init(c, test_row, test_column);

  int mult_fun_select = 0;

  if (argc == 2) mult_fun_select = atoi(argv[1]);

  if (mult_fun_select < 0 || mult_fun_select > 4) {
    printf("Select in 0, 1, 2, 3 and 4.\n");
    return 1;
  }

  MatrixMultFun mm_funs[] = {Matrix_mult_6, Matrix_mult_7, Matrix_mult_8, Matrix_mult_9};

  measure_matrix_mult(mm_funs[mult_fun_select], a, b, c);

  free(a);
  free(b);
  free(c);
}
