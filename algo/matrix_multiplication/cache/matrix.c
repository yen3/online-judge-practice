#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define MAX_SIZE 2048
#define MIN(x, y) (((x) > (y)) ? (y) : (x))

typedef struct Matrix_t {
  int value[MAX_SIZE][MAX_SIZE];
  size_t row;
  size_t column;
} Matrix;

typedef void (*MatrixMultFun)(Matrix*, Matrix*, Matrix*);

void* allocate_mem(size_t size) {
  const int MEM_ALIGN = 64;
#if _ISOC11_SOURCE
  return aligned_alloc(MEM_ALIGN, size);
#elif _POSIX_C_SOURCE >= 200112L
  void* m;
  posix_memalign(&m, MEM_ALIGN, size);
  return m;
#else
  return malloc(size);
#endif
}

void Matrix_init(Matrix* m, size_t row, size_t column) {
  m->row = row;
  m->column = column;
  memset(m->value, 0, MAX_SIZE * MAX_SIZE);
}

/* Perform c = a * b */
/* Native multplication */
void Matrix_mult(Matrix* restrict a, Matrix* restrict b, Matrix* restrict c) {
  for (size_t i = 0; i < a->row; ++i)
    for (size_t j = 0; j < b->column; ++j)
      for (size_t k = 0; k < a->column; ++k)
        c->value[i][j] += a->value[i][k] * b->value[k][j];
}

/* Perform c = a * b */
/* Native multiplication but change the access order to row-major. */
void Matrix_mult_2(Matrix* restrict a, Matrix* restrict b, Matrix* restrict c) {
  for (size_t i = 0; i < a->row; ++i)
    for (size_t k = 0; k < a->column; ++k)
      for (size_t j = 0; j < b->column; ++j)
        c->value[i][j] += a->value[i][k] * b->value[k][j];
}

/* Perform c = a * b */
/* Transpose matrix B then do the multiplication. */
void Matrix_mult_3(Matrix* restrict a, Matrix* restrict b, Matrix* restrict c) {
  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  for (size_t i = 0; i < b->row; ++i)
    for (size_t j = 0; j < b->column; ++j) bx->value[j][i] = b->value[i][j];
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i)
    for (size_t j = 0; j < b->row; ++j)
      for (size_t k = 0; k < c->column; ++k)
        c->value[i][j] += a->value[i][k] * bx->value[j][k];

  free(bx);
}

/* Perform c = a * b */
/* block multiplication with block size = 32 */
void Matrix_mult_4(Matrix* restrict a, Matrix* restrict b, Matrix* restrict c) {
  const size_t block_row = 32;
  const size_t block_column = 32;

  for (size_t br = 0; br < a->row; br += block_row)
    for (size_t bc = 0; bc < b->column; bc += block_column)
      for (size_t i = 0; i < a->row; ++i) {
        size_t min_row = MIN(br + block_row, a->row);
        size_t min_col = MIN(bc + block_column, b->column);
        for (size_t j = br; j < min_row; ++j)
          for (size_t k = bc; k < min_col; ++k)
            c->value[i][j] += a->value[i][k] * b->value[k][j];
      }
}

/* Perform c = a * b */
/* block multiplication with block size = 64 */
void Matrix_mult_5(Matrix* restrict a, Matrix* restrict b, Matrix* restrict c) {
  const size_t block_row = 64;
  const size_t block_column = 64;

  for (size_t br = 0; br < a->row; br += block_row)
    for (size_t bc = 0; bc < b->column; bc += block_column)
      for (size_t i = 0; i < a->row; ++i) {
        size_t min_row = MIN(br + block_row, a->row);
        size_t min_col = MIN(bc + block_column, b->column);
        for (size_t k = bc; k < min_col; ++k)
          for (size_t j = br; j < min_row; ++j)
            c->value[i][j] += a->value[i][k] * b->value[k][j];
      }
}

///////////////////////////////////////////////////////////////////////////////
// Test body
///////////////////////////////////////////////////////////////////////////////
long long measure_matrix_mult(MatrixMultFun mult_fun, Matrix* a, Matrix* b,
                              Matrix* c) {
  struct timeval time_start;
  struct timeval time_end;
  long long elapsed_time;

  Matrix_init(c, a->row, b->column);

  gettimeofday(&time_start, NULL);
  { (*mult_fun)(a, b, c); }
  gettimeofday(&time_end, NULL);

  elapsed_time = 1000 * (time_end.tv_sec - time_start.tv_sec) +
                 (time_end.tv_usec - time_start.tv_usec) / 1000;
  printf("%lld ms\n", elapsed_time);

  return elapsed_time;
}

int main(int argc, char* argv[]) {
  const size_t test_row = 1024;
  const size_t test_column = 1024;

  Matrix* a = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* b = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* c = (Matrix*)allocate_mem(sizeof(Matrix));

  Matrix_init(a, test_row, test_column);
  Matrix_init(b, test_row, test_column);
  Matrix_init(c, test_row, test_column);

  int mult_fun_select = 4;

  if (argc == 2) mult_fun_select = atoi(argv[1]);

  if (mult_fun_select < 0 || mult_fun_select > 4) {
    printf("Select in 0, 1, 2, 3 and 4.\n");
    return 1;
  }

  MatrixMultFun mm_funs[] = {Matrix_mult, Matrix_mult_2, Matrix_mult_3,
                             Matrix_mult_4, Matrix_mult_5};

  measure_matrix_mult(mm_funs[mult_fun_select], a, b, c);

  free(a);
  free(b);
  free(c);
}
