#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define MAX_SIZE 2048
#define MIN(x, y) (((x) > (y)) ? (y) : (x))

#define INDEX(x, y, row_size, column_size) (x * column_size + y)
#define INDEX2(x, y, matrix) (x * matrix->column + y)
#define M(matrix, x, y) matrix->value[INDEX2(x, y, matrix)]

typedef int a64int __attribute__ ((__aligned__(64)));


typedef struct Matrix_t {
  int value[MAX_SIZE * MAX_SIZE];
  size_t row;
  size_t column;
} Matrix;

/*#define M(matrix, x, y)  matrix->value[x * matrix->column + y]*/

typedef void (*MatrixMultFun)(Matrix*, Matrix*, Matrix*);

void* allocate_mem(size_t size) {
  const int MEM_ALIGN = 64;
#if _ISOC11_SOURCE
  return aligned_alloc(MEM_ALIGN, size);
#elif _POSIX_C_SOURCE >= 200112L
  void* m;
  posix_memalign(&m, MEM_ALIGN, size);
  return m;
#else
  return malloc(size);
#endif
}

void Matrix_init(Matrix* m, size_t row, size_t column) {
  m->row = row;
  m->column = column;
  memset(m->value, 0, sizeof(m->value));
}

void Matrix_mult_6(Matrix* restrict c, Matrix* restrict a,
                   Matrix* restrict b) {
  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      M(bx, j, i) = M(b, i, j);
    }
  }
  bx->column = b->row;
  bx->row = b->column;

  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; ++k) {
        M(c, i, j) += M(a, i, k) * M(bx, i, k);
      }
    }
  }

  free(bx);
}

void Matrix_mult_7(Matrix* restrict c, Matrix* restrict a,
                   Matrix* restrict b) {

  Matrix* bx = (Matrix*)allocate_mem(sizeof(Matrix));
  a64int* bx_v = __builtin_assume_aligned(bx->value, 64);
  a64int* b_v = __builtin_assume_aligned(b->value, 64);
  a64int* c_v = __builtin_assume_aligned(c->value, 64);
  a64int* a_v = __builtin_assume_aligned(a->value, 64);

#define CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(matrix) \
  ((matrix->column & 63) || (matrix->row & 63))

  if (CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(a) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(b) ||
      CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64(c)) {
    __builtin_unreachable();
  }
#undef CHECK_ROW_COLUMN_SIZE_NOT_DIVIDE_BY_64

  for (size_t i = 0; i < b->row; ++i) {
    for (size_t j = 0; j < b->column; ++j) {
      bx_v[INDEX2(j, i, bx)] = b_v[INDEX2(i, j, b)];
    }
  }
  bx->column = b->row;
  bx->row = b->column;


  for (size_t i = 0; i < b->column; ++i) {
    for (size_t j = 0; j < b->row; ++j) {
      for (size_t k = 0; k < c->column; ++k) {
        c_v[INDEX2(i, j, c)] += a_v[INDEX2(i, k, a)] * bx_v[INDEX2(i, k, bx)];
      }
    }
  }

  free(bx);
}

int testFunction(int* input, int length) {
  input = __builtin_assume_aligned(input, 64);

  if (length & 63) __builtin_unreachable();

  int sum = 0;
  for (int i = 0; i < length; ++i) {
    sum += input[i];
  }
  return sum;
}

///////////////////////////////////////////////////////////////////////////////
// Test body
///////////////////////////////////////////////////////////////////////////////
long long measure_matrix_mult(MatrixMultFun mult_fun, Matrix* a, Matrix* b,
                              Matrix* c) {
  struct timeval time_start;
  struct timeval time_end;
  long long elapsed_time;

  Matrix_init(c, a->row, b->column);

  gettimeofday(&time_start, NULL);
  { (*mult_fun)(a, b, c); }
  gettimeofday(&time_end, NULL);

  elapsed_time = 1000 * (time_end.tv_sec - time_start.tv_sec) +
                 (time_end.tv_usec - time_start.tv_usec) / 1000;
  printf("%lld ms\n", elapsed_time);

  return elapsed_time;
}

int main(int argc, char* argv[]) {
  const size_t test_row = 1024;
  const size_t test_column = 1024;

  Matrix* a = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* b = (Matrix*)allocate_mem(sizeof(Matrix));
  Matrix* c = (Matrix*)allocate_mem(sizeof(Matrix));

  Matrix_init(a, test_row, test_column);
  Matrix_init(b, test_row, test_column);
  Matrix_init(c, test_row, test_column);

  int mult_fun_select = 0;

  if (argc == 2) mult_fun_select = atoi(argv[1]);

  if (mult_fun_select < 0 || mult_fun_select > 4) {
    printf("Select in 0, 1, 2, 3 and 4.\n");
    return 1;
  }

  MatrixMultFun mm_funs[] = {Matrix_mult_6, Matrix_mult_7};

  measure_matrix_mult(mm_funs[mult_fun_select], a, b, c);

  free(a);
  free(b);
  free(c);
}
