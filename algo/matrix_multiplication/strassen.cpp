#include <iostream>
#include <vector>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <utility>
using namespace std;

/* Matrix Multiplication - native algorithm with divide and coquner */
/* Assume u, v are 2^n square matrix */
/* r = u * v */

void mulitplication(vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v);
void nativeMult(pair<int,int> lr, pair<int,int> lu, pair<int,int> lv, int size,
          vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v);

/* applicaiton function */
void mulitplication(vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v){
    nativeMult(make_pair(0,0), make_pair(0,0), make_pair(0,0), r.size(), r, u, v);
}

/* recursive function */
void nativeMult(pair<int,int> lr, pair<int,int> lu, pair<int,int> lv, int size,
          vector<vector<int> >& r, vector<vector<int> >& u, vector<vector<int> >& v){
    static const int addr[8][2] = {{0,0}, {0,0}, {0,1}, {0,1}, {1,0}, {1,0}, {1,1}, {1,1}};
    static const int addu[8][2] = {{0,0}, {0,1}, {0,0}, {0,1}, {1,0}, {1,1}, {1,0}, {1,1}};
    static const int addv[8][2] = {{0,0}, {1,0}, {0,1}, {1,1}, {0,0}, {1,0}, {0,1}, {1,1}};

    if(size>2){
        for(unsigned int i=0;i<8;i++){
            cout << "r0 " << (size>>1) * addr[i][0] << endl;
            nativeMult(make_pair(lr.first+((size/2)*addr[i][0]), lr.second+((size/2)*addr[i][1])),
                 make_pair(lu.first+((size/2)*addu[i][0]), lu.second+((size/2)*addu[i][1])),
                 make_pair(lv.first+((size/2)*addv[i][0]), lv.second+((size/2)*addv[i][1])),
                 size/2,
                 r,
                 u,
                 v);
        }
    }
    else{
        for(unsigned int i=0;i<8;i++){
            r[lr.first + addr[i][0]][lr.second + addr[i][1]] +=
                       u[lu.first + addu[i][0]][lu.second + addu[i][1]] *
                       v[lv.first + addv[i][0]][lv.second + addv[i][1]];
        }
    }
}


/*
 *   Matrix Class protype and implementation
 */

/* Matrix class Implementation end */
class Matrix{
    friend void makeRandomMatrix(Matrix& );
    friend void matrixCombine(Matrix& m, const Matrix a, const Matrix b, const Matrix c, const Matrix d);
    friend ostream& operator<<(ostream& out, const Matrix& m);
    public:
        Matrix(){};
        Matrix(int x, int y=0):m(x,vector<int>((y==0)?x:y,0)){};
        Matrix(const vector<vector<int> >& u):m(u){};
        Matrix(const Matrix& data, int x, int y, int size); /* specialize for strassen */

        Matrix operator+(const Matrix&);
        Matrix operator-(const Matrix&);
        Matrix operator*(const Matrix&);
        
        int size();
    private:
        vector<vector<int> > m;
};



int Matrix::size(){
    if(m.size()!=0 && m[0].size()!=0 && m.size()==m[0].size())   return m.size();
    else return 0;
}

Matrix::Matrix(const Matrix& data, int x, int y, int size):m(size, vector<int>(size, 0)){
    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<m[i].size();i++){
            m[i][j] = data.m[i+x][j+y];
        }
    }
}

Matrix Matrix::operator+(const Matrix& rhs){
    if(m.size()== rhs.m.size() && m[0].size() == rhs.m[0].size()){
        Matrix rel(m.size(), m[0].size());
        for(unsigned int i=0;i<m.size();++i){
            for(unsigned int j=0;j<m[0].size();j++){
                rel.m[i][j] = m[i][j] + rhs.m[i][j];
            }
        }
        return rel;
    }
    else exit(1);
}

Matrix Matrix::operator-(const Matrix& rhs){
    if(m.size()== rhs.m.size() && m[0].size() == rhs.m[0].size()){
        Matrix rel(m.size(), m[0].size());
        for(unsigned int i=0;i<m.size();++i){
            for(unsigned int j=0;j<m[0].size();j++){
                rel.m[i][j] = m[i][j] - rhs.m[i][j];
            }
        }
        return rel;
    }
    else exit(1);
}

Matrix Matrix::operator*(const Matrix& data){
    vector<vector<int> > u(m.size(), vector<int>(data.m[0].size(), 0));

    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<data.m[0].size();j++){
           for(unsigned int k=0;k<data.m[0].size();k++){
               u
               [i][j]+=(m[i][k]*data.m[k][j]);
           }
        }
    }
    return Matrix(u);
}

ostream& operator<<(ostream& out, const Matrix& m){
    for(unsigned int i=0;i<m.m.size();i++){
        for(unsigned int j=0;j<m.m[0].size();j++){
            cout << setw(4) << m.m[i][j] << " ";
        }
        cout << endl;
    }
    return out;
}/* Matrix class Implementation end */


void makeRandomMatrix(Matrix& data){
    for(unsigned int i=0;i<data.m.size();i++){
         for(unsigned int j=0;j<data.m[i].size();j++){
              data.m[i][j] = rand()%5 + 1;
         }
    }
}

void matrixCombine(Matrix& m, const Matrix a, const Matrix b, const Matrix c, const Matrix d){
    for(unsigned int i=0;i<a.m.size();i++){
        for(unsigned int j=0;j<a.m[i].size();j++){
            m.m[i]           [j]            = a.m[i][j];
            m.m[i]           [j+a.m.size()] = b.m[i][j];
            m.m[i+a.m.size()][j]            = c.m[i][j];
            m.m[i+a.m.size()][j+a.m.size()] = b.m[i][j];                   
        }
    }
}

void strassenMult(Matrix& m, Matrix u, Matrix v, int size){
    if(size>2){
        static const int add_x[2][2] = {{0,0},{1,1}};
        static const int add_y[2][2] = {{0,1},{0,1}};

        Matrix a[2][2],b[2][2];
        for(unsigned int i=0;i<2;i++){
            for(unsigned int j=0;j<2;j++){
                a[i][j] = Matrix(u, (size/2)*add_x[i][j], (size/2)*add_y[i][j], size/2);
                b[i][j] = Matrix(v, (size/2)*add_x[i][j], (size/2)*add_y[i][j], size/2);
            }
        }
        
        Matrix p[7];
        strassenMult(p[0], a[0][0]+a[1][1], b[0][0]+b[1][1], size/2);
        strassenMult(p[1], a[1][0]+a[1][1], b[0][0]        , size/2);
        strassenMult(p[2], a[0][0]        , b[0][1]-b[1][1], size/2);
        strassenMult(p[3], a[1][1]        , b[1][0]-b[0][0], size/2);
        strassenMult(p[4], a[0][0]+a[1][1], b[1][1]        , size/2);
        strassenMult(p[5], a[1][0]-a[0][0], b[0][0]+b[0][1], size/2);
        strassenMult(p[6], a[0][1]-a[1][1], b[1][0]+b[1][1], size/2);
        
        matrixCombine(m, p[0]+p[3]-p[4]+p[6], p[2]+p[4], p[1]+p[3], p[0]+p[2]-p[1]+p[5]);
    }
    else{
         m = u*v; 
    }
}

void strassenMultiplication(Matrix& r, const Matrix& u, const Matrix& v){
    strassenMult(r,u,v, r.size());
}

int main(){
    srand(time(NULL));    
    Matrix u(4),v(4);
    makeRandomMatrix(u);
    makeRandomMatrix(v);
    
    cout << u << endl;
    cout << v << endl;
    cout << u*v << endl;

    cout << endl;
    system("pause");
}
