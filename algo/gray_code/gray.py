from __future__ import print_function

def gray(n):
    if n == 1:
        return ["0", "1"]
    else:
        l = gray(n-1)
        prev = ["0" + nl for nl in l]
        l.reverse()
        succ = ["1" + nl for nl in l]
        return prev + succ 

def main():
    print(gray(3))

if __name__ == '__main__':
    main()
