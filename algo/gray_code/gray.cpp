#include <iostream>
#include <vector>
#include <string>

void gray_code(int n, std::vector<std::string >& g){
    if(n == 1){
        g.push_back("0");
        g.push_back("1");
    }
    else{
        gray_code(n-1, g);
        for(std::size_t begin=0, end=g.size(); begin< end; ++begin){
            g.push_back(g[end-begin-1]);
        }
        for(std::size_t prev=0, succ=g.size()/2; prev < g.size()/2; ++prev, ++succ){
            g[prev] = "0" + g[prev];
            g[succ] = "1" + g[succ];
        }
    }
}

int main(int argc, char** argv){
    std::vector<std::string > g;
    gray_code(4, g);

    for(auto& elem: g){
        std::cout << elem << std::endl;
    }

}
