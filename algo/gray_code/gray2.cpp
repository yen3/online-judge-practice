#include <iostream>
#include <string>
#include <vector>
#include <cmath>

void cjk(int b, int size, std::vector<int>& loc, const std::string& s){
    if(size == 1){
        for(std::size_t i=b; i<s.size(); i++){
            for(std::size_t j=0; j<loc.size(); j++){
                std::cout << s[loc[j]];
            }
            std::cout << s[i] << std::endl;
        }
    }
    else{
        for(int i=b; i<s.size(); i++){
            loc.push_back(i);
            cjk(i+1, size-1, loc, s);
            loc.pop_back();
        }
    }
}

void gray_code_(int code_size, int p, std::vector<std::string>& g_list){
    if (p!= code_size){
        int c_g_list_size = g_list.size();
        for(std::size_t i=0; i<c_g_list_size; ++i) g_list.push_back(g_list[c_g_list_size-i-1]);

        for(std::size_t i=0; i<c_g_list_size; ++i) g_list[i] = "0" + g_list[i];
        for(std::size_t i=c_g_list_size; i<g_list.size(); ++i) g_list[i] = "1" + g_list[i];

        gray_code_(code_size, p+1, g_list);
    }
}

void gray_code(int code_size){
    std::vector<std::string> g_list;
    g_list.push_back("0");
    g_list.push_back("1");

    if(code_size != 1) gray_code_(code_size, 1, g_list);

    for(std::size_t i=0; i<g_list.size(); i++){
        std::cout << g_list[i] << std::endl;
    }
}

//void gray_code_2(int code_size){
    //std::string t = "";
    //for(std::size_t i=0; i< code_size; i++) t+="0";
    //std::vector<std::string> g_list(static_cast<int>(std::pow(2.0, static_cast<double>(code_size))), t);

    //for(std::size_t bit=0; bit<code_size; ++bit){
        //int times =
    //}
//}


int main(int argc, char** argv){
    std::string s = "abcdefg";
    std::vector<int> loc;
    cjk(0, 2, loc, s);


    gray_code(3);
}
