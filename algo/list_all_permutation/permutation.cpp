#include <iostream>
#include <string>
#include <vector>

void FindAllPermutationsInternal(std::string& curr, bool* s_used,
                                 const std::string& s,
                                 std::vector<std::string>& ps) {
  if (curr.size() == s.size()) {
    ps.push_back(curr);
  }

  for (std::size_t i = 0; i < s.size(); ++i) {
    if (s_used[i] == false) {
      s_used[i] = true;
      curr.push_back(s[i]);

      FindAllPermutationsInternal(curr, s_used, s, ps);

      s_used[i] = false;
      curr.pop_back();
    }
  }
}

void FindAllPermutations(const std::string& s, std::vector<std::string>& ps) {
  bool s_used[s.size()];
  for (std::size_t i = 0; i < s.size(); ++i) {
    s_used[i] = false;
  }

  std::string curr;
  FindAllPermutationsInternal(curr, s_used, s, ps);
}

int main() {
  std::string s = "1234";
  std::vector<std::string> ps;
  FindAllPermutations(s, ps);

  std::cout << ps.size() << std::endl;
  for (const auto& p : ps) {
    std::cout << p << std::endl;
  }

  return 0;
}
