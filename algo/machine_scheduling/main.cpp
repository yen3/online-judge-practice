/* Computer Algorithms 2nd, p212, Example 4.2 */
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <queue>

typedef unsigned int TimeUnit;

typedef std::pair<TimeUnit, TimeUnit> Task; // <start time, end time>
typedef std::vector<Task> TaskList;

typedef std::priority_queue<TimeUnit, std::vector<TimeUnit>,
	std::greater<TimeUnit> > MachineList; // present next free time

void read_tasks(TaskList& tl)
{
  std::size_t num_tasks;
  TimeUnit start_time;
  TimeUnit end_time;

  std::cin >> num_tasks;
  for (std::size_t i = 0; i < num_tasks; ++i) {
      std::cin >> start_time >> end_time;
      tl.push_back(std::make_pair(start_time, end_time));
  }
}

unsigned int count_machines(const TaskList& tl)
{
  MachineList ml;

  if(tl.size())
    ml.push(tl[0].second);

  for (std::size_t i = 1; i < tl.size(); ++i) {
    TimeUnit min_available = ml.top();

    if (min_available <= tl[i].first)
      ml.pop();

    ml.push(tl[i].second);
  }

  return ml.size();
}


int main(int argc, char *argv[])
{
  TaskList task_list;
  read_tasks(task_list);

  std::sort(task_list.begin(), task_list.end(),
	    [](const Task& x, const Task& y){
	      if(x.first == y.first)  return x.second < y.second;
	      return x.first < y.first;
	    });

  std::cout << count_machines(task_list) << std::endl;

  return 0;
}
