import           Data.Function
import           Data.List

data HTree a b = Node (a, b) | INode (HTree a b) (HTree a b) deriving (Show, Eq, Read)

huffmanCode :: (Num b, Ord b) => [(a, b)] -> [(a, [Char])]
huffmanCode = huffmanCode_ "" . huffmanTree . map (\(x, y) -> Node (x, y))

value :: (Num b) => HTree a b -> b
value (Node (_, b))      = b
value (INode left right) = value left + value right

huffmanCode_ :: (Num b, Ord b) => [Char] -> HTree a b -> [(a, [Char])]
huffmanCode_ s (Node (a, _)) = [(a, s)]
huffmanCode_ s (INode left right) = huffmanCode_ ('0':s) left  ++ huffmanCode_ ('1':s) right

huffmanTree :: (Num b, Ord b) => [HTree a b] -> HTree a b
huffmanTree nl =
    if length nl == 1 then head nl
    else let (x:xs:xss) = sortBy (compare `on` value) nl in
         huffmanTree (INode x xs:xss)
