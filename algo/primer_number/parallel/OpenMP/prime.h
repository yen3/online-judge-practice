#ifndef PRIME_H_
#define PRIME_H_

int is_prime(int num, const int* p, const int p_size);
int* make_prime_table(const int p_size);
void make_int_max_prime_table(int* table, int* table_size, int* p, int p_size);
void write_to_file(const char* filename, int* table, int table_size);

#endif
