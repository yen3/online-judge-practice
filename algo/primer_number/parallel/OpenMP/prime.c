#include "prime.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

int is_prime(const int num, const int* p, const int p_size){
    int sqrt_num = (int)sqrt((double)num);

    int i=0;
    for(i=2; p[i] < sqrt_num+1; i++){
        if(num % p[i] == 0) return 0;
    }
    return 1;
}

int* make_prime_table(const int p_size){
    int* p = (int*)malloc(sizeof(int)*p_size);
    memset(p, 0, sizeof(int)*p_size);
    
    if(p_size >3){
        p[0] = 2;
        p[1] = 3;
        p[2] = 5;
    }

    int i=0, j=0;
    int num = 0;
    for(i=3, num=7, j=0; i<p_size; j++, num+=(j%2==0)?2:4){
        if(is_prime(num, p, p_size)){
            p[i++] = num;
        }
    }
    
    return p;
}

void make_int_max_prime_table(int* table, int* table_size, int* p, int p_size){
    unsigned int num = 10923*6-1;
    int temp_table_size = *table_size;
    int i=0;
    
    for(i=0; num<INT_MAX; i++, num+=(i%2==0)?2:4){
        if(is_prime(num, p, p_size)){
            table[temp_table_size++] = num;
        }
    }
    *table_size = temp_table_size;
}

void write_to_file(const char* filename, int* table, int table_size){
    FILE *fp = fopen(filename, "wb");
    fwrite(table, sizeof(int), table_size, fp);
    fclose(fp);
}
