#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "prime.h"
#include "prime_parallel.h"
#include <omp.h>

void make_locale_prime_range(CalRange* range, int* lp, int* lp_size, int* p, const int p_size){
    unsigned int num = range->begin/6*6-1;
    
    int i=0;
    for(i=0; num<range->begin; i++, num+=(i%2==0)?2:4) ;
    for(; num <= range->end; i++, num+=(i%2==0)?2:4){
        if(is_prime(num, p, p_size)){
            lp[*lp_size] = num;
            (*lp_size)++;
        }
    }
}

void make_openmp_prime_table(int** max_prime_table, int* max_prime_table_size, int thread_size){
/*    int thread_size = omp_get_num_procs();*/
    omp_set_num_threads(thread_size);

    int max_range_list_size = MAX_NUM/RANGE;
    printf("%d %d\n", max_range_list_size, thread_size);
    CalRange* total_range_list = get_max_range_list(max_range_list_size);
/*    int i=0;*/

    CalRange* rang_list_thread = get_rang_list_begin_end(max_range_list_size, thread_size); 
    int* prime_table = make_prime_table(PRIME_TABLE_SIZE);

    int* max_table = (int*)malloc(sizeof(int)*MAX_TABLE_SIZE);
    int max_table_size=0;
    memset(max_table, 0, sizeof(int)*MAX_TABLE_SIZE);

    #pragma omp parallel
    {
        int lp_size = 0;
        int* lp = (int*)malloc(sizeof(int)*MAX_LOCAL_TABLE_SIZE);
        memset(lp, 0, sizeof(int)*MAX_LOCAL_TABLE_SIZE);
        
/*        int thread_id = omp_get_thread_num();*/
        int i, j;

/*        printf("thread id: %d\n", thread_id);*/
        
        #pragma omp for
        for(i=0; i<thread_size; i++){
            for(j=rang_list_thread[i].begin; j<rang_list_thread[i].end; j++){
/*                printf("%d: begin: %d, end: %d\n", thread_id, total_range_list[j]. begin, total_range_list[j].end);*/
                make_locale_prime_range(&total_range_list[j], lp, &lp_size, prime_table, PRIME_TABLE_SIZE);
            }
        }
        
        #pragma omp for ordered
        for(i=0; i<thread_size; i++){
            #pragma omp ordered
            {
                memcpy(max_table+max_table_size, lp, sizeof(int)*lp_size);
                max_table_size += lp_size;
            }
        }

        SAFE_DELETE(lp);
    }

   SAFE_DELETE(total_range_list);
   SAFE_DELETE(rang_list_thread);
   SAFE_DELETE(prime_table);

   *max_prime_table = max_table;
   *max_prime_table_size = max_table_size;
}

void make_big_prime_table(const char* filename, int thread_size){
    int* max_prime_table;
    int max_prime_table_size;

    double start_time = omp_get_wtime();
    make_openmp_prime_table(&max_prime_table, &max_prime_table_size, thread_size);
    double end_time = omp_get_wtime();
    printf("total cost time: %lf\n", end_time-start_time);

    write_to_file(filename, max_prime_table, max_prime_table_size);
}

int main(int argc, char **argv){
    int thread_size = omp_get_num_procs();
    char out_name[128];
    memset(out_name, 0, sizeof(char)*128);
    if(argc >= 3){
        thread_size = atoi(argv[1]); 
        strncpy(out_name, argv[2], 128);
    }
    else{
        strncpy(out_name, "prime.data", 10);
    }
    
    make_big_prime_table(out_name, thread_size);

    return 0;
}
