#include "prime_parallel.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "prime.h"


void swap(CalRange* x, CalRange* y){
    CalRange temp;
    memcpy(&temp, x, sizeof(CalRange));
    memcpy(x, y, sizeof(CalRange));
    memcpy(y, &temp, sizeof(CalRange));
}

int* get_range_list_size(const int max_range_list_size, const int cpu_size){
    int i=0;
    int* range_size = (int*)malloc(sizeof(int)*cpu_size);
    memset(range_size, 0, sizeof(int)*cpu_size);

    int initial_size = max_range_list_size/cpu_size;
    int remain_size = max_range_list_size%cpu_size;
    for(i=0; i<cpu_size; i++){
        range_size[i] = initial_size;
        if(i<remain_size) range_size[i]++;
    }

    return range_size;
}

CalRange* get_rang_list_begin_end(const int max_range_list_size, const int size){
    int* range = get_range_list_size(max_range_list_size, size);
    CalRange* range_mark = (CalRange*)malloc(sizeof(CalRange)*size);
    memset(range_mark, 0, sizeof(CalRange)*size);

    int i=0;
    int index = 0;
    for(i=0; i<size; i++){
        range_mark[i].begin = index;
        range_mark[i].end = index + range[i];
        index += range[i];
    }

    SAFE_DELETE(range);
    return range_mark;
} 


CalRange* get_max_range_list(const int rang_list_size){
    CalRange* range_list = (CalRange*)malloc(sizeof(CalRange)*rang_list_size);
    memset(range_list, 0, sizeof(CalRange)*rang_list_size);
   
    int i=0;
    for(i=0; i<rang_list_size; i++){
        range_list[i].begin = RANGE*i+1;
        range_list[i].end = RANGE*(i+1);
    }
    range_list[0].begin = 6;
    range_list[rang_list_size-1].end = MAX_NUM;

    for(i=0; i<rang_list_size; i++){
        swap(&range_list[i], &range_list[rand()%rang_list_size]);
    }
    
    return range_list;
}

