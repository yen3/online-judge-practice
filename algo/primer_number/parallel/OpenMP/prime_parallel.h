#ifndef PRIME_PARALLEL_H
#define PRIME_PARALLEL_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include "prime.h"
#define RANGE                 1000000   
#define MAX_NUM               2147483647
//#define RANGE                 10000      // Just for test
//#define MAX_NUM               100000     // Just for test

#define MAX_TABLE_SIZE        110100480 
#define MAX_LOCAL_TABLE_SIZE  110100480 
//#define MAX_LOCAL_TABLE_SIZE  55050240 
#define PRIME_TABLE_SIZE      6543

#ifndef SAFE_DELETE
    #define SAFE_DELETE(x) if(x!=NULL){ free(x); x=NULL; }
#endif

typedef struct{
    int begin;
    int end;
}CalRange;

void swap(CalRange* x, CalRange* y);
int* get_range_list_size(const int max_range_list_size, const int cpu_size);
CalRange* get_rang_list_begin_end(const int max_range_list_size, const int size);
CalRange* get_max_range_list(const int rang_list_size);


#endif
