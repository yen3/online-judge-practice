package main

import (
	"fmt"
	"math"
)

func isPrime(num int, ps []int) bool {
	sqrtNum := int(math.Sqrt(float64(num))) + 1

	for i := 2; ps[i] <= sqrtNum; i++ {
		if num%ps[i] == 0 {
			return false
		}
	}
	return true
}

func primeFactors(sqrtMax int) []int {
	ps := make([]int, 0)
	ps = append(ps, 2)
	ps = append(ps, 3)
	ps = append(ps, 5)

	for num := 6; num <= sqrtMax; num += 6 {
		n1 := num + 1
		n2 := num + 5

		if isPrime(n1, ps) {
			ps = append(ps, n1)
		}
		if isPrime(n2, ps) {
			ps = append(ps, n2)
		}
	}

	return ps
}

// NumRange present a range of number (begin, end]
type NumRange struct {
	Begin int
	End   int
}

func calNumRanges(maxNum int, rangeSize int) []NumRange {
	rs := make([]NumRange, rangeSize)

	for i := 0; i < len(rs); i++ {
		begin := maxNum / rangeSize * i
		end := maxNum / rangeSize * (i + 1)
		if i == rangeSize-1 {
			end = maxNum + 1
		}

		rs[i].Begin = begin
		rs[i].End = end
	}
	return rs
}

func countPrimes(ps []int, rs []NumRange) int {
	qc := make(chan int)

	for i := 0; i < len(rs); i++ {
		go func(idx int, ps []int, rs []NumRange, qc chan int) {
			end := rs[idx].End

			localCount := 0
			for num := (rs[idx].Begin + 5) / 6; num < end; num += 6 {
				n1 := num + 1
				n2 := num + 5

				if ProbablyPrime(n1) {
					localCount++
				}
				// if isPrime(n1, ps) {
				// localCount++
				// }
				if isPrime(n2, ps) {
					localCount++
				}
			}
			qc <- localCount
		}(i, ps, rs, qc)
	}

	var totalCount int = 0
	for i := 0; i < len(rs); i++ {
		localCount := <-qc
		fmt.Printf("%v: %v\n", i, localCount)
		totalCount += localCount
	}

	return totalCount
}

func main() {
	ps := primeFactors(65536)
	rs := calNumRanges(4000000, 1000)
	fmt.Println(len(ps), len(rs))

	ns := countPrimes(ps, rs)
	fmt.Println(ns)
}
