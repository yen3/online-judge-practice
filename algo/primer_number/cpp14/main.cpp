#include <algorithm>
#include <array>
#include <atomic>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <functional>
#include <random>
#include <thread>
#include <utility>
#include <vector>

namespace {

const uint32_t kThreadSize = 8;
const int kCacheLineSize = 64;

}  // namespace

struct NumCacheLine {
  uint64_t num;
  uint8_t cache_line_padding[kCacheLineSize - sizeof(num)];
};

bool is_prime(uint32_t num, const std::vector<uint32_t>& ps) {
  for (size_t ps_idx = 2; ps_idx < ps.size() &&
                          ps[ps_idx] <= static_cast<uint32_t>(std::sqrt(num));
       ++ps_idx) {
    if (num % ps[ps_idx] == 0) {
      return false;
    }
  }

  return true;
}

std::vector<uint32_t> prime_bases(uint32_t sqrt_max = 65536) {
  std::vector<uint32_t> ps;
  ps.emplace_back(2);
  ps.emplace_back(3);
  ps.emplace_back(5);

  for (uint32_t n = 6; n <= sqrt_max; n += 6) {
    uint32_t n1 = n + 1;
    uint32_t n2 = n + 5;

    if (is_prime(n1, ps)) ps.emplace_back(n1);
    if (is_prime(n2, ps)) ps.emplace_back(n2);
  }

  return ps;
}

std::vector<uint32_t> prime_factors(uint32_t num,
                                    const std::vector<uint32_t>& ps) {
  std::vector<uint32_t> fs;

  while (num != 1) {
    size_t ps_idx = 0;
    for (; ps_idx < ps.size(); ++ps_idx) {
      if (num % ps[ps_idx] == 0) {
        num /= ps[ps_idx];
        fs.emplace_back(ps[ps_idx]);
        break;
      }
    }

    if (ps_idx == ps.size()) {
      fs.emplace_back(num);
      num = 1;
    }
  }

  return fs;
}

std::vector<std::pair<uint32_t, uint32_t>> cal_num_rages(
    uint32_t max_num = 4000000, uint32_t split_size = 1000) {
                    // 4294967295U
  std::vector<std::pair<uint32_t, uint32_t>> rs;

  for (uint32_t i = 0; i < split_size; ++i) {
    uint32_t begin = max_num / split_size * i;
    uint32_t end = max_num / split_size * (i + 1);
    if (i == split_size - 1) {
      end = max_num;
    }

    rs.emplace_back(begin, end);
  }

  return rs;
}

void get_primes(std::atomic_int32_t& rs_idx, const std::vector<uint32_t>& ps,
                const std::vector<std::pair<uint32_t, uint32_t>>& rs,
                uint64_t& total_ps) {
  int32_t loca_rs_idx = -1;

  while (rs_idx < static_cast<int32_t>(rs.size())) {
    std::vector<uint32_t> local_ps;
    loca_rs_idx = rs_idx++;

    uint32_t num = (rs[loca_rs_idx].first + 5) / 6 * 6;
    uint32_t end = rs[loca_rs_idx].second;

    for (; num < end; num += 6) {
      uint32_t n1 = num + 1;
      uint32_t n2 = num + 5;
      if (is_prime(n1, ps)) local_ps.emplace_back(n1);
      if (is_prime(n2, ps)) local_ps.emplace_back(n2);
    }

    while (local_ps.back() >= end) local_ps.pop_back();

    total_ps += local_ps.size();
  }
}

int main() {
  std::vector<uint32_t> ps = prime_bases();
  std::vector<std::pair<uint32_t, uint32_t>> rs = cal_num_rages();

  std::array<NumCacheLine, kThreadSize> nps;
  std::memset(static_cast<void*>(nps.data()), 0,
              nps.size() * sizeof(NumCacheLine));

  std::atomic_int32_t rs_idx{0};
  std::vector<std::thread> ts;
  for (size_t tidx = 0; tidx < kThreadSize; ++tidx) {
    // ref: C++ Concurrency in Action, p.24
    ts.emplace_back(get_primes, std::ref(rs_idx), std::cref(ps), std::cref(rs),
                    std::ref(nps[tidx].num));
  }

  for (auto& t : ts) {
    t.join();
  }

  size_t total_num_primes = 0;
  for (const auto& ns : nps) {
    total_num_primes += ns.num;
  }
  std::printf("%lu\n", total_num_primes);

  return 0;
}
