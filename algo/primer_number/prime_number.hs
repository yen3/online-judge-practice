module Prime where

prime :: Integer -> Bool
prime x 
    | x == 2 = True
    | otherwise = 
        isPrime4 x [2..sq]
        where sq = truncate (sqrt (fromIntegral x)) + 1

isPrime :: Integer -> [Integer] -> Bool
isPrime x y
    | x == 2 = True
    | otherwise =
         case y of 
             [] -> True
             y:ys -> (x `mod` y /= 0) && isPrime x ys

isPrime_2 :: Integer -> [Integer] -> Bool
isPrime_2 x y
    | x == 2 = True
    | otherwise =
        case y of
            [] -> True
            y:ys -> if y < sq then
                        if (x `mod` y == 0) then False
                        else isPrime_2 x ys
                    else True 
                    where sq = truncate (sqrt (fromIntegral x)) + 1

isPrime3 :: Integer -> [Integer] -> Bool
isPrime3 x y
    | x == 0 = False
    | x == 1 = False
    | x == 2 = True
    | otherwise =
        isPrime3' x y (truncate (sqrt (fromIntegral x)) + 1)

isPrime3' :: Integer -> [Integer] -> Integer -> Bool
isPrime3' x y sq =
    case y of
        [] -> True
        y:ys -> if y < sq then
                    if (x `mod` y == 0) then False
                    else isPrime3' x ys sq
                else True 

isPrime4 :: Integer -> [Integer] -> Bool
isPrime4 x y
    | x == 2 || y == [] = True
    | otherwise = and (map ( \e -> x `mod` e /= 0) y)
--     | otherwise = and (map (/=0) (map (x `mod`) y)) 

isPrime5 :: Integer -> [Integer] -> Bool
isPrime5 x y 
    | x == 2 = True
    | otherwise =
        isPrime5' x (takeWhile  (< (truncate (sqrt (fromIntegral x)) + 1)) y)

isPrime5' :: Integer -> [Integer] -> Bool
isPrime5' x y =
     case y of
         [] -> True
         y:ys -> (x `mod` y /= 0) && isPrime5' x ys

-- isPrime6 :: Integer -> [Integer] -> Bool
-- isPrime6 x y
--     | x == 2 = True
--     | otherwise = 
--         isPrime6' x y (truncate (sqrt (fromIntegral x)) + 1)

-- isPrime6' :: Integer -> [Integer] -> Integer -> Bool
-- isPrime6' x y sq =
--     case y of
--         [] -> True
--         y:ys -> if y < sq then
--                     if (x `mod` y == 0) then False
--                     else isPrime6' x ys sq
--                 else isPrime6' x ys sq

makePrimeList :: Integer -> [Integer]
makePrimeList x = makePrimeList_2 [2..x] []
-- makePrimeList x = makePrimeList_2 ([2, 3] ++ makeList x 5 False []) []

makePrimeList_2 :: [Integer] -> [Integer] -> [Integer]
makePrimeList_2 u v =
    case u of 
        [] -> []
        x:xs -> if isPrime3 x v then
                    [x] ++ makePrimeList_2 xs (v++[x])
                else
                    makePrimeList_2 xs v


makeList :: Integer -> Integer -> Bool -> [Integer] -> [Integer]
makeList e c b x =
    if c < e then
        if b then
            makeList e (c+4) False (x ++ [c])
        else
            makeList e (c+2) True (x ++ [c])
    else
        x

makePrimeList4 :: Integer -> [Integer]
makePrimeList4 x = [2, 3] ++ makePrimeList4' 5 x False [] 

makePrimeList4' :: Integer -> Integer -> Bool -> [Integer] -> [Integer]
makePrimeList4' c e b x
    = if c < e then
          if isPrime3 c x then
              makePrimeList4' (c+add) e (not b) (x ++ [c])
          else 
              makePrimeList4' (c+add) e (not b) x
      else x
      where add = if b then 4 else 2


isPrime7 :: Integer -> [Integer] -> Bool
isPrime7 x y 
    | x == 2 = True
    | otherwise =
        isPrime5' x (dropWhile (> (truncate (sqrt (fromIntegral x)) + 1)) y)


makePrime x = if prime (x+add) then (x+add)
            else makePrime (x+add) 
            where add = if x `mod` 6 == 1 then 4 else 2

makePrime_plusn n x = if prime (x+n) then (x+n) else makePrime_plusn n (x+6) 

merge' (x:xs) (y:ys)
    | x < y = x: merge' xs (y:ys)
    | x == y = x:merge' xs ys
    | x > y = y: merge' (x:xs) ys

primes = 5: map makePrime primes
primes2 = merge primes_plus2 primes_plus4 
    where primes_plus2 = 5: map (makePrime_plusn 6) primes_plus2 
          primes_plus4 = 7: map (makePrime_plusn 6) primes_plus4

un = 1: merge' (map (2*) un) (merge' (map (3*) un)
                                     (map (5*) un))

merge :: (Ord t) => [t] -> [t] -> [t]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys)
    | x <= y = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys
