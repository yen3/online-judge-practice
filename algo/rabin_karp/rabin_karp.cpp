#include <iostream>
#include <string>

namespace {

class RollingString {
public:
  const int kPrimeDivision = 271;
  const int kNumberBase = 256;

  int get_hash_value() { return hash_value; }

  void append(char c) {
    hash_value = (hash_value * kNumberBase + static_cast<int>(c)) %
                 kPrimeDivision;
    ++str_length;
  }

  void pop_head(char c) {
    hash_value = (hash_value - static_cast<int>(c) *
                  (pow(kNumberBase, str_length-1) % kPrimeDivision)) %
        kPrimeDivision;
    --str_length;
  }

private:
  int hash_value;
  int str_length;
};

bool is_same_hash_value_and_substr(
    const RollingString& rs, const RollingString& rt,
    const std::string& s, const std::string& t,
    int t_idx) {
  if (rs.get_hash_value() == rt.get_hash_value() &&
      s == t.substr(t_idx, s.length())) {
    return true;
  }

  return false;
}

} // namespace

int rabin_karp(const std::string& s, const std::string& t) {
  // Ignore empty string case
  if (s.empty() || t.empty()) {
    return -1;
  }

  const int p = 271;    // Prime number for division hash function

  RollingString rs;
  RollingString rt;

  for (auto c: hs) {
    rs.append(c);
  }

  for (std::size_t i = 0; i < s.size(); ++i) {
    rt.append(t[i]);
  }

  if (is_same_hash_value_and_substr(rs, rt, s, t, 0)) {
    return 0;
  }

  for (int i = s.size(); i < t.size(); ++i) {
    rt.skip(t[i - s.size()]);
    rt.append(t[i]);

    if (is_same_hash_value_and_substr(rs, rt, s, t, i)) {
      return i;
    }
  }

  return -1;
}
