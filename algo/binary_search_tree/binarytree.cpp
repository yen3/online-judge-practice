#include <iostream>

template<class T>
class TreeNode{
public: 
    T data;
    
    TreeNode<T> *right,*left;
    void assign(T d, TreeNode<T>* r=0, TreeNode<T>* l=0){
        data = d;
        right = r;
        left = l;
    };
};

template<class T>
class BinaryTree{
private:
    typedef T value_type;
    typedef TreeNode<T> data_type;
    typedef TreeNode<T>* pointer_type;
    typedef TreeNode<T>& reference_type;
public:
    BinaryTree();
    BinaryTree(T data);
    BinaryTree(pointer_type data);
//   BinaryTree<T>& operator=(BinaryTree<T>& data);
    ~BinaryTree();                  /* call private : deleteAllNode() */
      
    BinaryTree<T> LeftSubTree();
    BinaryTree<T> RightSubTree();
    T RootData();
        
    bool empty(); 
    void insert(value_type data);
    //void Delete(value_type data);
    bool find(value_type data);

    void Inorder();
    void Postorder();
    void Preorder();
    
private:      
    void Inorder(pointer_type currentNode);
    void Postorder(pointer_type currentNode);
    void Preorder(pointer_type currentNode);
    void Visit(pointer_type data,int control);

    void deleteAllNode(pointer_type currentNode);
    pointer_type root;
};


template<class T>
BinaryTree<T>::BinaryTree():root(0){   
}

template<class T>
BinaryTree<T>::BinaryTree(value_type data){
    pointer_type temp = new data_type;
    temp->assign(data);
    
    root = temp;
}

template<class T>
BinaryTree<T>::BinaryTree(pointer_type data){
    root = data;
}

template<class T>
BinaryTree<T>::~BinaryTree(){
    deleteAllNode(root);
}
/*
template<class T>
BinaryTree<T>& BinaryTree<T>::operator=(BinaryTree<T>& data){
    deleteAllNode(root);
    
}*/

template<class T>
void BinaryTree<T>::deleteAllNode(pointer_type currentNode){
    if(currentNode){
        deleteAllNode(currentNode -> left);
        deleteAllNode(currentNode -> right);
        delete currentNode;
    }
}

template<class T>
bool BinaryTree<T>::empty(){
    if(root == 0)    return true;
    else   return false;
}

template<class T>
BinaryTree<T> BinaryTree<T>::LeftSubTree(){
     return BinaryTree<T>(root->left);
}

template<class T>
BinaryTree<T> BinaryTree<T>::RightSubTree(){
     return BinaryTree<T>(root->right);
}

template<class T>
T BinaryTree<T>::RootData(){
    return root->data;
}

template<class T>
void BinaryTree<T>::Visit(pointer_type data,int control){
    /*   control == 1  印出資料 inorder postorde preorder 的member function 所使用  */
    if(control==1) std::cout << data->data << " ";
}

template<class T>
void BinaryTree<T>::Inorder(){
    Inorder(root);
}

template<class T>
void BinaryTree<T>::Inorder(pointer_type currentNode){
    if(currentNode){
        Inorder(currentNode->left);
        Visit(currentNode,1);
        Inorder(currentNode->right);
    }
}

template<class T>
void BinaryTree<T>::Postorder(){
    Postorder(root);
}

template<class T>
void BinaryTree<T>::Postorder(pointer_type currentNode){
    if(currentNode){
        Postorder(currentNode->left);
        Postorder(currentNode->right);
        Visit(currentNode,1);
    }
}

template<class T>
void BinaryTree<T>::Preorder(){
    Preorder(root);
}

template<class T>
void BinaryTree<T>::Preorder(pointer_type currentNode){
    if(currentNode){
        Visit(currentNode,1);
        Preorder(currentNode->left);
        Preorder(currentNode->right);
    }
}


template<class T>
void BinaryTree<T>::insert(value_type data){
    if(root==0){
        pointer_type temp = new data_type;
        temp->assign(data);
        root = temp;
    }
    else{
        pointer_type currentNode = root, preCurrentNode = root;
        bool control = true;  /* Ture is right, false is left  */
        while(1){
            /* 判斷二元樹的走向，並儲存上一node */
            preCurrentNode = currentNode;
            if(data < currentNode->data) {
                currentNode = currentNode -> left;
                control = false;
            }
            else { 
                currentNode = currentNode -> right;
                control = true; 
            }
            
            /* 若二元樹到底，則儲存之，用control 決定儲存左邊或右邊 */
            if(currentNode==0) {
                pointer_type AddData = new data_type;
                AddData -> assign(data);
                AddData -> data = data;
                AddData -> left = AddData -> right = 0;
                
                if(control) preCurrentNode -> right = AddData;
                else preCurrentNode -> left = AddData;
                
                break;
            }
        }
    }
}

template<class T>
bool BinaryTree<T>::find(value_type data){
    pointer_type currentNode = root;
    while(1){
        if(currentNode){
            if(currentNode -> data == data)   return true;
            else if(currentNode -> data < data) currentNode = currentNode -> right;
            else currentNode = currentNode -> left;
        }
        else   return false;
    }
}

/******************************************************************************/
