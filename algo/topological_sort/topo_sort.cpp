#include <cstdint>
#include <iostream>
#include <utility>
#include <vector>

enum class VertexState : uint32_t { kNotVisited, kVisited, kTemporaryVisited };

bool visit_nodes(uint32_t vertex, const std::vector<std::vector<bool>>& graph,
                 std::vector<VertexState>& vertex_states,
                 std::vector<uint32_t>& vertexs) {
  if (vertex_states[vertex] == VertexState::kTemporaryVisited) {
    // It's not a DAG.
    return false;
  }
  if (vertex_states[vertex] == VertexState::kVisited) {
    return true;
  }

  vertex_states[vertex] = VertexState::kTemporaryVisited;
  for (std::size_t next_vertex = 0; next_vertex < graph[vertex].size();
       ++next_vertex) {
    if (graph[vertex][next_vertex] == true) {
      std::cout << vertex << " " << next_vertex << std::endl;
      bool visit_state =
          visit_nodes(next_vertex, graph, vertex_states, vertexs);
      if (visit_state == false) {
        return false;
      }
    }
  }

  vertex_states[vertex] = VertexState::kVisited;
  vertexs.push_back(vertex);
  return true;
}

bool TopologicalSort(const std::vector<std::vector<bool>>& graph,
                     std::vector<uint32_t>& vertexs) {
  std::vector<VertexState> vertex_states(graph.size(),
                                         VertexState::kNotVisited);

  for (std::size_t vertex = 0; vertex < vertex_states.size(); ++vertex) {
    if (vertex_states[vertex] == VertexState::kNotVisited) {
      bool search_state = visit_nodes(vertex, graph, vertex_states, vertexs);
      if (search_state == false) {
        return false;
      }
    }
  }

  reverse(vertexs.begin(), vertexs.end());
  return true;
}

void test_case() {
  std::vector<std::vector<bool>> graph(6, std::vector<bool>(6, 0));
  graph[0][1] = true;
  graph[0][3] = true;
  graph[5][1] = true;
  graph[1][2] = true;
  graph[2][4] = true;
  graph[2][3] = true;
  graph[4][3] = true;

  std::vector<uint32_t> vertexs;
  bool sorte_state = TopologicalSort(graph, vertexs);

  if (sorte_state == true) {
    for (std::size_t i = 0; i < vertexs.size(); ++i) {
      std::cout << vertexs[i] << " ";
    }
    std::cout << std::endl;

  } else {
    std::cout << "It's not a DAG." << std::endl;
  }
}

int main(int argc, char* argv[]) {
  test_case();
  return 0;
}
