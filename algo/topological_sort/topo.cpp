#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
using namespace std;

void DFS(vector<int>& u, const vector<vector<int> >& m);
void _dfs(int index, vector<int>& u, vector<int>& v,
          const vector<vector<int> >& m);
void topological_sort(vector<int>& u, const vector<vector<int> >& m);

void topological_sort(vector<int>& u, const vector<vector<int> >& m) {
  DFS(u, m);
  reverse(u.begin(), u.end());
}

void DFS(vector<int>& u, const vector<vector<int> >& m) {
  vector<int> visit_node(m.size(), 0);
  for (unsigned int i = 0; i < visit_node.size(); i++) {
    if (visit_node[i] == 0) {
      _dfs(i, u, visit_node, m);
    }
  }
}

void _dfs(int index, vector<int>& u, vector<int>& v,
          const vector<vector<int> >& m) {
  vector<int> r;
  r.push_back(index);
  v[index] = 1;

  while (r.size() != 0) {
    bool control = true;
    for (unsigned int i = 0; i < m.size(); i++) {
      if (m[r.size() - 1][i] == 1 && v[i] == 0) {
        v[i] = 1;
        r.push_back(i);
        control = false;
        break;
      }
    }
    if (control) {
      u.push_back(r[r.size() - 1]);
      r.pop_back();
    }
  }
}

void input_graph(vector<vector<int> >& m) {
  int e, v;
  cin >> e >> v;
  m = vector<vector<int> >(v, vector<int>(v, 0));
  int i_m, i_n;
  for (unsigned int i = 0; i < e; i++) {
    cin >> i_m >> i_n;
    m[i_m - 1][i_n - 1] = 1;
  }
}

int main() {
  vector<vector<int> > m;
  vector<int> u;

  input_graph(m);

  topological_sort(u, m);
  copy(u.begin(), u.end(), ostream_iterator<int>(cout, " "));
  cout << endl;
}
