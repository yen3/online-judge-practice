#ifndef BT_H_
#define BT_H_

#include <string>

namespace BTREE{

const unsigned int DISK_BLOCK_SIZE = 1024; // 1*1024 
const unsigned int MINIMAL_DEGREE = 4;
const unsigned int MAX_URL_SIZE = 128; // each url maximunal size 
const unsigned int MAX_KEYS = MINIMAL_DEGREE*2-1;
const unsigned int PENDING_SIZE = DISK_BLOCK_SIZE - 16 - (MAX_URL_SIZE+4)*MAX_KEYS;

const std::string BTREEINFO="BTree.info";
const std::string NODELIST="node.list";
const std::string DELETELIST="delete.list";
const std::string DATAFILE="data.txt";

struct _bnode{
    _bnode();

    unsigned int node_num;
    int leaf;
    unsigned int key_size;
    char key[MAX_KEYS][MAX_URL_SIZE];
    int pointer[MAX_KEYS+1];
    char pending[PENDING_SIZE];        
};


class BNode{
    friend class BTree;
    friend class BNodeFactory;
public:
    BNode(const std::string& fn, int p);
    ~BNode();

    bool leaf(){ return node->leaf==1; };
    unsigned int key_size(){ return node->key_size; };
    unsigned int node_num(){ return node->node_num; };
    void set_node_num(int n){ node->node_num = n; };
    void set_leaf(bool l){ node->leaf = l;};
    void set_delete(bool d){ is_delete = d; };
    
    int insert(char* url, int p_right_index=0, int p_left_index=0);
    int erase(char* url, int erase_pointer=1);
    
    bool exsist(char* url, int& p);
    void list();

    //private data member

private:
    int location(char* url);

    _bnode* node;
    bool is_delete;
    const std::string& file_name;
};

class BNodeFactory{
public:
    BNodeFactory(std::string bif, std::string nl, std::string dl, std::string df);
    ~BNodeFactory();
    
    int get_root_num(){ return root_node_number; };
    void set_root_num(int p);
    BNode* get_root_node(){ return get_node(get_root_num()); };
    
    BNode* get_node(int p);
    void erase_node(int p);
    
    BNode* get_new_node();
    BNode* get_new_root_node();
    
    void total_keys_counter(bool add);
    
    void list();
private:
    bool node_exsist(int p);
    
    const std::string bnodeinfo;
    const std::string nodelist;
    const std::string deletelist;
    const std::string datafile;
    
    const static int NODE_SIZE=1024;
    unsigned int node[NODE_SIZE];
    unsigned int delete_node[NODE_SIZE];
    unsigned int node_size;
    unsigned int delete_node_size;
    unsigned int next_node_number;
    
    unsigned int root_node_number;
    unsigned int minimal_degree;
    unsigned int total_keys;
    unsigned int height;
};

class BTree{
public:
    BTree(std::string bif, std::string nl, std::string dl, std::string df):BNF(bif, nl, dl, df){};
    bool insert(char* url);
    bool erase(char* url);
    bool query(char* url);
    void list();
private:
    void spilit(BNode* next_node, BNode* node,int mode);
    void fill_node(BNode* next_node, BNode* node, int index, int mode);
    BNodeFactory BNF;        
};

};

#endif /*BT_H_*/
