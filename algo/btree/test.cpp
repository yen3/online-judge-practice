#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

#define MAX_PATH_LEN 500

int inode[100];
int count = 0;
int size = 0;

int Check(struct stat* buf) {
    int i;
    int check = 1; //check if buf is in inode array
    for (i = 0; i < count; i++) {
        if (inode[i] == buf->st_ino) {
            check = 0;//buf is in inode array
            break;
        }
    }

    if (check == 1) {
        inode[count] = buf->st_ino;
        count++;
        if (!S_ISDIR(buf->st_mode))
            size += buf->st_size;
    }
    return check;
}

void privilege(struct stat* buf, char* priv) {
    char oct[9];
    sprintf(oct, "%o", buf->st_mode);

    strcpy(priv, "---------");
    int i, j;
    for (i = strlen(oct) - 3, j = 0; i < strlen(oct) && j < 9; i++, j += 3) {
        if ((oct[i] - '0') & 4 == 4)
            priv[j] = 'r';
        if ((oct[i] - '0') & 2 == 2)
            priv[j + 1] = 'w';
        if ((oct[i] - '0') & 1 == 1)
            priv[j + 2] = 'x';
    }
}

void catelog(char* cat, char* name) {
    strcat(cat, "/");
    strcat(cat, name);
}

void print_dir(char* path) {
    printf(":%s\n\n", path);
    char PATH[MAX_PATH_LEN];
    strcpy(PATH, path);

    struct stat buf; //儲存檔案的狀態
    stat(PATH, &buf);

    DIR * dp; //指向dir的pointer
    struct dirent *entry;//指向dir內的entry

    printf(
            "\tmode\t\towner\tgroup\tinode\tsize\tn_link\tlast modify time\tname\n");
    if (S_ISDIR(buf.st_mode)) {
        dp = opendir(PATH);
        char path_temp[MAX_PATH_LEN];
        strcpy(path_temp, PATH);

        while ((entry = readdir(dp)) != NULL) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..")
                    != 0) {
                char cat[MAX_PATH_LEN];
                strcpy(cat, path_temp);

                catelog(cat, entry->d_name);
                stat(cat, &buf);
                char privi[10];

                privilege(&buf, privi);

                if (S_ISDIR(buf.st_mode)) {
                    printf("\t%s\t%s\t%s\t%d\t<dir>\t%d\t%s\t%s\n", privi,
                            getpwuid(buf.st_uid)->pw_name,
                            getgrgid(buf.st_gid)->gr_name, buf.st_ino,
                            buf.st_nlink, ctime(&buf.st_mtime), entry->d_name);
                    printf("\n\n");
                    if (Check(&buf))
                        print_dir(cat);
                } else {
                    printf("\t%s\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", privi,
                            getpwuid(buf.st_uid)->pw_name,
                            getgrgid(buf.st_gid)->gr_name, buf.st_ino,
                            buf.st_size, buf.st_nlink, ctime(&buf.st_mtime),
                            entry->d_name);
                    Check(&buf);
                }
            }
        }
        printf("\n\n");
    }
}

main() {
    char path[MAX_PATH_LEN];
    getcwd (path, MAX_PATH_LEN);
    print_dir(path);
    printf("total size=%d\n\n", size);
}

