#include <iostream>
#include <string>
#include <fstream>

#include <cstring>
#include <cstdio>

#include <fcntl.h>
#include <unistd.h>
#include "bt.h"

#ifdef BT_H_

using namespace std;

namespace BTREE{


void swap(char* x, char* y){
    char* temp = new char[std::strlen(x)+1];
    memset(temp, 0, (std::strlen(x)+1)*sizeof(char));
    
    std::strcpy(temp, x);
    std::strcpy(x, y);
    std::strcpy(y, temp);
    delete temp;
}


/**********************************/

_bnode::_bnode():node_num(0), leaf(1), key_size(0){
    std::memset(pointer, 0, sizeof(int)*(MAX_KEYS+1));
    std::memset(key, 0, sizeof(char)*MAX_KEYS*MAX_URL_SIZE);
}

/********************************/

BNode::BNode(const std::string& fn, int p=0):is_delete(false), file_name(fn){
    --p;
    node = new _bnode();
    if(p!=-1 && file_name.c_str()){
        int fd = open(file_name.c_str(), O_RDONLY);
        if(fd>0){
            lseek(fd, p*sizeof(_bnode), SEEK_SET);
            read(fd, (void*)node, sizeof(_bnode));
            close(fd);
        }
        else std::cout << "Read Node error on file name:" << file_name << std::endl;    
    }
}

BNode::~BNode(){
    if(!is_delete){
        int p = node->node_num-1;
        
        // if the file does't exsist, we creat the file. 
        int fd = open(file_name.c_str(), O_WRONLY);
        if(fd==-1){
            close(fd);
            fd = open(file_name.c_str(), O_WRONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        }
        
        lseek(fd, p*sizeof(_bnode), SEEK_SET);
        if(fd>0){
            write(fd, (void*)node, sizeof(_bnode));
            close(fd);
        }    
    }
    delete node;
}

//BNode::exisit()
//如果存在傳回該url存在的index
//如果不存在，傳回下一個該尋找的node number
bool BNode::exsist(char* url, int& p){
    int index = location(url);
    if(strcmp(url, node->key[index])==0){
        p = index;
        return true;
    }
    else{
        p = node->pointer[index];
        return false;
    }
}

// -1: exsist, 0: insert sucess, another: index (not leaf)
int BNode::insert(char* url, int p_right_index, int p_left_index){
    int p = location(url);
    
    if(std::strcmp(node->key[p], url)==0) return -1;
    else{
        for(int i=node->key_size;i>p;i--)   strcpy(node->key[i], node->key[i-1]);
        for(int i=node->key_size+1;i>p;i--) node->pointer[i] = node->pointer[i-1];
        std::strcpy(node->key[p], url);
        if(p_right_index!=0)   node->pointer[p+1] = p_right_index;
        if(p_left_index!=0)    node->pointer[p] = p_left_index;
        
        node->key_size++;
        return 0;
    }
}

//erase_pointer = 1, erase right pointer
//erase_pointer = 0, erase left pointer
int BNode::erase(char* url, int erase_pointer){
    int p = location(url);
    if(std::strcmp(node->key[p], url)!=0) return -1;
    else{
        for(unsigned int i=p;i<node->key_size-1;i++) strcpy(node->key[i], node->key[i+1]);
        for(unsigned int i=p+erase_pointer;i<node->key_size;i++) node->pointer[i] = node->pointer[i+1];
        
        node->key_size--;
        return 0;
    }
}

int BNode::location(char* url){
    unsigned int i=0;
    for(;i<node->key_size;i++){
        if(strcmp(url, node->key[i])<=0)   return i;
    }
    return i;
    /*
    int begin=0, end=node->key_size, mid=0, cmp=0;
    while(end-begin>1){
        mid = (begin+end)/2;
        
        cmp = std::strcmp(url, node->key[mid]);
        if(cmp==0) return mid;
        else if(cmp>0) begin = mid+1;
        else end = mid-1;
    }
    return begin;
    */    
}

void BNode::list(){
    std::cout << node->node_num << " " << node->leaf << " " << node->key_size << " (n, isleaf, keysize)" << std::endl;
    std::cout << "key:" << std::endl;
    for(unsigned int i=0;i<node->key_size;i++){
        std::cout << "   " << node->key[i] << std::endl;
    }
    std::cout << "pointer:" << std::endl << "   ";
    for(unsigned int i=0;i<node->key_size+1;i++){
        std::cout << node->pointer[i] << " ";
    }
    std::cout << std::endl << std::endl;
}

/*******************************************************/

BNode* BNodeFactory::get_node(int p){
    if(node_exsist(p))   return new BNode(datafile, p);
    else return NULL;
}

BNode* BNodeFactory::get_new_node(){
    BNode* new_node = new BNode(datafile);
    if(delete_node_size>0){
        new_node->set_node_num(delete_node[--delete_node_size]);
    }
    else{
        node[node_size++] = next_node_number++;
        new_node->set_node_num(node[node_size-1]);
    }
    return new_node;
}

BNode* BNodeFactory::get_new_root_node(){
    BNode* node = get_new_node();
    root_node_number = node->node_num();
    height++;
    return node;
}

void BNodeFactory::set_root_num(int p){
    root_node_number = p;
    height--;
}

void BNodeFactory::erase_node(int p){
    int index=-1;
    delete_node[delete_node_size++] = p;
    for(unsigned int i=0;i<node_size;i++){
        if(p==node[i]){
            index = i;
            break;
        }
    }
    if(index!=-1){
        node[index] = node[--node_size];
    }
}

void BNodeFactory::total_keys_counter(bool add){
    total_keys += (add)?1:-1;
}

bool BNodeFactory::node_exsist(int p){
    for(unsigned int i=0; i<node_size; i++){
        if(node[i]==p) return true;
    }
    return false;
}

BNodeFactory::BNodeFactory(std::string bif, std::string nl, std::string dl, std::string df):
    bnodeinfo(bif), nodelist(nl), deletelist(dl),datafile(df){
    
    std::ifstream infile;
    // read btree.infro
    infile.open(bnodeinfo.c_str());
    if(!infile.fail()){
        infile >> minimal_degree >> total_keys >> height >>
                  root_node_number >> node_size >> delete_node_size >>
                  next_node_number;
    }
    infile.close();
    infile.clear();
    
    //read node list
    infile.open(nodelist.c_str());
    if(!infile.fail()){
        for(unsigned int i=0;i<node_size;i++) infile >> node[i];
    }
    infile.close();
    infile.clear();
    
    // read delete node list
    infile.open(deletelist.c_str());
    if(!infile.fail()){
        for(unsigned int i=0;i<delete_node_size;i++) infile >> delete_node[i];
    }
    infile.close();
    infile.clear();    
}

BNodeFactory::~BNodeFactory(){
    std::ofstream outfile;
    
    //write btree.info
    outfile.open(bnodeinfo.c_str(), std::ofstream::out);
    if(!outfile.fail()){
        outfile << minimal_degree << " " << total_keys << " " << height << " " <<
                   root_node_number << " " << node_size << " " << delete_node_size << " "<<
                   next_node_number << " " << std::endl;
    }
    outfile.close();
    outfile.clear();
    
    //write node list
    outfile.open(nodelist.c_str(), std::ofstream::out);
    if(!outfile.fail()){
        for(unsigned int i=0;i<node_size;i++) outfile << node[i] << " ";
    }
    outfile.close();
    outfile.clear();
    
    //write delete node list
    outfile.open(deletelist.c_str(), std::ofstream::out);
    if(!outfile.fail()){
        for(unsigned int i=0;i<delete_node_size;i++) outfile << delete_node[i] << " ";    
    }
    outfile.close();
    outfile.clear();
}


void BNodeFactory::list(){
    /*
    std::cout << minimal_degree << " " << total_keys << " " << height << " " <<
            root_node_number << " " << node_size << " " << delete_node_size << " " <<
            next_node_number << std::endl;
    */
    std::cout << "Minimal Degree: " << minimal_degree << std::endl
              << "Total Keys: " << total_keys << std::endl
              << "Height: " << height << std::endl
              << "Root Node Number" << root_node_number << std::endl;
    
    std::cout << "node number list(size: " <<  node_size << "):" << std::endl;
    for(unsigned int i=0;i<node_size;i++) cout << node[i] << " ";
    std::cout << std::endl;
    std::cout << "delete node number list:(size: " << delete_node_size << ")" << std::endl;
    for(unsigned int i=0;i<delete_node_size;i++) cout << delete_node[i] << " ";
    std::cout << std::endl;
} 

/*********************************/


void BTree::list(){
    BNF.list();
}

bool BTree::query(char* url){
    BNode* node = BNF.get_root_node();
    
    int p=0;
    while(1){
        if(node->exsist(url, p)){
            return true;
        }
        else{
            if(node->leaf()) return false;
            else{
                delete node;
                node = BNF.get_node(p);
            }
        }
    }
}
 
// mode = 0  leaf divide
// mode = 1  root divide
void BTree::spilit(BNode* next_node, BNode* node, int mode=0){
    BNode* right = BNF.get_new_node();
    right->set_leaf(next_node->leaf());
    //copy keys, pointers 
    for(unsigned int i=0;i<MINIMAL_DEGREE-1;i++)    std::strcpy(right->node->key[i], next_node->node->key[MINIMAL_DEGREE+i]);
    for(unsigned int i=0;i<MINIMAL_DEGREE;i++)    right->node->pointer[i] = next_node->node->pointer[MINIMAL_DEGREE+i];
    next_node->node->key_size = right->node->key_size = MINIMAL_DEGREE-1;
    
    //set paraent node
    if(mode==0){
        node->insert(next_node->node->key[MINIMAL_DEGREE-1], right->node_num());
        //delete node;
    }
    else{
        BNode* root = BNF.get_new_root_node();
        root->set_leaf(false);
        root->insert(next_node->node->key[MINIMAL_DEGREE-1], right->node_num(), next_node->node_num());
        delete root;
        
    }
    delete right;
}

bool BTree::insert(char* url){
    BNode* node = BNF.get_root_node();
    BNode* next_node = 0;
    int p=0;

    if(node->key_size()==MAX_KEYS){
        spilit(node, 0, 1); //root mode
        
        delete node;
        node = BNF.get_root_node();
    }

    while(1){
        if(node->exsist(url, p)){
            return false; //如果不存在就傳回下一個節點。
        }
        else{
            if(node->leaf()){
                node->insert(url);
                BNF.total_keys_counter(true);
                delete node;
                return true;
            }
            else{
                next_node = BNF.get_node(p);
                if(next_node->key_size()==MAX_KEYS){
                    spilit(next_node, node, 0); // leaf mode
                    delete next_node;
                }
                else{
                    delete node;
                    node = next_node;
                }
            }
        }
    }
}

//mode = 0 normal case
//mode = 1 special case
void BTree::fill_node(BNode* next_node, BNode* node, int index, int mode=0){
    BNode* left_next_node=NULL;
    BNode* right_next_node=NULL;
    
    if(index-1>=0)        left_next_node = BNF.get_node(node->node->pointer[index-1]);
    if(index+1<=node->key_size()) right_next_node = BNF.get_node(node->node->pointer[index+1]);
    
    // left rotation
    if(right_next_node && right_next_node->key_size()>=MINIMAL_DEGREE){
        //next_node setting
        std::strcpy(next_node->node->key[next_node->node->key_size], node->node->key[index]);
        next_node->node->pointer[next_node->node->key_size+1] = right_next_node->node->pointer[0];
        next_node->node->key_size++;

        //node setting
        std::strcpy(node->node->key[index], right_next_node->node->key[0]);
        
        //right_next_node setting
        for(unsigned int i=0;i<right_next_node->node->key_size-1;i++){
            std::strcpy(right_next_node->node->key[i], right_next_node->node->key[i+1]);
        }
        for(unsigned int i=0;i<right_next_node->node->key_size;i++){
            right_next_node->node->pointer[i] = right_next_node->node->pointer[i+1];
        }
        right_next_node->node->key_size--;
    }
    // right rotation
    else if(left_next_node && left_next_node->key_size()>=MINIMAL_DEGREE){
        if(index == node->key_size()) index--;
            
        //next_node setting
        for(int i=next_node->node->key_size;i>0;i--){
            std::strcpy(next_node->node->key[i], next_node->node->key[i-1]);
        }
        for(int i=next_node->node->key_size+1;i>0;i--){
            next_node->node->pointer[i] = next_node->node->pointer[i-1];
        }
        std::strcpy(next_node->node->key[0], node->node->key[index]);
        next_node->node->pointer[0] = left_next_node->node->pointer[left_next_node->node->key_size];
        next_node->node->key_size++;
        
        //node setting
        std::strcpy(node->node->key[index], left_next_node->node->key[left_next_node->node->key_size-1]);
        
        //left_next_node setting
        left_next_node->node->key_size--;
    }
    else if(right_next_node || left_next_node){
        bool erase_state = true;  // true is right, false is left
        BNode* erase_node = NULL;
        
        // merge with right
        if(right_next_node && right_next_node->key_size()==MINIMAL_DEGREE-1){
            erase_state = true;
            erase_node = right_next_node;
            
            cout << "test left" << endl;
            cout << right_next_node -> key_size() << " " << next_node ->key_size() << endl;
            
            // next_node add paraent key[index]
            
            std::strcpy(next_node->node->key[next_node->node->key_size], node->node->key[index]);
            
            // next_node merge right node
            //for(unsigned int i=next_node->node->key_size+1, j=0; j<right_next_node->node->key_size; i++,j++){
            for(unsigned int i=MINIMAL_DEGREE, j=0; j<right_next_node->node->key_size; i++,j++){
                std::strcpy(next_node->node->key[i], right_next_node->node->key[j]);
            }
            
            for(unsigned int i=next_node->node->key_size+1, j=0; j<right_next_node->node->key_size+1; i++, j++){
                next_node->node->pointer[i] = right_next_node->node->pointer[j];
            }
            next_node->node->key_size += 1 + right_next_node->node->key_size;
    
        } 
        // merge with left
        else if(left_next_node && left_next_node->key_size()==MINIMAL_DEGREE-1){
            if(index==node->key_size()) index--;
                
            erase_state = false;
            erase_node = left_next_node;

            //move the original to key_size+1 ~ end;
            for(unsigned int i=0; i<next_node->node->key_size; i++){
                std::strcpy(next_node->node->key[i+left_next_node->node->key_size+1], next_node->node->key[i]);
            }
            for(unsigned int i=0; i<next_node->node->key_size+1; i++){
                next_node->node->pointer[i+left_next_node->node->key_size+1] = next_node->node->pointer[i];
            }
            
            //next node merge left_next_node
            for(unsigned int i=0; i<left_next_node->node->key_size; i++){
                std::strcpy(next_node->node->key[i], left_next_node->node->key[i]);
            }
            for(unsigned int i=0; i<left_next_node->node->key_size+1; i++){
                next_node->node->pointer[i] = left_next_node->node->pointer[i];    
            }

            //next_next add paraent key[index]
            cout << node -> node ->key[index] << endl;
            std::strcpy(next_node->node->key[next_node->node->key_size], node->node->key[index]);
            
            //change the next_node key_size to MAX_KEYS
            next_node->node->key_size += 1 + left_next_node->node->key_size;
        }
        else{
            std::cout << "fill node merge error" << std::endl;
            return;
        }
        
        // node erase the key;
        char temp[MAX_URL_SIZE];
        strcpy(temp, node->node->key[index]);
        node->erase(temp, (erase_state)?1:0);
        
        //delete right_next_node
        BNF.erase_node(erase_node->node_num());
        erase_node->set_delete(true);
        
        //special case for root
        if(mode==1 && node->key_size()==0){
            node->set_delete(true);
            BNF.erase_node(node->node_num());
            BNF.set_root_num(next_node->node_num());
            
            delete node;
            node = BNF.get_root_node();
        }
        
    }
    else{
        std::cout << "fill node error" << std::endl;
    }
    
    if(left_next_node){
        delete left_next_node;
    }
    if(right_next_node){
        delete right_next_node;
    }
}

bool BTree::erase(char* url){
    BNode* node = BNF.get_root_node();
    BNode* next_node = NULL;
    
    int p=0;
    bool key_exsist = false;
    while(1){
         key_exsist = node->exsist(url, p);

         //prepare work
         next_node = BNF.get_node((key_exsist)? node->node->pointer[p]: p);   // 如果key存在的話，p為key's index，需改指成node->pointer[p]，方為下一個index

         if(next_node && next_node->key_size()<MINIMAL_DEGREE){
             // 1: special case for root
             // 0: normal case
             bool root_state = node->node_num()==BNF.get_root_num();
             fill_node(next_node, node, node->location(url),
                       (root_state)?1:0);

             
             int node_num=0;
             node_num = node->node_num();
             delete node;
             delete next_node;
             
             node = BNF.get_node(node_num);
             key_exsist = node->exsist(url, p);
             next_node = BNF.get_node((key_exsist)? node->node->pointer[p]: p);
         }        
         
         if(node->leaf()){
             if(key_exsist){
                 node->erase(url);
                 delete node;
                 BNF.total_keys_counter(false);
                 
                 return true;                 
             }
             else  return false;
         }
         else{
             //erase work
             if(key_exsist){
                 // paraent and leaf swap
                 if(strcmp(node->node->key[p], next_node->node->key[node->key_size()-1])>0){
                     swap(next_node->node->key[next_node->key_size()-1], node->node->key[p]);
                 }
                 else{
                     swap(next_node->node->key[0], node->node->key[p]);
                 }
             }
             //else ;
             
             delete node;
             node = next_node;
         }
     }
}

};

#endif
