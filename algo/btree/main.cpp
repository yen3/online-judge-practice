#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include "bt.h"
using namespace std;

namespace BTREE{
void init_envior(const string& btreeinfo, const string& nodelist, const string& deletelist, const string& datafile){

    ifstream infile;
    ofstream outfile;
    infile.open(btreeinfo.c_str());
    if(infile.fail()){
        outfile.open(btreeinfo.c_str());
        outfile << MINIMAL_DEGREE << " 0 1 1 1 0 2";
        outfile.close();
        outfile.clear();
    }
    infile.close();
    infile.clear();
    
    infile.open(nodelist.c_str());
    if(infile.fail()){
        outfile.open(nodelist.c_str());
        outfile << "1 ";
        outfile.close();
        outfile.clear();
    }
    infile.close();
    infile.clear();
    
    infile.open(deletelist.c_str());
    if(infile.fail()){
        outfile.open(deletelist.c_str());
        outfile.close();
        outfile.clear();
    }
    infile.close();
    infile.clear();
    
    infile.open(datafile.c_str());
    if(infile.fail()){
        BNode* root = new BNode(datafile, 0);
        root->set_node_num(1);
        root->set_leaf(true);
        
        delete root;
    }
    infile.close();
    infile.clear();
}

};

inline void process_command_insert(char* url, BTREE::BTree& btree){
    if(btree.insert(url))   cout << "OK, insert: " << url << endl;
    else cout << "Error, insert: " << url << endl;    
}

inline void process_command_delete(char* url, BTREE::BTree& btree){
    if(btree.erase(url))   cout << "OK, delete: " << url << endl;
    else cout << "Error, insert: " << url << endl;    
}

inline void process_command_query(char* url, BTREE::BTree& btree){
    if(btree.query(url))   cout << "O\t" << url << endl;
    else cout << "X\t" << url << endl;    
}

bool process_command(int argc, char* argv[], BTREE::BTree& btree){
    if(argc==2){
        btree.list();
    }
    else{
        if(strlen(argv[1])==2 && argv[1][0]=='-'){
            char* url = argv[2];
            bool char_heigher=false;
            void (*process_sub_command)(char*, BTREE::BTree&) = NULL;
            
            if(argv[1][1]>='A' && argv[1][1]<='Z'){
                char_heigher=true;
                argv[1][1]+=32;
            }
            
            if(argv[1][1]=='a')       process_sub_command = &process_command_insert;
            else if(argv[1][1]=='d')  process_sub_command = &process_command_delete; 
            else if(argv[1][1]=='q')  process_sub_command = &process_command_query;
            else    return false;
            
            if(!char_heigher){
                (*process_sub_command)(url, btree);
            }
            else{
                ifstream infile;
                infile.open(argv[2]);
                
                if(infile.fail()){
                    infile.close();
                    infile.clear();
                    return false;
                }
                
                char temp[BTREE::MAX_URL_SIZE];
                while(!infile.eof()){
                    infile >> temp;
                    if(!infile.eof())   (*process_sub_command)(temp, btree);
                }
            }
        }
        
        else return false;
    }
    return true;
}

int main(int argc, char* argv[]){
    cout << BTREE::PENDING_SIZE << endl;
    string btree_dir = (argc==2)? argv[1]: (argc==4)? argv[3]: ".";
    string btreeinfo = btree_dir + "/" + BTREE::BTREEINFO;
    string nodelist = btree_dir + "/"+ BTREE::NODELIST;
    string deletelist = btree_dir + "/" + BTREE::DELETELIST;
    string datafile = btree_dir + "/" + BTREE::DATAFILE;
    
    if(argc>=2 && argc<=4){
        BTREE::init_envior(btreeinfo, nodelist, deletelist, datafile);
        BTREE::BTree btree(btreeinfo, nodelist, deletelist, datafile);
        btree.list();
        
        //if(!process_command(argc, argv, btree)) cout << "commnad error" << endl;
    }
    else{
        cout << "command format error" << endl;
    }
    cout << "success" << endl;
    
}
