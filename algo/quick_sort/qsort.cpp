#include <cassert>
#include <cstdint>
#include <iostream>
#include <utility>
#include <vector>

void qsort(std::vector<int>& xs, uint32_t left, uint32_t right) {
  if (left >= right) {
    return;
  }

  int pivot = xs[left];

  uint32_t i = left + 1;
  uint32_t j = right;

  while (1) {
    for (; xs[i] < pivot; ++i)
      ;
    for (; xs[j] > pivot; --j)
      ;

    if (i > j) break;

    std::swap(xs[i], xs[j]);
  }

  std::swap(xs[left], xs[j]);

  qsort(xs, left, j - 1);
  qsort(xs, j + 1, right);
}

void test_case() {
  std::vector<int> xs{7, 5, 8, 9, 1, 3, 6, 4, 2, 10, 15, 22, 33};
  std::vector<int> ans{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 22, 33};

  for (auto x : xs) std::cout << x << " ";
  std::cout << std::endl;

  qsort(xs, 0, xs.size() - 1);

  assert(xs.size() == ans.size());
  for (std::size_t i = 0; i < xs.size(); ++i) {
    assert(xs[i] == ans[i]);
  }

  for (auto x : xs) std::cout << x << " ";
  std::cout << std::endl;
}

int main() {
  test_case();
  return 0;
}
