#include <iostream>
#include <vector>
#include <cassert>

int binary_search(int n, int* nums, int nums_size) {
  int begin = 0;
  int end = nums_size - 1;

  while (begin <= end) {
    int mid = (end - begin) / 2 + begin;

    if (nums[mid] == n)     return mid;
    else if (nums[mid] < n) begin = mid + 1;
    else                    end = mid - 1;
  }

  return -1;
}

int binary_search_shift(int n, int* nums, int nums_size, int shift = 0) {
  int begin = 0;
  int end = nums_size - 1;

  while (begin <= end) {
    int mid = (end - begin) / 2 + begin;
    int mid_real = (mid + shift) % nums_size;  // shift the index

    if (nums[mid_real] == n)     return mid_real;
    else if (nums[mid_real] < n) begin = mid + 1;
    else                         end = mid - 1;
  }

  return -1;
}

int GetShiftFromRotatedSortedArray(int* nums, int nums_size) {
  int shift = 0;
  for (; shift + 1 < nums_size && nums[shift] < nums[shift+1]; shift++) ;

  return (shift == nums_size - 1)? 0: (shift+1);
}

int main() {
  {
    int a[] = {1, 2, 3, 4, 5};

    assert(binary_search(0, a, 5) == -1);
    assert(binary_search(1, a, 5) == 0);
    assert(binary_search(2, a, 5) == 1);
    assert(binary_search(3, a, 5) == 2);
    assert(binary_search(4, a, 5) == 3);
    assert(binary_search(5, a, 5) == 4);
    assert(binary_search(6, a, 5) == -1);
  }

  {
    int a[] = {1, 2, 3, 4, 5};

    int shift = GetShiftFromRotatedSortedArray(a, 5);
    assert(shift == 0);

    assert(binary_search_shift(0, a, 5, shift) == -1);
    assert(binary_search_shift(1, a, 5, shift) == 0);
    assert(binary_search_shift(2, a, 5, shift) == 1);
    assert(binary_search_shift(3, a, 5, shift) == 2);
    assert(binary_search_shift(4, a, 5, shift) == 3);
    assert(binary_search_shift(5, a, 5, shift) == 4);
    assert(binary_search_shift(6, a, 5, shift) == -1);
  }

  {
    int a[] = {3, 4, 5, 1, 2};

    int shift = GetShiftFromRotatedSortedArray(a, 5);
    assert(shift == 3);

    assert(binary_search_shift(0, a, 5, shift) == -1);
    assert(binary_search_shift(1, a, 5, shift) == 3);
    assert(binary_search_shift(2, a, 5, shift) == 4);
    assert(binary_search_shift(3, a, 5, shift) == 0);
    assert(binary_search_shift(4, a, 5, shift) == 1);
    assert(binary_search_shift(5, a, 5, shift) == 2);
    assert(binary_search_shift(6, a, 5, shift) == -1);
  }
}
