#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

void print_mcm(int i, int j, vector<vector<int> >& s);
void mcm(vector<int>& p, vector<vector<int> >& m, vector<vector<int> >& s);    
    
int main(){
    int temp[] = {5, 10, 3, 12, 5, 50, 6};
    vector<int> p(temp, temp+7);
    for(unsigned int i=0;i<p.size();i++) cout << p[i] << " ";
    cout << endl;

    vector<vector<int> > m(p.size()-1, vector<int>(p.size()-1, 0));
    vector<vector<int> > s(p.size()-1, vector<int>(p.size()-1, 0));
    mcm(p, m, s);
    print_mcm(0, 5, s);
    cout << endl;

    for(unsigned int i=0;i<m.size();i++){
        for(unsigned int j=0;j<m[i].size();j++){
            cout << setw(5) << m[i][j];
	    }
        cout << endl;
    }
    cout << endl; 
    for(unsigned int i=0;i<s.size();i++){
        for(unsigned int j=0;j<s[i].size();j++){
            cout << setw(5) << s[i][j];
        }
	    cout << endl;
    }    
}

void print_mcm(int i, int j, vector<vector<int> >& s){
    if(i==j){
        cout << i << " ";
    }
    else{
        cout << "( ";
        print_mcm(i, s[i][j], s);
        print_mcm(s[i][j]+1, j, s);
        cout << ")";
    }
}

void mcm(vector<int>& p, vector<vector<int> >& m, vector<vector<int> >& s){    
    for (unsigned int i=0; i<m.size(); i++)
        m[i][i] = 0;

    for (unsigned int l=1; l<=m.size(); l++) {
        for (unsigned int i=0; i<m.size()-l; i++) {
            int j = i+l;
            for (int k=i; k<=j-1; k++) {
                int q = m[i][k] + m[k+1][j] + p[i] * p[k+1] * p[j+1];
                if(m[i][j]==0 || q<m[i][j]){
                    m[i][j] = q;
                    s[i][j] = k;
                }
            }
        }
    }
}
