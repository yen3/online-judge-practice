#include <iostream>
#include <string>
#include <vector>
using namespace std;

void LCS(string& s, const string& x, const string& y);
void _lcs(vector<vector<int> >& c, vector<vector<int> >& b, const string& x, const string& y);
void produce_LCS(int i, int j, string& s, const string& x, const vector<vector<int> >& b);

void LCS(string& s, const string& x, const string& y){
    vector<vector<int> > c, b;
    _lcs(c, b, x, y);
    produce_LCS(x.length(), y.length(), s, x, b);
    cout << s << endl;
}

void _lcs(vector<vector<int> >& c, vector<vector<int> >& b, const string& x, const string& y){
    c = vector<vector<int> >(x.length()+1, vector<int>(y.length()+1, 0));
    b = vector<vector<int> >(x.length()+1, vector<int>(y.length()+1, 0));

    for(unsigned int i=0;i<c.size();i++) c[i][0]=0;
    for(unsigned int i=0;i<c[0].size();i++) c[0][i]=0;

    /* b: 0 向左 1 向上 2 斜左上 */
    for(unsigned int i=1;i<c.size();i++){
        for(unsigned int j=1;j<c[i].size();j++){
	    if(x[i-1]==y[j-1]){
	        c[i][j] = c[i-1][j-1]+1;
		b[i][j] = 2;
	    }
	    else if(c[i-1][j]>=c[i][j-1]){
	        c[i][j] = c[i-1][j];
		b[i][j] = 1;
	    }
	    else{
	        c[i][j] = c[i][j-1];
		b[i][j] = 0;
	    }
	}
    }

    for(unsigned int i=0;i<c.size();i++){
        for(unsigned int j=0;j<c[i].size();j++){
	    cout << c[i][j] << " ";
	}
	cout << endl;
    }
    cout << endl;

    for(unsigned int i=0;i<b.size();i++){
        for(unsigned int j=0;j<b[0].size();j++){
	    cout << b[i][j] << " ";
	}
	cout << endl;
    }
}

void produce_LCS(int i, int j, string& s, const string& x, const vector<vector<int> >& b){
    if(i==0 || j==0) return;
    
    if(b[i][j]==2){
	produce_LCS(i-1,j-1, s, x, b);
        s+=x[i-1];
    }
    else if(b[i][j]==1)   produce_LCS(i-1, j, s, x, b);
    else produce_LCS(i, j-1, s, x, b);
}

int main(){
    string x="100101011001";
    string y="010110110110";
    /*
    string x="ABCBDAB";
    string y="BDCABA";
    */
    string s;
    LCS(s, x, y);


}
