#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <utility>
using namespace std;

void produce_maze(vector<vector<char> >& maze){
    ifstream in;
    in.open("maze.txt");
    
    int x, y;
    if(!in.fail())  in >> x >> y;
    
    for(unsigned int i=0;i<x+2;i++)   maze.push_back(vector<char>(y+2,0));
    for(unsigned int i=1;i<maze.size()-1;i++){
        for(unsigned int j=1;j<maze[i].size()-1;j++){
            in >> maze[i][j];
        }
    }
    in.close();
    
    /* set the wall */
    for(unsigned int i=0;i<maze[0].size();i++)  maze[0][i] = maze[maze.size()-1][i] = '1';
    for(unsigned int i=0;i<maze.size();i++)     maze[i][0] = maze[i][maze[i].size()-1] = '1';

    for(unsigned int i=0;i<maze.size();i++){
        for(unsigned int j=0;j<maze[i].size();j++){
            cout << maze[i][j] << " ";
        }
        cout << endl;
    }
}

bool run_maze(vector<pair<int, int> >& s, vector<vector<char> >& m){
    vector<pair<int,int> > direction;
    direction.push_back(make_pair(0,1));    direction.push_back(make_pair(0,-1));
    direction.push_back(make_pair(1,1));    direction.push_back(make_pair(1,0));
    direction.push_back(make_pair(1,-1));   direction.push_back(make_pair(-1,1));
    direction.push_back(make_pair(-1,-1));  direction.push_back(make_pair(-1,0));
    
    s.push_back(make_pair(1,1));
    bool find_next = false;
    while(true){
        //cout << s[s.size()-1].first << " " << s[s.size()-1].second << endl;        
        for(unsigned int i=0;i<direction.size();i++){
            if(m[s[s.size()-1].first+direction[i].first][s[s.size()-1].second+direction[i].second]=='0'){
                m[s[s.size()-1].first+direction[i].first][s[s.size()-1].second+direction[i].second]= 'r';
                s.push_back(make_pair(s[s.size()-1].first+direction[i].first,
                                      s[s.size()-1].second+direction[i].second));
                find_next = true;
                break;
            }
            else if(m[s[s.size()-1].first+direction[i].first][s[s.size()-1].second+direction[i].second]=='E'){
                s.push_back(make_pair(s[s.size()-1].first+direction[i].first,
                                      s[s.size()-1].second+direction[i].second));
                return true;
            }
        }
        if(find_next){
            find_next = false;
        }
        else{
            s.pop_back();
            if(s.size()==0) return false;
        }
    }
}

int main() {
    vector<vector<char> > maze;
    vector<pair<int,int> > stack;
    
    produce_maze(maze);
    run_maze(stack, maze);
 
    for(unsigned int i=0;i<stack.size();i++){
        cout << "(" << setw(2)  << stack[i].first << ", " << setw(2) << stack[i].second << ") ";
        if((i+1)%5==0) cout << endl;
            
    }
    cout << "Success Maze" << endl;
    cout << endl;
}
