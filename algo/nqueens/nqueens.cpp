#include <iostream>
#include <vector>
#include <algorithm>


void printOneResult(const std::vector<int>& queen){
    for(std::size_t i=0; i< queen.size(); ++i){
        for(std::size_t j=0; j< queen.size(); ++j){
            std::cout << ' ' << ((queen[i] == j)? 'Q': '.');
        }
        std::cout << std::endl;
    }
}


void printQueensResult(const std::vector<std::vector<int> >& queens){
    std::for_each(queens.begin(), queens.end(), printOneResult);
}


void nqueens_check(const int n,
                   std::vector<int>& column,
                   std::vector<int>& rus,
                   std::vector<int>& lus,
                   std::vector<int>& check, 
                   std::vector<std::vector<int> >& queens){
    if(n >= check.size()){
        queens.push_back(check);
    }
    else{
        for(std::size_t i=0; i< check.size(); i++){
            if(column[i] == 1 && rus[n+i] == 1 && lus[n-i+check.size()-1] == 1){
                column[i] = rus[n+i] = lus[n-i+check.size()-1] = 0;
                check[n] = i;

                nqueens_check(n+1, column, rus, lus, check, queens);

                check[n] = 0;
                column[i] = rus[n+i] = lus[n-i+check.size()-1] = 1;
            }
        }
    }
}


void nqueens(std::vector<std::vector<int> >& queens, const int N=8){
    std::vector<int> column(N, 1);
    std::vector<int> check(N, 0);
    std::vector<int> lus(2*N+1, 1);
    std::vector<int> rus(2*N+1, 1);

    nqueens_check(0, column, rus, lus, check, queens);    
}


int main(int argc, char const* argv[]) {
    std::vector<std::vector<int> > queens;

    nqueens(queens, 8);

    std::cout << queens.size() << std::endl;
    printQueensResult(queens);

    return 0;
}
