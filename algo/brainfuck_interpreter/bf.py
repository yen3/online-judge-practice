#!/usr/bin/env python3
from sys import argv as sysargv


class BFState:
    def __init__(self):
        self.data = [0] * 10000
        self.dp = 0
        self.ip = 0
        self.ip_stack = []


def incPtr(bfs):
    bfs.dp = bfs.dp + 1


def decPtr(bfs):
    bfs.dp = bfs.dp - 1


def incData(bfs):
    bfs.data[bfs.dp] = bfs.data[bfs.dp] + 1


def decData(bfs):
    bfs.data[bfs.dp] = bfs.data[bfs.dp] - 1


def outputData(bfs):
    print(chr(bfs.data[bfs.dp]), end="")


def loopBegin(bfs):
    bfs.ip_stack.append(bfs.ip)


def loopEnd(bfs):
    if bfs.data[bfs.dp] == 0:
        bfs.ip_stack.pop()
    else:
        bfs.ip = bfs.ip_stack[-1]


def bfinterpreter(src, bfs):
    oper_fun = {
        ">": incPtr,
        "<": decPtr,
        "+": incData,
        "-": decData,
        ".": outputData,
        "[": loopBegin,
        "]": loopEnd,
    }

    oper_fun[src[bfs.ip]](bfs)

    bfs.ip = bfs.ip + 1
    if bfs.ip < len(src):
        bfinterpreter(src, bfs)


def main():
    if len(sysargv) >= 2:
        src = sysargv[1]
    else:
        src = (
            "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++."
            "<<+++++++++++++++.>.+++.------.--------.>+.>."
        )

    bfs = BFState()
    bfinterpreter(src, bfs)


if __name__ == "__main__":
    main()
