-- It's a draft version, not tested and verified.
-- Only for practicing StateT
module BrainFucker2 (
    brainFunckInterpreter
  , program
  , main2
) where

import           Control.Monad.State
import           Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as C
import           Data.Char             (chr)
import           Data.Vector.Unboxed   (Vector, (!), (//))
import qualified Data.Vector.Unboxed   as V

data Machine = Machine
    { dPtr      :: Int
    , iPtr      :: Int
    , iPtrStack :: [Int]
    , tap       :: Vector Int
    , instrs    :: ByteString
    }
  deriving Show

initMachine :: ByteString -> Machine
initMachine is = Machine
    { dPtr = 0
    , iPtr = 0
    , iPtrStack = []
    , tap = V.replicate 10000 0
    , instrs = is
    }

type MachineState a = StateT Machine IO a

dPtrOp :: (Int -> Int) -> MachineState ()
dPtrOp f = get >>= (\s -> put $ s { dPtr = f (dPtr s) })

tapEleOp :: (Int -> Int) -> MachineState ()
tapEleOp f = get >>= (\s -> put $ s { tap = update (dPtr s) (f (tap s ! dPtr s)) (tap s) })
  where update idx val vs = vs // [(idx, val)]

printTapElement :: MachineState ()
printTapElement = get >>= (\s -> liftIO $ print (chr (tap s ! dPtr s)))

loopBegin :: MachineState ()
loopBegin =  get >>= (\s -> put $ s { iPtrStack = iPtr s : iPtrStack s })

loopEnd :: MachineState ()
loopEnd = do
  s <- get
  let stack = iPtrStack s
      loopState = tap s ! dPtr s
  put $ s { iPtr = if loopState == 0 then iPtr s else head stack
          , iPtrStack = if loopState == 0 then tail stack else stack
          }

runInstrs :: MachineState ()
runInstrs = do
  s <- get
  let idx = iPtr s

  case  instrs s `C.index` idx of
    '>' -> dPtrOp (+1)
    '<' -> dPtrOp (\x -> x - 1)
    '+' -> tapEleOp (+1)
    '-' -> tapEleOp(\x -> x - 1)
    '.' -> printTapElement
    '[' -> loopBegin
    ']' -> loopEnd
    _   -> error "Not support for the instruction"

  put $ s { iPtr = idx + 1}

  if idx + 1 < C.length (instrs s) then runInstrs else pure ()

brainFunckInterpreter :: ByteString -> IO ()
brainFunckInterpreter xs = evalStateT runInstrs (initMachine xs)

program :: ByteString
program = C.pack "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."

main2 :: IO ()
main2 = brainFunckInterpreter program
