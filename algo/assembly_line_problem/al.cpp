#include <iostream>
#include <vector>
using namespace std;

void input_data(vector<vector<int> >& a, vector<vector<int> >& t, int& e1, int& e2, int& x1, int& x2){
    int size=0;
    cin >> size;
    a = vector<vector<int> >(2, vector<int>(size, 0));
    t = vector<vector<int> >(2, vector<int>(size-1, 0));

    cin >> e1 >> e2 >> x1 >> x2;
    for(unsigned int i=0;i<a.size();i++){
        for(unsigned int j=0;j<a[i].size();j++) cin >> a[i][j];
    }
    for(unsigned int i=0;i<t.size();i++){
        for(unsigned int j=0;j<t[i].size();j++) cin >> t[i][j];
    }
}

void assembly_line(vector<vector<int> >& f, vector<vector<int> >& l, 
	           int& f_e, int& l_e,
	           const vector<vector<int> >& a, const vector<vector<int> >& t,
		   int& e1, int& e2, int& x1, int& x2){
    f = vector<vector<int> >(2, vector<int>(a[0].size(), 0));
    l = vector<vector<int> >(2, vector<int>(a[0].size(), 0));

    f[0][0] = e1 + a[0][0];
    f[1][0] = e2 + a[1][0];
    int temp_a, temp_b;
    for(unsigned int i=1;i<f[0].size();i++){
        temp_a = f[0][i-1] + a[0][i];
	temp_b = f[1][i-1] + a[0][i] + t[1][i-1];
	f[0][i] = (temp_a<temp_b)?temp_a: temp_b;
	l[0][i] = (temp_a<temp_b)?0:1;

	temp_a = f[1][i-1] + a[1][i];
	temp_b = f[0][i-1] + a[1][i] + t[0][i-1];
	f[1][i] = (temp_a<temp_b)?temp_a: temp_b;
	l[1][i] = (temp_a<temp_b)?1:0;
    }

    temp_a = f[0][f[0].size()-1]+x1;
    temp_b = f[1][f[1].size()-1]+x2;
    f_e = (temp_a<temp_b)?temp_a:temp_b;
    l_e = (temp_a<temp_b)?0:1;
}

void print_assembly_line(const int& l_e, const vector<vector<int> >& l){
   int i = l_e;
   cout << "line " << i+1 << " station " << l[0].size() << endl;
   for(int j=l[0].size()-1;j>=1;j--){
       i = l[i][j];
       cout << "line " << i+1 << " station " << j  << endl;
   }
}

int main(){
    vector<vector<int> > f, l, a, t;
    int e1, e2, x1, x2, f_e, l_e;
    input_data(a, t, e1, e2, x1, x2);

    assembly_line(f, l , f_e, l_e, a, t, e1, e2, x1, x2);
    print_assembly_line(l_e, l);
}
