-- 2021/07/20
module MergeSort2 (
  mergeSort,
  pMergeSort
) where

import           Control.Monad.Par

merge :: (Ord a) => [a] -> [a] -> [a]
merge [] [] = []
merge xs [] = xs
merge [] ys = ys
merge xs@(x:xss) ys@(y:yss) | x < y     = x : merge xss ys
                            | otherwise = y : merge xs yss

uninterleave :: [a] -> ([a],[a])
uninterleave [] = ([], [])
uninterleave xs = fst . foldr (\x ((ys, zs), s) -> if s then ((x:ys, zs), False)
                                                        else ((ys, x:zs), True))
                        (([],[]), False) $ xs

mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = let (ys, zs) = uninterleave xs
                in merge (mergeSort ys) (mergeSort zs)

pMergeSort :: (Ord a, NFData a) => [a] -> [a]
pMergeSort = runPar . go
  where
    go [] = pure []
    go [x] = pure [x]
    go xs = do
      let (ys', zs') = uninterleave xs
      fys <- spawnP (runPar $ go ys')
      fzs <- spawnP (runPar $ go zs')
      ys <- get fys
      zs <- get fzs
      return $ merge ys zs
