#!/usr/bin/env python3

import sys
import functools


def score_name(name):
    return sum([ord(c) - ord('A') + 1 for c in name])


def main():
    names = [raw_name[1:-1] for raw_name in sys.stdin.read().split(',')]
    names.sort()

    name_scores = [score_name(name) for name in names]
    scores = [order * name_score
              for order, name_score
              in zip(range(1, len(name_scores)+1), name_scores)]
    sum_score = sum(scores)

    print(sum_score)

if __name__ == "__main__":
    main()
