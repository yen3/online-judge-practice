import Data.Char

sumNumStrList :: String -> String
sumNumStrList = numListToString . adjustDec . addNumList . getNumList

getNumList:: String -> [[Int]]
getNumList s = map getNumAsList $ lines s
    where getNumAsList = reverse . map (\x -> Data.Char.ord x - 48)

addNumList :: [[Int]] -> [Int]
addNumList = foldr1 (zipWith (+))

adjustDec :: [Int] -> [Int]
adjustDec = flip adjustDec' 0

adjustDec' :: [Int] -> Int -> [Int]
adjustDec' [] carry_in =
    if carry_in > 10 then (carry_in `mod` 10) : (adjustDec' [] (carry_in `quot` 10))
    else [carry_in]
adjustDec' (x:xs) carry_in = ((x + carry_in) `mod` 10): (adjustDec' xs (x + carry_in `quot` 10))

numListToString :: [Int] -> String
numListToString nl = reverse $ map (\x -> Data.Char.chr (x+48)) nl


main :: IO()
main = do
    contents <- readFile "pe13.input"
    putStrLn $ sumNumStrList contents
