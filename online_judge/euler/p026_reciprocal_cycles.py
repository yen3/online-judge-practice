#!/usr/bin/env python3

def reciprocal_cycles(div):
    """
    Return:
        None: no cycle
        otherwise: cycle number

    >>> reciprocal_cycles(2)
    >>> reciprocal_cycles(3)
    '3'
    >>> reciprocal_cycles(7)
    '142857'
    >>> reciprocal_cycles(15)
    '6'
    >>> reciprocal_cycles(150)
    '6'
    """

    def find_cycle(prev_divs, div):
        for i, pd in enumerate(prev_divs):
            if div == pd[0]:
                return i

        return -1

    prev_divs = []

    dividend = 1
    while True:
        dividend *= 10

        if dividend % div == 0:
            return None

        if dividend < div:
            prev_divs.append((dividend, 0))
        else:
            cycle = find_cycle(prev_divs, dividend)
            if cycle != -1:
                s = "".join([str(prev_divs[i][1]) for i in range(cycle, len(prev_divs))])
                return s

            q, r = divmod(dividend, div)
            prev_divs.append((dividend, q))
            dividend = r

    return None


def main():
    cycles = [reciprocal_cycles(d) for d in range(1,1000)]

    num_cycles = [((num+1), int(cycle)) for num, cycle in enumerate(cycles) if cycle != None]
    num_cycles.sort(key=lambda x: x[1])
    print(num_cycles[-1][0])


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    main()
