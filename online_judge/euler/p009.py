#!/usr/bin/env python3

import math

def check(a, b):
    c = 1000 - (a + b)
    if (a*a + b*b) == c*c:
        return True
    else:
        return False

def main():
    for a in range(1, 500):
        for b in range(a, 500):
            if check(a, b):
                print(a, b, 1000 - a - b)


if __name__ == "__main__":
    main()
