#!/usr/bin/env python3

import math


def distinct_pow(max_base, max_pow):
    small_base = [0] * (max_base + 1)

    for i in range(2, len(small_base)):
        if small_base[i] == 0:
            j = i
            new_p = 1
            while j < len(small_base):
                small_base[j] = (i, new_p)

                j *= i
                new_p = new_p + 1

    small_base = small_base[2:]

    power_nums = {b: set() for b, _ in small_base }
    for base, factor in small_base:
        for p in range(2, max_pow+1):
            power_nums[base].add(p * factor)

    return sum([len(ps) for _, ps in power_nums.items()])


def main():
    print(distinct_pow(100, 100))


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    main()
