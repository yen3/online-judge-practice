{-# LANGUAGE BangPatterns #-}

module Main where

fibs :: Integer -> Integer -> [Integer]
fibs !n1 !n2 = n1 : fibs n2 (n1+n2)

take1000 = length $ takeWhile (\x -> (length . show) x < 1000) (fibs 1 1)

main = print $ take1000 + 1
