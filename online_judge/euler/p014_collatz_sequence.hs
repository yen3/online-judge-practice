{-# LANGUAGE BangPatterns #-}

module Main where

collateSeq 1 !t = t
collateSeq !n !t = let !val = if even n then n `div` 2 else n * 3 + 1
                 in collateSeq val (t+1)

longestCollateSeq n = let !xs = indexSeq n
                      in foldr cmp (1, 1) xs
  where cmp m@(_, mt) n@(_, nt) = if mt > nt then m else n
        indexSeq !n = zip [1..] (map (`collateSeq` 1) [1..n])

main = print (fst $ longestCollateSeq (1000000-1))
