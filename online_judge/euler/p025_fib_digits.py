#!/usr/bin/env python3

class Fib:
    def __init__(self):
        self.fibs = [0, 1]

    def __call__(self, n):
        if len(self.fibs) < n+1:
            for i in range(len(self.fibs), n+1):
                self.fibs.append(self.fibs[i-1] + self.fibs[i-2])

        return self.fibs[n]


def main():
    fib = Fib()
    assert fib(12) == 144
    assert fib(1) == 1
    assert fib(11) == 89

    i = 0
    while len(str(fib(i))) < 1000:
        i = i + 1

    print(i)

if __name__ == "__main__":
    main()
