#include <iostream>
#include <vector>
#include <cstdint>

void
read_triangle(std::vector<std::vector<uint32_t> >& triangle)
{
  int num;

  int max_column = 1;
  int current_column = 0;

  triangle.push_back(std::vector<uint32_t>());
  while(std::cin >> num)
    {
      if (current_column >= max_column)
	{
	  triangle.push_back(std::vector<uint32_t>());
	  current_column = 0;

	  max_column++;
	}

      triangle.back().push_back(num);

      current_column++;
    }
}

uint32_t
max_path_sum(std::vector<std::vector<uint32_t> >& triangle)
{
  for (int r = triangle.size() - 2; r >= 0; --r)
    {
      for (std::size_t c = 0; c < triangle[r].size(); ++c)
        {
	  uint32_t left = triangle[r][c] + triangle[r+1][c];
	  uint32_t right = triangle[r][c] + triangle[r+1][c+1];

	  triangle[r][c] = std::max(left, right);
	}
    }

  return triangle[0][0];
}

int
main()
{
  std::vector<std::vector<uint32_t> > triangle;
  read_triangle(triangle);

  std::cout << max_path_sum(triangle) << std::endl;
}
