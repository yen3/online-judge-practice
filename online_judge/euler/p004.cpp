#include <iostream>
#include <string>
#include <sstream>

bool isPalindrome(const std::string& s) {
    for (int b = 0, e = s.size() - 1; b<e; b++, e--){
        if(s[b] != s[e]){
            return false; 
        } 
    }
    return true;
}

bool isPalindromeNumber(int num) {
    std::ostringstream oss;
    oss << num;

    return isPalindrome(oss.str());
}

int findLargestPalindromeNumber(int nbegin=100, int nend=999){
    int maxP = 0;
    for(int i=nbegin; i<=nend; ++i) {
        for(int j=nbegin; j<=nend; ++j) {
            int p = i * j;
            if(isPalindromeNumber(p) && p > maxP) {
                maxP = p; 
            }
        }
    }

    return maxP;
}

int main(int argc, char *argv[])
{
    int maxP = findLargestPalindromeNumber();
    std::cout << maxP << std::endl;
    return 0;
}
