from __future__ import print_function
import operator
import math


def num_factor(num):
    fs = dict()

    while num != 1:
        for n in range(2, num + 1):
            if num % n == 0:
                num = num / n

                if n in fs:
                    fs[n] = fs[n] + 1
                else:
                    fs[n] = 1

                break
    return fs


def collect_num_factor(num):
    num_factors = [num_factor(i) for i in range(2, num + 1)]

    factors = dict()
    for nf in num_factors:
        for k, v in nf.items():
            if k in factors:
                if v > factors[k]:
                    factors[k] = v
            else:
                factors[k] = v

    return factors


def smallest_multiple(num):
    factors = collect_num_factor(num)
    return reduce(operator.mul, [int(math.pow(k, v)) for k, v in factors.items()], 1)


def main():
    print(smallest_multiple(20))


if __name__ == "__main__":
    main()
