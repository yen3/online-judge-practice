#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <cassert>

enum class AmicableNum { uncheck, yes, no };

uint32_t
sum_divisors (uint32_t num)
{
  std::vector<uint32_t> divisors;
  divisors.push_back(1);

  uint32_t sqrt_num = static_cast<uint32_t>(std::sqrt(num)) + 1;
  for (uint32_t d = 2; d < sqrt_num; ++d)
    if (num % d == 0)
      divisors.insert (divisors.end (), {d, num / d});


  uint32_t sum = 0;
  for (auto d : divisors)
    sum += d;

  return sum;
}

void
check_amicablenum (std::vector<AmicableNum>& nums)
{
  nums[0] = AmicableNum::no;
  nums[1] = AmicableNum::no;

  for (uint32_t num = 2; num < nums.size(); ++num)
    {
      uint32_t opposite_num = sum_divisors(num);
      if (num < opposite_num &&
	  opposite_num <= (nums.size() - 1) &&
	  sum_divisors(opposite_num) == num)
	{
	  nums[num] = AmicableNum::yes;
	  nums[opposite_num] = AmicableNum::yes;
	}
    }
}



int main ()
{
  assert (sum_divisors (2) == 1);
  assert (sum_divisors (220) == 284);
  assert (sum_divisors (284) == 220);

  std::vector<AmicableNum> nums (10000 + 1, AmicableNum::uncheck);

  check_amicablenum (nums);
  assert (nums[220] == AmicableNum::yes);
  assert (nums[284] == AmicableNum::yes);

  uint64_t sum = 0;
  for (std::size_t i = 0; i < nums.size(); ++i)
    if (nums[i] == AmicableNum::yes)
      sum += i;

  std::cout << sum << std::endl;

  return 0;
}
