#include <cmath>
#include <iostream>
#include <unordered_set>
#include <vector>

const uint32_t MAX_NON_ABUNDANT = 28123;

uint32_t sum_divisors(uint32_t num) {
  std::vector<uint32_t> divisors;
  divisors.push_back(1);

  uint32_t sqrt_num = static_cast<uint32_t>(std::sqrt(num)) + 1;
  for (uint32_t d = 2; d < sqrt_num; ++d) {
    if (num % d == 0) {
      if (d * d != num)
        divisors.insert(divisors.end(), {d, num / d});
      else
        divisors.push_back(d);
    }
  }

  uint32_t sum = 0;
  for (auto d : divisors) sum += d;

  return sum;
}

bool check_sum_two_abundant_nums(
    uint32_t num, const std::vector<uint32_t>& abundant_nums,
    const std::unordered_set<uint32_t>& search_nums) {
  uint32_t check_upper = num / 2 + 1;

  for (std::size_t i = 0;
       i < abundant_nums.size() && abundant_nums[i] < check_upper; ++i)
    if (search_nums.find(num - abundant_nums[i]) != search_nums.end())
      return true;

  return false;
}

void list_all_non_sum_abundant_nums(std::vector<uint32_t>& nums) {
  std::vector<uint32_t> abundant_nums;
  std::unordered_set<uint32_t> search_nums;
  for (uint32_t num = 1; num <= MAX_NON_ABUNDANT; ++num) {
    if (sum_divisors(num) > num) {
      abundant_nums.push_back(num);
      search_nums.insert(num);
    }
  }

  for (uint32_t num = 1; num < MAX_NON_ABUNDANT; ++num) {
    if (!check_sum_two_abundant_nums(num, abundant_nums, search_nums))
      nums.push_back(num);
  }
}

int main() {
  std::vector<uint32_t> non_sum_abundant_nums;
  list_all_non_sum_abundant_nums(non_sum_abundant_nums);

  uint32_t sum = 0;
  for (auto n : non_sum_abundant_nums) sum += n;

  std::cout << sum << std::endl;

  return 0;
}
