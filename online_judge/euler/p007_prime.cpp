#include <iostream>
#include <vector>
#include <cstdint>
#include <cmath>

typedef std::vector<uint32_t> PrimeList;

bool is_prime(uint32_t num, const PrimeList& primes)
{
  uint32_t sqrt_num = static_cast<int>(sqrt(num)) + 1;
  for (uint32_t i = 2; i < primes.size() && primes[i] <= sqrt_num; ++i)
    if (num % primes[i] == 0)
      return false;

  return true;
}

void
make_primes(uint32_t max_prime, PrimeList& primes)
{
  if (max_prime >= 2)
    primes.push_back(2);
  if (max_prime >= 3)
    primes.push_back(3);
  if (max_prime >= 5)
    primes.push_back(5);

  for (uint32_t check_num = 7, coin = 0; check_num <= max_prime;)
    {
      if (is_prime(check_num, primes))
	primes.push_back(check_num);

      check_num += (coin == 0)? 4 : 2;
      coin = (coin == 0) ? 1 : 0;
    }
}

void
make_primes_size(uint32_t size, PrimeList& primes)
{
  if (size >= 1)
    primes.push_back(2);
  if (size >= 2)
    primes.push_back(3);
  if (size >= 3)
    primes.push_back(5);

  for (uint32_t check_num = 7, coin = 0; primes.size() < size;)
    {
      if (is_prime(check_num, primes))
	primes.push_back(check_num);

      check_num += (coin == 0)? 4 : 2;
      coin = (coin == 0) ? 1 : 0;
    }
}


int
main()
{
  PrimeList primes;

  //make_primes(100, primes);
  make_primes_size(10001, primes);

  for(auto p : primes)
    std::cout << p << std::endl;

  std::cout << primes.size() << std::endl;


}
