#!/usr/bin/env python3

import math


def len_factor(num):
    """
    Get the number of factors for the number

    Arg:
        num (int) : the number

    Return:
        int : the number of factors for the number
    """
    upper_bound = int(math.sqrt(num))

    len_num = 0
    for f in range(1, upper_bound + 1):
        if num % f == 0:
            if f * f == num:
                len_num += 1
            else:
                len_num += 2

    return len_num


def factor_tringle_number():
    len_facts = {}

    def get_len_fact(num, len_facts):
        if num in len_facts:
            return len_facts[num]
        else:
            len_num_fact = len_factor(num)
            len_facts = len_num_fact
            return len_num_fact

    i = 2
    while True:
        # Tringle(n) = n1 * n2
        if i % 2 == 0:
            n1 = i // 2
            n2 = i + 1
        else:
            n1 = i
            n2 = (i+1) // 2

        len_n1_fact = get_len_fact(n1, len_facts)
        len_n2_fact = get_len_fact(n2, len_facts)

        len_facts[n1*n2] = len_n1_fact * len_n2_fact

        # Check the currnet status is the answer
        if len_n1_fact * len_n2_fact >= 500:
            return n1 * n2

        i = i + 1


def main():
   print(factor_tringle_number())


if __name__ == "__main__":
    main()
