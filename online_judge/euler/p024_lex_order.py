#!/usr/bin/env python3

import functools
import math


def lex_order(num, chars):
    if len(chars) == 1:
        return chars[0]

    size_permutation = math.factorial(len(chars) - 1)

    char_index, remaining = divmod(num, size_permutation)

    return chars[char_index] + \
           lex_order(remaining, (chars[:char_index] + chars[char_index+1:]))


def main():
    chars = '0123456789'
    num = 1000000
    print(lex_order(num-1, chars))

if __name__ == "__main__":
    main()
