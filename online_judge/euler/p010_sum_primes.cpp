#include <iostream>
#include <vector>
#include <cstdint>
#include <cmath>

typedef std::vector<uint32_t> PrimeList;

bool is_prime(uint32_t num, const PrimeList& primes)
{
  uint32_t sqrt_num = static_cast<int>(sqrt(num)) + 1;
  for (uint32_t i = 2; i < primes.size() && primes[i] <= sqrt_num; ++i)
    if (num % primes[i] == 0)
      return false;

  return true;
}

void
make_primes(uint32_t max_prime, PrimeList& primes)
{
  if (max_prime >= 2)
    primes.push_back(2);
  if (max_prime >= 3)
    primes.push_back(3);
  if (max_prime >= 5)
    primes.push_back(5);

  for (uint32_t check_num = 7, coin = 0; check_num <= max_prime;)
    {
      if (is_prime(check_num, primes))
	primes.push_back(check_num);

      check_num += (coin == 0)? 4 : 2;
      coin = (coin == 0) ? 1 : 0;
    }
}

int
main()
{
  PrimeList primes;

  make_primes(2000000, primes);


  uint64_t sum_primes = 0;
  for (auto p : primes)
    sum_primes += p;

  std::cout << sum_primes << std::endl;
}
