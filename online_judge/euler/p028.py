#!/usr/bin/env python3
import math

def sum_spiral(n):
    levels = int((n-1)/2)

    s = 1

    spiral = 1
    for level in range(1, levels+1):
        current_spirals = [spiral + 2*level*i for i in range(1,4+1)]
        print(current_spirals)
        s += sum(current_spirals)
        spiral = current_spirals[-1]

    return s


def main():
    print(sum_spiral(5))
    print()
    print(sum_spiral(1001))


if __name__ == "__main__":
    main()
