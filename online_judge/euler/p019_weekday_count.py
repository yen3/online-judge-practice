#!/usr/bin/env python3

def weekday(y, m, d):
    if m in [1, 2]:
        m += 12
        y = y-1

    w = (d + 2*m + 3*(m+1) // 5 + y + y // 4 - y // 100 + y // 400) % 7;
    return w

def main():
    c = 0
    for y in range(1901, 2000 + 1):
        for m in range(1, 12 + 1):
            if weekday(y, m, 1) == 6:
                c = c + 1
                print(y, m, 1)
    print(c)

if __name__ == "__main__":
    main()
