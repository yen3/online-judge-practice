#include <iostream>
#include <vector>

std::size_t lattice_paths(int n)
{
  std::vector<std::vector<std::size_t> > m(n+1,
					   std::vector<std::size_t>(n+1, 0));

  for (std::size_t i = 1; i < m.size(); i++)
    m[i][0] = 1;

  for (std::size_t i = 1; i < m.size(); i++)
    m[0][i] = 1;

  for (std::size_t i = 1; i < m.size(); ++i)
    for(std::size_t j = 1; j < m.size(); ++j)
       m[i][j] = m[i-1][j] + m[i][j-1];

  return m[m.size()-1][m[m.size()-1].size()-1];
}

int main(int argc, char *argv[])
{
  std::cout << lattice_paths(3) << std::endl;
  std::cout << lattice_paths(20) << std::endl;
  return 0;
}
