#include <iostream>
#include <algorithm>
#include <vector>

int minimumAbsoluteDifference(std::vector<int> arr) {
  if (arr.size() <= 1) {
    return 0;
  }

  std::sort(arr.begin(), arr.end());

  int min_diff = std::abs(arr[1] - arr[0]);
  for (std::size_t i = 1; i < arr.size() - 1; ++i) {
    int curr_min_diff = std::abs(arr[i+1] - arr[i]);
    if (curr_min_diff < min_diff) {
      min_diff = curr_min_diff;
    }
  }

  return min_diff;
}
