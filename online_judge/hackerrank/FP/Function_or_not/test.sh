#!/bin/bash

EXCEPT_OUTPUT=$(cat <<-END
YES
YES
NO
END
)

ghc -O2 fun_or_not.hs > /dev/null 2>&1
OUTPUT=$(./fun_or_not < ./input.txt)

if [ x"$OUTPUT" == x"$EXCEPT_OUTPUT" ]; then
    echo "Test pass"
else
    echo "Test fail"
fi

rm -f ./fun_or_not
rm -f ./fun_or_not.hi
rm -f ./fun_or_not.o
