import Control.Monad
import qualified Data.Map.Strict as Map

isFun :: [(Int, Int)] -> Map.Map Int Int -> Bool
isFun [] _ = True
isFun ((d,k):xs) ms = if Map.member d ms then False
                      else isFun xs (Map.insert d k ms)

getMultipleLines :: Int -> IO [String]
getMultipleLines n
    | n <= 0 = return []
    | otherwise = do
        x <- getLine
        xs <- getMultipleLines (n-1)
        let ret = (x:xs)
        return ret

readStrToTuple :: [String] -> [(Int, Int)]
readStrToTuple xs = map (\s -> (fst_num s, snd_num s)) xs
    where fst_num s = read (words s !! 0) :: Int
          snd_num s = read (words s !! 1) :: Int

getInt :: IO Int
getInt = do
    line <- getLine
    let n = read line :: Int
    return n

main :: IO ()
main = do
    n_case <- getInt
    forM_ [1..n_case] $ \_ -> do
        n_line <- getInt
        xs <- getMultipleLines n_line
        let ts = readStrToTuple xs
        let ans = isFun ts (Map.fromList [])
        putStrLn (if ans == True then "YES" else "NO")

