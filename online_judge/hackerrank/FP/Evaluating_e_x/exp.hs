import Control.Applicative
import Control.Monad
import System.IO

my_product 0 = 1
my_product n = n * my_product (n-1)

my_pow _ 0 = 1
my_pow x 1 = x
my_pow x n = x * my_pow x (n-1)


ele _ 0 = 1.0
ele x 1 = x
ele x n = (my_pow x n) / (my_product n)

my_exp x = sum $ map (ele x) [0..9]

main :: IO ()
main = do
    n_temp <- getLine
    let n = read n_temp :: Int
    forM_ [1..n] $ \a0  -> do
        x_temp <- getLine
        let x = read x_temp :: Double
        putStrLn (show (my_exp x))

getMultipleLines :: Int -> IO [String]

getMultipleLines n
    | n <= 0 = return []
    | otherwise = do
        x <- getLine
        xs <- getMultipleLines (n-1)
        let ret = (x:xs)
        return ret


