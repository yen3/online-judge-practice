#!/bin/python3


def cal_combine2(n):
    return n * (n-1) // 2


def find_length_equal_one(s):
    # key: str
    # value: [int]
    char_loc = dict()

    for i, c in enumerate(s):
        char_loc.setdefault(c, []).append(i)

    char_loc = {k: v for k, v in char_loc.items() if len(v) > 1}
    return char_loc


def get_one(s):
    char_loc = find_length_equal_one(s)
    num_comb = sum([cal_combine2(len(v)) for v in char_loc.values()])
    return char_loc, num_comb


def get_n(s, char_loc):
    loc = sorted([l for v in char_loc.values() for l in v])

    ds = dict()
    for i in range(0, len(loc)):
        for j in range(1, len(loc)):
            b = loc[i]
            e = loc[j]

            if e - b < 2:
                continue

            ds.setdefault("".join(sorted(s[b:e])), set()).add((b,e))
            ds.setdefault("".join(sorted(s[b+1:e+1])), set()).add((b+1,e+1))

    return sum([cal_combine2(len(v)) for v in ds.values()])


def sherlock_and_anagrams(s):
    # Deal length == 1
    char_loc, num_comb_1 = get_one(s)

    # Deal length >= 2
    num_comb_n = get_n(s, char_loc)

    return num_comb_1 + num_comb_n


def main():
    q = int(input().strip())
    for a0 in range(q):
        s = input().strip()
        result = sherlock_and_anagrams(s)
        print(result)


if __name__ == "__main__":
    main()

