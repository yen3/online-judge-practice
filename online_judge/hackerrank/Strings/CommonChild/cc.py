#!/bin/python3


from enum import IntEnum
import sys
import math
import os
import random
import re

class Direct(IntEnum):
    Unknown = 0
    Up = 1
    Left = 2
    UpLeft = 3


class Record(object):
    __slots__ = ['l', 'd']
    def __init__(self, l=0, d=Direct.Unknown):
        self.l= l     # length
        self.d = d    # direct

    def __repr__(self):
        return "({l},{d})".format(l=self.l, d=self.d)


def find_lcs_str(i, j, x, y, c):
    s = []

    while i != 0 and j != 0:
        if c[i][j].d == Direct.UpLeft:
            s.append(x[i-1])
            i = i - 1
            j = j - 1
        elif c[i][j].d == Direct.Up:
            i = i - 1
        elif c[i][j].d == Direct.Left:
            j = j - 1

    if isinstance(x, str) and isinstance(y, str):
        return "".join(s[::-1])
    else:
        return s[::-1]


def longestCommonSubsequence(x, y):
    c = [[Record() for _y in range(len(y) + 1)] for _x in range(len(x) + 1)]

    for i in range(1, len(c)):
        for j in range(1, len(c[i])):
            if x[i-1] == y[j-1]:
                c[i][j].l = c[i-1][j-1].l + 1
                c[i][j].d = Direct.UpLeft
            elif c[i-1][j].l >= c[i][j-1].l:
                c[i][j].l = c[i-1][j].l
                c[i][j].d = Direct.Up
            else:
                c[i][j].l = c[i][j-1].l
                c[i][j].d = Direct.Left

    return find_lcs_str(len(x), len(y), x, y, c)


def commonChild(s1, s2):
    return len(longestCommonSubsequence(s1, s2))


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s1 = input()

    s2 = input()

    result = commonChild(s1, s2)

    fptr.write(str(result) + '\n')

    fptr.close()

