#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

enum class Direct : int {
  Unknown = 0,
  Up,
  Left,
  UpLeft
};

struct Trace {
  int l;
  Direct d;
};

std::string trace_lcs(const std::string& x, const std::string& y, const std::vector<std::vector<Trace>>& c) {
  std::string lcs;

  for (int i = x.size(), j = y.size(); i != 0 && j != 0;) {
    if (c[i][j].d == Direct::UpLeft) {
      lcs += x[i-1];
      --i;
      --j;
    } else if (c[i][j].d == Direct::Up){
      --i;
    } else if (c[i][j].d == Direct::Left) {
      --j;
    }
  }

  std::reverse(lcs.begin(), lcs.end());
  return lcs;
}

std::string longest_common_subsequence(const std::string& x, const std::string& y) {
  std::vector<std::vector<Trace>> c(x.size() + 1, std::vector<Trace>(y.size() + 1));

  for (std::size_t i = 1; i < c.size(); ++i) {
    for (std::size_t j = 1; j < c[i].size(); ++j) {
      if (x[i-1] == y[j-1]) {
        c[i][j].l = c[i-1][j-1].l + 1;
        c[i][j].d = Direct::UpLeft;
      } else if (c[i-1][j].l >= c[i][j-1].l) {
        c[i][j].l = c[i-1][j].l;
        c[i][j].d = Direct::Up;
      } else {
        c[i][j].l = c[i][j-1].l;
        c[i][j].d = Direct::Left;
      }
    }
  }

  return trace_lcs(x, y, c);
}

int commonChild(const std::string& s1, const std::string& s2) {
  return longest_common_subsequence(s1, s2).length();
}

int main() {
    ofstream fout(getenv("OUTPUT_PATH"));

    string s1;
    getline(cin, s1);

    string s2;
    getline(cin, s2);

    int result = commonChild(s1, s2);

    fout << result << "\n";

    fout.close();

    return 0;
}
