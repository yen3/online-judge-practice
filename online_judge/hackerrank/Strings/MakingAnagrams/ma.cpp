#include <string>
#include <algorithm>
#include <cmath>


int makeAnagram(const std::string& x, const std::string& y) {
  const std::size_t kCharSize;

  int xs[kCharSize];
  int ys[kCharSize];

  std::fill_n(xs, kCharSize, 0);
  std::fill_n(ys, kCharSize, 0);

  for (std::size_t i = 0; i < x.size(); ++i) {
    xs[x[i]] += 1;
  }

  for (std::size_t i = 0; i < y.size(); ++i) {
    ys[y[i]] += 1;
  }

  int edit_chars = 0;
  for (std::size_t i = 0; i < kCharSize; ++i) {
    if (xs[i] != 0 || ys[i] != 0) {
      edit_chars += std::abs(xs[i] - ys[i]);
    }
  }

  return edit_chars;
}
