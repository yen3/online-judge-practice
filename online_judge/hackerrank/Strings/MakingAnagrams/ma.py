#!/bin/python3

import sys

def makingAnagrams(x, y):
    freq_x = [0] * 256
    freq_y = [0] * 256

    for c in x:
        freq_x[ord(c)] += 1

    for c in y:
        freq_y[ord(c)] += 1

    delete_chars = 0
    for fx, fy in zip(freq_x, freq_y):
        delete_chars += abs(fx - fy)

    return delete_chars


def main():
    s1 = input().strip()
    s2 = input().strip()
    result = makingAnagrams(s1, s2)
    print(result)


if __name__ == "__main__":
    main()


