#!/bin/python3

import sys

def common_str_from_start(target, search):
    common_str = ""

    for i in range(0, len(search)):
        curr = ""

        t_idx = 0
        s_idx = i
        while s_idx < len(search) and t_idx < len(target) and search[s_idx] == target[t_idx]:
            curr += search[s_idx]
            t_idx+=1
            s_idx+=1

        if len(curr) > len(common_str):
            common_str = curr

    return common_str

def string_construction_2(s):
    p = ""
    cost = 0

    while p != s:
        common_str = common_str_from_start(s[len(p):], p)
        if common_str == "":
            p += s[len(p)]
            cost += 1
        else:
            p += common_str

    return cost


def string_construction(s):
    return len(set(s))


def main():
    q = int(input().strip())
    for a0 in range(q):
        s = input().strip()
        result = string_construction(s)
        print(result)


if __name__ == "__main__":
    main()

