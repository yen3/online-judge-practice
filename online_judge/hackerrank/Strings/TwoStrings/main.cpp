#ifdef SELF_TEST
#include <iostream>
#include <vector>
#include <string>
#else
#include <bits/stdc++.h>
#endif

bool twoStrings(std::string x, std::string y){
  std::vector<int> char_count(256, 0);
  for (std::size_t i = 0; i < x.size(); ++i){
    char_count[x[i]]++;
  }

  for (std::size_t i = 0; i < y.size(); ++i){
    if (char_count[y[i]] != 0) {
      return true;
    }
  }

  return false;
}

int main() {
    int q;
    std::cin >> q;
    for(int a0 = 0; a0 < q; a0++){
      std::string s1;
        std::cin >> s1;
        std::string s2;
        std::cin >> s2;
        bool result = twoStrings(s1, s2);
        if(result){
          std::cout << "YES" << std::endl;
        } else {
          std::cout << "NO" << std::endl;
        }
    }
    return 0;
}

