#!/usr/bin/env bash

set -e

clang++ -g -Wall -DSELF_TEST -std=c++11 ./main.cpp -o main 

./main < input1.txt

rm -rf main
rm -rf main.dSYM
