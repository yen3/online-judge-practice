#include <bits/stdc++.h>

using namespace std;

// Complete the alternatingCharacters function below.
int alternatingCharacters(const std::string& s) {
  int delete_chars = 0;

  char curr_char = s[0];
  for (std::size_t i = 1; i < s.size(); ++i) {
    if (s[i] != curr_char) {
      curr_char = s[i];
    } else {
      ++delete_chars;
    }
  }

  return delete_chars;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string s;
        getline(cin, s);

        if (s != "") {
            int result = alternatingCharacters(s);
            fout << result << "\n";
        }
    }

    fout.close();

    return 0;
}

