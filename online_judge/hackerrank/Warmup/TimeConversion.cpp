/**
 * https://www.hackerrank.com/challenges/time-conversion
 * */

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>
#ifdef SELF_TEST
#include <cassert>
#include <cstdlib>
#endif /* SELF_TEST */

std::string ConvertTimeFormat(const std::string& time_str) {
  std::string ftime_str = time_str.substr(0, time_str.size() - 2);
  int hour = (time_str[0] - '0') * 10 + (time_str[1] - '0');
  if (time_str[time_str.size() - 2] == 'A') {

    if (hour == 12) {
      ftime_str[0] = '0';
      ftime_str[1] = '0';
    }
  } else {
    if (hour != 12)
      hour = hour + 12;

    ftime_str[0] = hour / 10 + '0';
    ftime_str[1] = hour % 10 + '0';
  }

  return ftime_str;
}

#ifdef SELF_TEST
void test() {
  assert(ConvertTimeFormat("07:05:45PM") == "19:05:45");
  assert(ConvertTimeFormat("12:05:45PM") == "12:05:45");
  assert(ConvertTimeFormat("12:05:45AM") == "00:05:45");

  std::exit(0);
}
#endif /* SELF_TEST */

int main() {
#ifdef SELF_TEST
  test();
#endif /* SELF_TEST */

  std::string time;
  std::cin >> time;

  std::cout << ConvertTimeFormat(time) << std::endl;

  return 0;
}
