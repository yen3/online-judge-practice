#if defined(__clang__)
#include "../stdc++.h"
#elif defined(__GNUG__) || defined(__GNUC__)
#include <bits/stdc++.h>
#endif

void SortBigNumbers(std::vector<std::string>& big_nums) {
  std::sort(big_nums.begin(), big_nums.end(),
            [](const std::string& x, const std::string& y) {
              if (x.size() != y.size()) return x.size() < y.size();

              for (std::size_t i = 0; i < x.size(); ++i)
                if (x[i] != y[i]) return x[i] < y[i];

              return false;
            });
}

#ifdef SELF_TEST
template <typename T>
bool IsVectorTheSame(const std::vector<T>& xs, const std::vector<T>& ys) {
  if (xs.size() != ys.size()) return false;

  for (std::size_t i = 0; i < xs.size(); ++i)
    if (xs[i] != ys[i]) return false;

  return true;
}

void test() {
  {
    std::vector<std::string> big_nums{
        "31415926535897932384626433832795", "1", "3", "10", "3", "5"};

    SortBigNumbers(big_nums);

    assert(IsVectorTheSame(
            big_nums, {"1", "3", "3", "5", "10",
                       "31415926535897932384626433832795"}) == true);
  }
  std::exit(0);
}
#endif /* SELF_TEST */

int main() {
#ifdef SELF_TEST
  test();
#endif /* SELF_TEST */

  int n;
  std::cin >> n;

  std::vector<std::string> unsorted(n);
  for (int unsorted_i = 0; unsorted_i < n; unsorted_i++) {
    std::cin >> unsorted[unsorted_i];
  }

  // your code goes here
  SortBigNumbers(unsorted);

  for (const auto& bn : unsorted)
    std::cout << bn << std::endl;

  return 0;
}
