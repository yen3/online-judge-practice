import qualified Data.List as L

maximumToys :: [Int] -> Int -> Int
maximumToys ps k = fst . L.last . L.takeWhile (\(a, b) -> b <= k) . L.zip [1..] . L.scanl1 (+) . L.sort $ ps
