#!/usr/bin/env python3


def read_numbers():
    n = int(input().strip())
    nums_str = input().strip()
    nums = [int(s) for s in nums_str.split()]
    return nums


def find_min_distance(nums):
    nums.sort()
    num_pairs = zip(nums[1:], nums)

    min_value = None
    min_n1, min_n2 = None, None

    values = []

    for n1, n2 in num_pairs:
        v = n1 - n2
        if min_value is None:
            min_value = v
            values.extend([n2, n1])
        elif v == min_value:
            values.extend([n2, n1])
        elif v < min_value:
            min_value = v
            values = [n2, n1]

    return values

def format_answer(values):
    return " ".join([str(v) for v in values])


def test():
    assert format_answer(
        find_min_distance([-20, -3916237, -357920, -3620600,
                           7374819, -7330761, 30, 6246457,
                           -6461594, 266854])) == "-20 30"


def main():
    test()

    nums = read_numbers()
    values = find_min_distance(nums)
    print(format_answer(values))


if __name__ == "__main__":
    main()
