#include <iostream>
#include <vector>

inline int calculate_glass_sum(std::size_t row, std::size_t column,
                 const std::vector<std::vector<int>>& table) {
  return table[row][column] + table[row][column + 1] + table[row][column + 2] +
         table[row + 1][column + 1] +
         table[row + 2][column] + table[row + 2][column + 1] +
         table[row + 2][column + 2];
}


int hourglassSum(const std::vector<std::vector<int>>& table) {
  const std::size_t kGlassSize = 3;

  int max_glass_sum = calculate_glass_sum(0, 0, table);
  for (std::size_t row = 0; row <= table.size() - kGlassSize; ++row) {
    for (std::size_t column = 0; column <= table[row].size() - kGlassSize; ++column) {
      int curr_glass_sum = calculate_glass_sum(row, column, table);
      if (curr_glass_sum > max_glass_sum) {
        max_glass_sum = curr_glass_sum;
      }
    }
  }

  return max_glass_sum;
}

int main(){

}
