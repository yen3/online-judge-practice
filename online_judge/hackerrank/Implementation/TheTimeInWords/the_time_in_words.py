#!/bin/python3
#!/bin/python3
import sys

NUMBER_STR = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five',
              6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten',
              11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
              15: 'quarter', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
              19: 'nineteen', 20: 'twenty', 21: 'twenty one', 22: 'twenty two',
              23: 'twenty three', 24: 'twenty four', 25: 'twenty five',
              26: 'twenty six', 27: 'twenty seven', 28: 'twenty eight',
              29: 'twenty nine', 30: 'half'}


def is_valid_input(hour, minute):
    return hour in range(1, 12) and minute in range(0, 60)


def time_word_string(hour, minutes):
    def before_half(h, m):
        return NUMBER_STR[m] + " minutes past " + NUMBER_STR[h]

    def after_half(h, m):
        return NUMBER_STR[60-m] + " minutes to " + NUMBER_STR[h+1]

    time_str_funs = [lambda h, m=i: before_half(h, m) for i in range(0, 30 + 1)]
    time_str_funs.extend(
        [lambda h, m=i: after_half(h, m) for i in range(31, 60)])

    time_str_funs[0] = lambda h: NUMBER_STR[h] + " o' clock"
    time_str_funs[1] = lambda h: "one minute past " + NUMBER_STR[h]
    time_str_funs[15] = lambda h: NUMBER_STR[15] + " past " + NUMBER_STR[h]
    time_str_funs[30] = lambda h: "half past " + NUMBER_STR[h]
    time_str_funs[45] = lambda h: NUMBER_STR[15] + " to " + NUMBER_STR[h+1]
    time_str_funs[59] = lambda h: "one minute to" + NUMBER_STR[h+1]

    return time_str_funs[minutes](hour)


def main():
    h = int(input().strip())
    m = int(input().strip())

    print(time_word_string(h, m))

    if not is_valid_input(h, m):
        sys.exit(1)


if __name__ == "__main__":
    main()
