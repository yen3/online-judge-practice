#include <cmath>
#include <iterator>
#include <cstdint>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>

const std::uint32_t MAX_NUM_ELEMENT = 1000;
using NumberVector = std::vector<std::uint32_t>;

using Facts = std::vector<NumberVector>;

Facts CreateFactorialNumbers(std::uint32_t max_n) {
  Facts fs;
  fs.push_back({1});

  for (std::uint32_t n = 1; n <= max_n; ++n) {
    NumberVector v(fs[n-1]);
    for (std::size_t i = 0; i < v.size(); ++i) {
      v[i] *= n;
    }

    std::size_t carry = 0;
    for (std::size_t i = 0; i < v.size(); ++i) {
      if (carry != 0) {
        v[i] += carry;
        carry = 0;
      }

      if (v[i] >= MAX_NUM_ELEMENT) {
        carry = v[i] / MAX_NUM_ELEMENT;
        v[i] = v[i] % MAX_NUM_ELEMENT;
      }
    }

    while (carry != 0) {
      v.push_back(carry % MAX_NUM_ELEMENT);
      carry /= MAX_NUM_ELEMENT;
    }

    fs.push_back(v);
  }

  return fs;
}

void PrintNumberVector(const NumberVector& num) {
  auto end_iter = num.rend();
  auto iter = num.rbegin();

  std::cout << *iter;
  iter++;

  for (; iter != end_iter; ++iter) {
    std::cout << std::setw(3) << std::setfill('0') << *iter;
  }
}

int main() {
  const std::uint32_t MAX_FACTORIAL_NUM = 100;
  Facts fs = CreateFactorialNumbers(MAX_FACTORIAL_NUM);

#if 0
  for (std::size_t i = 0; i < fs.size(); ++i) {
    std::cout << std::setw(3) << std::setfill(' ') << i << ": ";
    PrintNumberVector(fs[i]);
    std::cout << std::endl;
  }
#endif

  std::uint32_t n = 0;

  std::cin >> n;

  PrintNumberVector(fs[n]);
  std::cout << std::endl;

  return 0;
}

