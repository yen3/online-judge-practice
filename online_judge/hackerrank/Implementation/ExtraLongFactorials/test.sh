#!/usr/bin/env bash

CC=/Users/yen3/tools/gcc/bin/g++

function test {
    input=$1
    output=$2

    EXEC=$(echo "$input" | ./main.out)
    if [ "$EXEC" != "$output" ]; then
        echo "Test $input fails! Result: $EXEC, Expected: $output"
    else
        echo "Test $input success!"
    fi
}

${CC} -O0 -g3 -std=c++14 -Wall -Wextra main.cpp -o main.out

# Testcases
test "1" "1"
test "2" "2"
test "3" "6"
test "10" "3628800"
test "100" "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000"

# Clear environment
rm -rf main.out main.out.dSYM

