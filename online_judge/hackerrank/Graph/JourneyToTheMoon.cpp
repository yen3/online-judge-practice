#if defined(__clang__)
#include "../stdc++.h"
#elif defined(__GNUG__) || defined(__GNUC__)
#include <bits/stdc++.h>
#endif

void MarkConnected(uint32_t group_num, uint32_t vertex_num,
                   std::vector<uint32_t>& vertexs,
                   const std::vector<std::vector<int>>& edges) {
  for (std::size_t i = 0; i < edges[vertex_num].size(); ++i) {
    if (vertexs[edges[vertex_num][i]] == 0) {
      vertexs[edges[vertex_num][i]] = group_num;
      MarkConnected(group_num, edges[vertex_num][i], vertexs, edges);
    }
  }
}

void FindConnectedCompoment(const std::vector<std::vector<int>>& edges,
                            std::vector<uint32_t>& vertexs) {
  for (std::size_t i = 0, group_num = 1; i < vertexs.size(); ++i) {
    if (vertexs[i] != 0) continue;

    vertexs[i] = group_num;
    MarkConnected(group_num, i, vertexs, edges);
    group_num++;
  }
}

std::vector<uint32_t> CollectGroups(const std::vector<uint32_t>& nums) {
  std::vector<uint32_t> groups(nums.size(), 0);

  for (auto n : nums) groups[n]++;

  while (groups.back() == 0) groups.pop_back();

  return groups;
}

uint64_t CalculateAllWays(const std::vector<uint32_t>& groups) {
  std::vector<uint32_t> more;
  uint64_t one = 0;
  for (auto g : groups) {
    if (g == 0)
      continue;

    if (g == 1)
      one++;
    else
      more.push_back(g);
  }

  uint64_t ways = 0;
  for (std::size_t i = 0; i < more.size(); ++i) {
    for (std::size_t j = i+1; j < more.size(); ++j) {
      ways += more[i] * more[j];
    }
  }

  if (one != 0) {
    uint64_t sum_more = 0;
    for (auto m : more) sum_more += m;
    ways = ways + sum_more * one + ((one - 1) * one / 2);
  }

  return ways;
}

uint64_t ChoosenWaysOfAstronauts(int n,
                                 const std::vector<std::vector<int>>& pairs) {
  std::vector<uint32_t> vertexs(n, 0);

  FindConnectedCompoment(pairs, vertexs);

  std::vector<uint32_t> groups = CollectGroups(vertexs);

  uint64_t ways = CalculateAllWays(groups);
  return ways;
}

#ifdef SELF_TEST
void test() {
  assert(ChoosenWaysOfAstronauts(4, {{2}, {}, {0}, {}}) == 5);
  {
    std::vector<std::vector<int>> edges(100000);
    edges[1].push_back(2);
    edges[2].push_back(1);
    edges[3].push_back(4);
    edges[4].push_back(3);

    assert(ChoosenWaysOfAstronauts(edges.size(), edges) == 4999949998);
  }
  std::exit(0);
}
#endif /* SELF_TEST */

int main() {
#ifdef SELF_TEST
  test();
#endif /* SELF_TEST */

  int N, I;
  std::cin >> N >> I;
  std::vector<std::vector<int>> pairs(N);
  for (int i = 0; i < I; ++i) {
    int a, b;
    std::cin >> a >> b;
    pairs[a].push_back(b);
    pairs[b].push_back(a);
  }

  long long result = 0;

  /** Write code to compute the result using N,I,pairs **/
  result = ChoosenWaysOfAstronauts(N, pairs);

  std::cout << result << std::endl;

  return 0;
}
