#!/bin/python3

from collections import namedtuple
import itertools

StackInfo = namedtuple('StackInfo', ['s_idx', 'p_idx'])


def password_cracker_internal_initial(s, ps, curr_ans, ans):
    if len(ans):
        return

    if s == "":
        ans.append([w for w in curr_ans])
        return

    for p in ps:
        if s.startswith(p):
            curr_ans.append(p)
            password_cracker_internal(s[len(p):], ps, curr_ans, ans)
            curr_ans.pop()


def search_ps(s, ps, p_idx=0):
    for i in range(p_idx, len(ps)):
        if s.startswith(ps[i]):
            return i

    return None


def password_cracker_internal(s, ps):
    memo = set()
    stack = [StackInfo(0, 0)]

    while stack:
        si = stack.pop()

        new_p_idx = search_ps(s[si.s_idx:], ps, si.p_idx)
        if new_p_idx != None:
            p = ps[new_p_idx]
            if si.s_idx + len(p) == len(s):
                stack.append(StackInfo(si.s_idx + len(p), new_p_idx+1))
                return [s.p_idx - 1 for s in stack]
            else:
                stack.append(StackInfo(si.s_idx, new_p_idx+1))
                stack.append(StackInfo(si.s_idx + len(p), 0))

    return []


def password_cracker_internal2(s, ps):
    valids = [False for _ in range(len(s)+1)]

    # print(ps)
    for s_idx in range(len(s)):
        if s_idx != 0 and valids[s_idx] == False:
            continue

        for p in ps:
            # print(s[s_idx:], p, s_idx, s_idx + len(p))
            # if s[s_idx:].startswith(p):
            if s.find(p, s_idx, s_idx+len(p)) != -1:
                v_idx = s_idx + len(p)
                if v_idx < len(valids):
                    valids[v_idx] = True

    # print(valids)
    # print(valids[-1])
    if valids[-1] == False:
        return []
    else:
        return password_cracker_internal(s, ps)


def reduce_search_passowrd(ps):
    if len(ps) <= 1:
        return ps

    is_reduced = [False for _ in range(len(ps))]

    for i, p in enumerate(ps):
        ans = password_cracker_internal(p, ps[0:i] + ps[i+1:])
        if ans:
            is_reduced[i] = True

    return [p for r, p in zip(is_reduced, ps) if not r]


def list_distinct_passwords(ps):
    ds = [True for _ in range(len(ps))]
    for i in range(len(ps)):
        for j in range(i+1, len(ps)):
            if set(ps[i]) & set(ps[j]):
                ds[i] = False
                ds[j] = False

    return [p for p, d in zip(ps, ds) if d]


def split_s(s, ds):
    split_idxs = []
    for d in ds:
        s_idx = 0
        while s_idx < len(s):
            b = s.find(d, s_idx)
            if b != -1:
                split_idxs.append((b, b+len(d)))
                s_idx = b + len(d)
            else:
                break

    split_idxs.sort(key=lambda x:x[0])

    if not split_idxs:
        return []

    if split_idxs[0][0] != 0:
        e = split_idxs[0][0]
        split_idxs = [(0, e)] + split_idxs

    if split_idxs[-1][1] != len(s):
        b = split_idxs[-1][1]
        split_idxs.append((b, len(s)))

    prev_si = None
    full_split_idxs = []
    for si in split_idxs:
        if prev_si != None and prev_si[1] != si[0]:
            full_split_idxs.append((prev_si[1], si[0]))
        full_split_idxs.append(si)
        prev_si = si

    return full_split_idxs


def passwordCracker2(ps, s):
    ps = reduce_search_passowrd(ps)
    ds = list_distinct_passwords(ps)
    split_idxs = split_s(s, ds)

    # No distinct passwords to split the string to several substrings
    if not split_idxs:
        split_idxs.append((0, len(s)))

    ans = []
    for b, e in split_idxs:
        curr_ans = password_cracker_internal2(s[b:e], ps)
        if not curr_ans:
            return []
        else:
            ans.append(curr_ans)

    return [ps[p_idx] for p_idx in itertools.chain(*ans)]


def passwordCracker(ps, s):
    ps = reduce_search_passowrd(ps)
    ds = list_distinct_passwords(ps)
    ans = password_cracker_internal(s, ps)

    return [ps[p_idx] for p_idx in ans]


def main():
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        ps = input().strip().split(' ')
        attempt = input().strip()
        # print(ps, attempt, len(attempt))
        result = list(passwordCracker2(ps, attempt))
        if result:
            print(" ".join(result))
        else:
            print("WRONG PASSWORD")


if __name__ == "__main__":
    main()

