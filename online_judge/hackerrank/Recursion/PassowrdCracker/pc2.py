#!/bin/python3

from collections import namedtuple
import itertools


def find_pwds_combs(s, ps):
    valids = [-1 for _ in range(len(s)+1)]

    for s_idx in range(len(s)):
        if s_idx != 0 and valids[s_idx] == -1:
            continue

        for p_idx, p in enumerate(ps):
            if s.find(p, s_idx, s_idx+len(p)) != -1:
                v_idx = s_idx + len(p)
                if v_idx < len(valids):
                    valids[v_idx] = p_idx

    # WRONG ANSWER
    if valids[-1] == -1:
        return []

    # There is at lease one combination
    valids = valids[::-1]
    v_idx = 0
    combs = []

    while v_idx < len(s):
        p = ps[valids[v_idx]]
        combs.append(p)
        v_idx += len(p)

    return combs[::-1]


def reduce_search_passowrd(ps):
    if len(ps) <= 1:
        return ps

    is_reduced = [False for _ in range(len(ps))]

    for i, p in enumerate(ps):
        ans = find_pwds_combs(p, ps[0:i] + ps[i+1:])
        if ans:
            is_reduced[i] = True

    return [p for r, p in zip(is_reduced, ps) if not r]


def passwordCracker(ps, s):
    ps = reduce_search_passowrd(ps)
    ans = find_pwds_combs(s, ps)
    return ans


def main():
    t = int(input().strip())
    for a0 in range(t):
        n = int(input().strip())
        ps = input().strip().split(' ')
        attempt = input().strip()
        result = list(passwordCracker(ps, attempt))
        if result:
            print(" ".join(result))
        else:
            print("WRONG PASSWORD")


if __name__ == "__main__":
    main()

