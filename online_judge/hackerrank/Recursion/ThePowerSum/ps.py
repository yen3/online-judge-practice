#!/bin/python3


class Power(object):
    def __init__(self, n, power, used):
        self.n = n
        self.power = power
        self.used = used

    def __repr__(self):
        return "({n},{power},{used})".format(n=self.n, power=self.power,
                                             used=self.used)


def gen_power_list(p, max_num):
    ps = []

    for i in range(1, max_num+1):
        pi = i ** p
        if pi > max_num:
            return ps

        ps.append(Power(i, pi, False))


def power_sum_internal(i, x, ps, combs):
    if x == 0:
        combs.add(tuple(p.n for p in ps if p.used))
        return

    while i < len(ps):
        if ps[i].power > x:
            return

        if ps[i].used == True:
            i += 1
            continue

        ps[i].used = True
        power_sum_internal(i+1, x - ps[i].power, ps, combs)
        ps[i].used = False

        i += 1


def power_sum(x, n):
    ps = gen_power_list(n, x)
    combs = set()
    power_sum_internal(0, x, ps, combs)

    return len(combs)


def main():
    X = int(input().strip())
    N = int(input().strip())
    result = power_sum(X, N)
    print(result)


if __name__ == "__main__":
    main()

