#!/bin/python3

import sys

def digit_sum(n, k):
    # initial step
    ns = sum([ord(c) - ord('0') for c in n]) * k

    while ns > 10:
        ns = sum([ord(c) - ord('0') for c in str(ns)])

    return ns


def main():
    n, k = input().strip().split(' ')
    n, k = [str(n), int(k)]
    result = digitSum(n, k)
    print(result)


if __name__ == "__main__":
    main()

