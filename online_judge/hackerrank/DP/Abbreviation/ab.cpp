#include <iostream>
#include <vector>
#include <string>
#include <cctype>

bool abbreviation(const std::string& s, const std::string& t) {
  // 0: does not match
  // 1: match
  std::vector<std::vector<std::uint8_t>> m(
      t.size() + 1, std::vector<std::uint8_t>(s.size() + 1, 0));

  // init the table
  for (std::size_t j = 0; j < m[0].size(); ++j) {
    m[0][j] = 1;
  }

  for (std::size_t i = 1; i < m.size(); ++i) {
    for (std::size_t j = i; j < m[i].size(); ++j) {
      if (m[i-1][j-1] && std::toupper(s[j-1]) == t[i-1]) {
          m[i][j] = 1;
      }
      if (m[i][j-1] && std::islower(s[j-1])) {
          m[i][j] = 1;
      }
    }
  }

  return m[t.size()][s.size()] == 1;
}

int main() {
  std::cout << abbreviation("daBcd", "ABC") << std::endl;
  std::cout << abbreviation("AbcDE", "ABDE") << std::endl;
  std::cout << abbreviation("AccCCC", "ACC")  << std::endl;
  std::cout << abbreviation("AccCCC", "ACCC")  << std::endl;
  std::cout << abbreviation("beFgh", "EFH")  << std::endl;
  return 0;
}
