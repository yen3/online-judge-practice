#include <iostream>
#include <vector>
#include <algorithm>

int maxSubsetSum(const std::vector<int>& xs) {
  std::vector<int> sub_sum(xs.size(), 0);

  sub_sum[0] = xs[0];
  sub_sum[1] = std::max(xs[0], xs[1]);

  for (std::size_t i = 2; i < xs.size(); ++i) {
    int candiates[3] = {sub_sum[i-1], sub_sum[i-2] + xs[i], xs[i]};
    sub_sum[i] = *std::max_element(candiates, candiates+3);
  }

  return *std::max_element(sub_sum.begin(), sub_sum.end());
}

int main() {
  std::cout << maxSubsetSum({-2, 1, 3, -4, 5}) << std::endl;
  std::cout << maxSubsetSum({3, 7, 4, 6, 5}) << std::endl;
  std::cout << maxSubsetSum({3, 5, -7, 8, 10}) << std::endl;
}

