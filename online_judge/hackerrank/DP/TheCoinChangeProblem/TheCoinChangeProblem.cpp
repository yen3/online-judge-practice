#if defined(__clang__)
#include "../stdc++.h"
#elif defined(__GNUG__) || defined(__GNUC__)
#include <bits/stdc++.h>
#endif

uint64_t getWays(long n, const std::vector<long>& c) {
  std::vector<uint64_t> ways(n + 1, 0);
  ways[0] = 1;

  for (std::size_t i = 0; i < c.size(); ++i) {
    for (std::size_t j = c[i]; j < ways.size(); ++j) {
      ways[j] += ways[j - c[i]];
    }
  }
  return ways.back();
}

#ifdef SELF_TEST
void test() {
  assert(getWays(4, {1, 2, 3}) == 4);
  assert(getWays(10, {2, 5, 3, 6}) == 5);
  assert(getWays(250, {41, 34, 46, 9,  37, 32, 42, 21, 7,  13, 1,  24,
                       3,  43, 2,  23, 8,  45, 19, 30, 29, 18, 35, 11}) ==
         15685693751);

  std::exit(0);
}
#endif /* SELF_TEST */

int main() {
#ifdef SELF_TEST
  test();
#endif /* SELF_TEST */

  int n;
  int m;
  std::cin >> n >> m;
  std::vector<long> c(m);
  for (int c_i = 0; c_i < m; c_i++) {
    std::cin >> c[c_i];
  }

  // Print the number of ways of making change for 'n' units using coins having
  // the values given by 'c'
  uint64_t ways = getWays(n, c);
  std::cout << ways << std::endl;

  return 0;
}
