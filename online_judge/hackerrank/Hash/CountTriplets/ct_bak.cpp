#include <iostream>
#include <vector>
#include <map>
#include <cstdint>
#include <algorithm>
#include <functional>

typedef std::map<std::uint32_t, std::vector<std::uint32_t> > NumLocations;
typedef NumLocations::iterator NumLocationsIter;
typedef NumLocations::const_iterator NumLocationsConstIter;

const std::size_t kDist = 3;

std::size_t find_more_than_start_index(std::uint32_t find_val,
                                       const std::vector<std::uint32_t>& xs) {
  for (std::size_t i = 0; i < xs.size(); ++i) {
    if (xs[i] > find_val) {
      return i;
    }
  }

  return xs.size();
}

std::size_t countPermutations(
    const std::vector<std::vector<std::uint32_t> >& loc_groups) {
  std::size_t num_perms = 0;

  for (std::size_t i = 0; i < loc_groups[0].size(); ++i) {
    std::uint32_t loc = loc_groups[0][i];

    std::uint32_t curr_perm = 1;
    std::uint32_t curr_dist = 1;

    for (std::size_t g = 1; g < loc_groups.size(); ++g) {
      std::size_t find_index = find_more_than_start_index(loc, loc_groups[g]);
      if (find_index == loc_groups[g].size()) {
        break;
      }

      curr_perm *= (loc_groups[g].size() - find_index);
      ++curr_dist;

      loc = loc_groups[g][find_index];
    }

    if (curr_dist == loc_groups.size()) {
      num_perms += curr_perm;
    }
  }

  return num_perms;
}

std::size_t countPermutations(const std::vector<NumLocationsConstIter>& groups) {
  std::vector<std::vector<std::uint32_t> > gs(
      groups.size(), std::vector<std::uint32_t>());

  for (std::size_t i = 0; i < groups.size(); ++i) {
    for (std::size_t j = 0; j < groups[i]->second.size(); ++j) {
      gs[i].push_back(groups[i]->second[j]);
    }
  }

  return countPermutations(gs);
}

void find_consenquence_num_iters(NumLocationsConstIter iter,
                                 std::vector<NumLocationsConstIter>& groups,
                                 const NumLocations& nloc,
                                 std::uint32_t r) {
  groups.push_back(iter);

  NumLocationsConstIter iter_end = nloc.end();
  for (std::size_t d = 1; d < kDist; ++d) {
    NumLocationsConstIter find_iter = nloc.find(groups.back()->first * r);
    if (find_iter == iter_end) {
      return;
    }

    groups.push_back(find_iter);
  }
}

std::size_t comb(std::uint32_t n, std::uint32_t k) {
   std::size_t comb = 1;

   for (std::size_t i = n-k+1; i <= n; ++i) {
     comb *= i;
   }

   std::size_t d = 1;
   for (std::size_t i = 2; i <= k; ++i) {
     d *= i;
   }

   return comb / d;
}

std::size_t countOneNumCombiantion(std::size_t n, std::size_t dist) {
  std::size_t c = 0;
  for (std::size_t d = dist - 1; d <= n - 1;  ++d) {

    std::cout << d << " " << dist-1 << std::endl;
    c += comb(d, dist - 1);
  }
  return c;
}

std::size_t countTripletsOne(const NumLocations& nloc) {
  NumLocationsConstIter iter_end = nloc.end();

  int counts = 0;
  for (NumLocationsConstIter iter = nloc.begin(); iter != iter_end; ++iter) {
    if (iter->second.size() >= kDist) {
      counts += countOneNumCombiantion(iter->second.size(), kDist);
    }
  }

  return counts;
}

std::size_t countTriplets(const std::vector<std::uint32_t>& xs,
                          std::uint32_t r) {
  std::size_t number_permutations = 0;

  NumLocations nloc;

  for (std::uint32_t i = 0; i < xs.size(); ++i) {
    nloc[xs[i]].push_back(i);
  }

  if (r == 1) {
    return countTripletsOne(nloc);
  }

  NumLocationsConstIter iter_end = nloc.end();
  for (NumLocationsIter iter = nloc.begin(); iter != iter_end; ++iter) {
    std::vector<NumLocationsConstIter> groups;
    find_consenquence_num_iters(iter, groups, nloc, r);

    if (groups.size() == kDist) {
      number_permutations += countPermutations(groups);
    }
  }

  return number_permutations;
}

int main() {
  std::vector<std::uint32_t> ns(100, 1);
  std::cout << countTriplets(ns, 1) << std::endl;

  //std::cout << countTriplets({1, 3, 9, 9, 27, 81}, 3) << std::endl;
  //std::cout << countTriplets({1, 2, 2, 4}, 2) << std::endl;
  //std::cout << countTriplets({1, 5, 5, 25, 125}, 5) << std::endl;
}
