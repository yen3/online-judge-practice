#include <iostream>
#include <vector>
#include <map>
#include <cstdint>
#include <algorithm>
#include <functional>

typedef std::map<std::uint64_t, std::vector<std::uint64_t> > NumLocations;
typedef NumLocations::iterator NumLocationsIter;
typedef NumLocations::const_iterator NumLocationsConstIter;

const std::uint64_t kDist = 3;

std::uint64_t find_more_than_start_index(std::uint64_t find_val,
                                         const std::vector<std::uint64_t>& xs) {
  for (std::uint64_t i = 0; i < xs.size(); ++i) {
    if (xs[i] > find_val) {
      return i;
    }
  }

  return xs.size();
}

std::uint64_t countPermutations(
    const std::vector<std::vector<std::uint64_t> >& loc_groups) {
  std::uint64_t num_perms = 0;

  for (std::uint64_t i = 0; i < loc_groups[0].size(); ++i) {
    std::uint64_t loc = loc_groups[0][i];

    std::uint64_t curr_perm = 1;
    std::uint64_t curr_dist = 1;

    for (std::uint64_t g = 1; g < loc_groups.size(); ++g) {
      std::uint64_t find_index = find_more_than_start_index(loc, loc_groups[g]);
      if (find_index == loc_groups[g].size()) {
        break;
      }

      curr_perm *= (loc_groups[g].size() - find_index);
      ++curr_dist;

      loc = loc_groups[g][find_index];
    }

    if (curr_dist == loc_groups.size()) {
      num_perms += curr_perm;
    }
  }

  return num_perms;
}

std::uint64_t countPermutations(const std::vector<NumLocationsConstIter>& groups) {
  std::vector<std::vector<std::uint64_t> > gs(
      groups.size(), std::vector<std::uint64_t>());

  for (std::uint64_t i = 0; i < groups.size(); ++i) {
    for (std::uint64_t j = 0; j < groups[i]->second.size(); ++j) {
      gs[i].push_back(groups[i]->second[j]);
    }
  }

  return countPermutations(gs);
}

void find_consenquence_num_iters(NumLocationsConstIter iter,
                                 std::vector<NumLocationsConstIter>& groups,
                                 const NumLocations& nloc,
                                 std::uint64_t r) {
  groups.push_back(iter);

  NumLocationsConstIter iter_end = nloc.end();
  for (std::uint64_t d = 1; d < kDist; ++d) {
    NumLocationsConstIter find_iter = nloc.find(groups.back()->first * r);
    if (find_iter == iter_end) {
      return;
    }

    groups.push_back(find_iter);
  }
}

std::uint64_t comb(std::uint64_t n, std::uint64_t k) {
   std::uint64_t comb = 1;

   for (std::uint64_t i = n-k+1; i <= n; ++i) {
     comb *= i;
   }

   std::uint64_t d = 1;
   for (std::uint64_t i = 2; i <= k; ++i) {
     d *= i;
   }

   return comb / d;
}

std::uint64_t countOneNumCombiantion(std::uint64_t n, std::uint64_t dist) {
  return comb(n, dist);
}

std::uint64_t countTripletsOne(const NumLocations& nloc) {
  NumLocationsConstIter iter_end = nloc.end();

  std::uint64_t counts = 0;
  for (NumLocationsConstIter iter = nloc.begin(); iter != iter_end; ++iter) {
    if (iter->second.size() >= kDist) {
      counts += countOneNumCombiantion(iter->second.size(), kDist);
    }
  }

  return counts;
}

std::uint64_t countTriplets(const std::vector<std::uint64_t>& xs,
                          std::uint64_t r) {
  std::uint64_t number_permutations = 0;

  NumLocations nloc;

  for (std::uint64_t i = 0; i < xs.size(); ++i) {
    nloc[xs[i]].push_back(i);
  }

  if (r == 1) {
    return countTripletsOne(nloc);
  }

  NumLocationsConstIter iter_end = nloc.end();
  for (NumLocationsIter iter = nloc.begin(); iter != iter_end; ++iter) {
    std::vector<NumLocationsConstIter> groups;
    find_consenquence_num_iters(iter, groups, nloc, r);

    if (groups.size() == kDist) {
      number_permutations += countPermutations(groups);
    }
  }

  return number_permutations;
}

int main() {
  {
    std::vector<std::uint64_t> ns(100, 1);
    std::cout << countTriplets(ns, 1) << std::endl;
  }

  {
    std::cout << comb(100000, 3) << std::endl;
    std::vector<std::uint64_t> ns(100000, 1);
    std::cout << countTriplets(ns, 1) << std::endl;
  }

  {
  }

  //std::cout << countTriplets({1, 3, 9, 9, 27, 81}, 3) << std::endl;
  //std::cout << countTriplets({1, 2, 2, 4}, 2) << std::endl;
  //std::cout << countTriplets({1, 5, 5, 25, 125}, 5) << std::endl;
}
