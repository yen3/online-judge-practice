#include <iostream>
#include <vector>
#include <algorithm>
#include <cassert>

std::vector<int> ReadList() {
  std::size_t size;
  std::cin >> size;

  std::vector<int> xs;
  for (std::size_t i = 0; i < size; ++i) {
    int num;
    std::cin >> num;
    xs.push_back(num);
  }

  return xs;
}

std::vector<int> DiffList(std::vector<int>& xs,
                          std::vector<int>& ys) {
  std::sort(xs.begin(), xs.end());
  std::sort(ys.begin(), ys.end());

  std::vector<int> ds;
  std::size_t xidx = 0, yidx = 0;
  for (; xidx < xs.size() && yidx < ys.size(); ) {
    if (xs[xidx] == ys[yidx]) {
      xidx++;
      yidx++;
    } else if (xs[xidx] < ys[yidx]) {
      if (ds.size() == 0 || xs[xidx] != ds.back())  {
        ds.push_back(xs[xidx]);
      }
      xidx++;
    } else {
      if (ds.size() == 0 || ys[yidx] != ds.back()) {
        ds.push_back(ys[yidx]);
      }
      yidx++;
    }
  }

  if (xidx < xs.size()) {
    for (; xidx < xs.size(); ++xidx) ds.push_back(xs[xidx]);
  }
  if (yidx < ys.size()) {
    for (; yidx < ys.size(); ++yidx) ds.push_back(ys[yidx]);
  }

  return ds;
}

void Test() {
  std::vector<int> xs{203, 204, 205, 206, 207, 208, 203, 204, 205, 206};
  std::vector<int> ys{203, 204, 204, 205, 206, 207, 205, 208, 203, 206, 205, 206, 204};

  std::vector<int> ds = DiffList(xs, ys);
  assert(ds.size() == 3);
  assert(ds[0] == 204);
  assert(ds[1] == 205);
  assert(ds[2] == 206);
}

int main() {
  Test();

  std::vector<int> xs = ReadList();
  std::vector<int> ys = ReadList();

  std::vector<int> ds = DiffList(xs, ys);

  for (auto d : ds) {
    std::cout << d << " ";
  }
  std::cout << std::endl;

  return 0;
}

