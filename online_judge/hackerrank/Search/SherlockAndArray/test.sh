#!/usr/bin/env bash

answer="YES
YES
YES
NO
YES
YES
YES
YES
YES
YES"

output=$(echo "10
1
1
1
2
1
3
2
1 2
3
1 4 1
3
1 5 1
1
234
1
20000
3
6 23 6
1
1" | python3 ./sherlock_and_array.py)

if [ x"${output}" != x"${answer}" ]; then
    echo "test fail"
else
    echo "test pass"
fi
