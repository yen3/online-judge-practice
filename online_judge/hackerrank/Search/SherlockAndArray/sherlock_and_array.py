#!/bin/python3


def sherlock_and_array(nums):
    if len(nums) == 1:
        return True

    if len(nums) < 3:
        return False

    left_sum = 0
    right_sum = sum(nums[1:])

    for midx in range(0, len(nums) - 2):
        left_sum = left_sum + nums[midx]
        right_sum = right_sum - nums[midx + 1]

        if left_sum == right_sum:
            return True

    return False


def main():
    T = int(input().strip())

    for a0 in range(T):
        n = int(input().strip())
        a = list(map(int, input().strip().split(' ')))

        result = sherlock_and_array(a)
        print("YES" if result else "NO")


if __name__ == "__main__":
    main()
