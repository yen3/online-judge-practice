#!/usr/bin/py

def pairs(nums, target):
    nums.sort()

    answer = 0
    bidx = 0
    eidx = 1
    while bidx != len(nums) and eidx != len(nums):
        v = abs(nums[eidx] - nums[bidx])
        if bidx == eidx:
            eidx = eidx + 1
        elif v == target:
            answer = answer + 1
            eidx = eidx + 1
        elif v < target:
            eidx = eidx + 1
        else:
            bidx = bidx + 1

    return answer


def main():
    a = input().strip()
    a = list(map(int, a.split(' ')))
    _a_size = a[0]
    _k = a[1]
    b = input().strip()
    b = list(map(int, b.split(' ')))
    print(pairs(b,_k))


if __name__ == '__main__':
    main()

