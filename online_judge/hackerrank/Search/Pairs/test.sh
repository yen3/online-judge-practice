#!/usr/bin/env bash

val=$(echo "5 2  
1 5 3 4 2 " | python3 pairs.py)

if [ x"${val}" != x"3" ] ; then
    echo "test fail"
else
    echo "test pass"
fi
