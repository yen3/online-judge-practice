#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <fstream>
#include <iterator>

typedef std::vector<int> VectorInt;

std::size_t find_max_less_than_equal_index(int n, const VectorInt& xs);
std::size_t find_max_less_than_equal_index2(int n, const VectorInt& xs);

std::size_t triplets_internal(const VectorInt& a, const VectorInt& b,
                              const VectorInt& c);

std::size_t triplets(const std::set<int>& a,
                     const std::set<int>& b,
                     const std::set<int>& c);

std::size_t find_max_less_than_equal_index(int n, const VectorInt& xs) {
  if (xs[0] > n) {
    return xs.size();
  }

  for (std::size_t i = 1; i < xs.size(); ++i) {
    if (xs[i] > n) {
      return i - 1;
    }
  }

  return xs.size() - 1;
}

std::size_t find_max_less_than_equal_index2(int n, const VectorInt& xs) {
  // Not found
  if (xs[0] > n) {
    return xs.size();
  }

  // All is less than n
  if (xs.back() <= n) {
    return xs.size() - 1;
  }

#if 0
  std::size_t bidx = 0;
  std::size_t eidx = xs.size() - 1;
  while(bidx < eidx) {
    if (eidx - bidx == 1) {
      return bidx;
    }

    std::size_t midx = bidx + (eidx - bidx) / 2;

    if (xs[midx] == n) {
      return midx;
    } else if (xs[midx] > n) {
      eidx = midx;
    } else {
      bidx = midx;
    }
  }

  // Impossible
  return xs.size();
#else
  auto iter = std::upper_bound(xs.begin(), xs.end(), n);
  if (iter != xs.end() && *iter != n) {
      --iter;
  }

  return std::distance(xs.begin(), iter);
#endif
}


std::size_t triplets(const std::set<int>& as,
                     const std::set<int>& bs,
                     const std::set<int>& cs) {
  VectorInt a;
  VectorInt b;
  VectorInt c;

  std::copy(as.begin(), as.end(), std::back_inserter(a));
  std::copy(bs.begin(), bs.end(), std::back_inserter(b));
  std::copy(cs.begin(), cs.end(), std::back_inserter(c));

  return triplets_internal(a, b, c);
}

std::size_t triplets_internal(const VectorInt& a, const VectorInt& b,
                              const VectorInt& c) {
  std::size_t triplet_sums = 0;

  for (std::size_t bidx = 0; bidx < b.size(); ++bidx) {
    std::size_t aidx = find_max_less_than_equal_index2(b[bidx], a);
    std::size_t cidx = find_max_less_than_equal_index2(b[bidx], c);

    if (aidx == a.size() || cidx == c.size()) {
      continue;
    }

    triplet_sums += (aidx + 1) * (cidx + 1);
  }

  return triplet_sums;
}

void read_set_from_stdin(std::set<int>& xs, std::size_t input_size) {
  int n;
  for (std::size_t i = 0; i < input_size; ++i) {
    std::cin >> n;
    xs.insert(n);
  }
}

int main() {
  std::ofstream fout(getenv("OUTPUT_PATH"));

  std::size_t input_a_size;
  std::size_t input_b_size;
  std::size_t input_c_size;

  std::cin >> input_a_size >> input_b_size >> input_c_size;

  std::set<int> as;
  std::set<int> bs;
  std::set<int> cs;

  read_set_from_stdin(as, input_a_size);
  read_set_from_stdin(bs, input_b_size);
  read_set_from_stdin(cs, input_c_size);

  std::size_t ans = triplets(as, bs, cs);

  fout << ans << "\n";

  fout.close();

  return 0;
}
