#!/bin/bash

set -ex

clang++ -Wall -Wextra -std=c++17 -O3 -g ./triple_sum.cpp
time OUTPUT_PATH=output-test.txt ./a.out < input1.txt
time OUTPUT_PATH=output-test2.txt ./a.out < input2.txt

cat ./output-test.txt
cat ./output1.txt

echo ""

cat ./output-test2.txt
cat ./output2.txt
