#ifdef SELF_TEST
#include <iostream>
#include <set>
#include <vector>
#else
#include <bits/stdc++.h>
#endif

int hackerlandRadioTransmitters(std::vector<int> x, int k) {
  if (x.empty()) {
    return 0;
  }

  int end_power = -1;
  int num_powers = 0;

  for (std::size_t i = 0; i < x.size(); ++i){
    if (x[i] > end_power) {
      int perfect_power = x[i] + k;

      std::size_t j = i;
      for (; j < x.size() && x[j] <= perfect_power; ++j);
      j--;

      end_power = x[j] + k;
      num_powers++;
    }
  }

  return num_powers;
}

std::vector<int> read_input(int n) {
  std::set<int> s;
  int h;
  for (std::size_t i = 0; i < n; ++i) {
    std::cin >> h;
    s.insert(h);
  }

  std::vector<int> x;
  for (auto iter = s.begin(); iter != s.end(); ++iter) {
    x.push_back(*iter);
  }

  return x;
}

int main() {
    int n;
    int k;

    std::cin >> n >> k;
    std::vector<int> x = read_input(n);

    int result = hackerlandRadioTransmitters(x, k);
    std::cout << result << std::endl;

    return 0;
}

