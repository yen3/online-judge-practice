#!/usr/bin/env bash

set -e

clang++ -g -Wall -DSELF_TEST -std=c++11 ./hrt.cpp -o hrt

result1=$(./hrt < input.txt)
result2=$(./hrt < input2.txt)
result3=$(./hrt < input3.txt)

[ x"$result1" == x"3" ] || (echo "error for input.txt" && exit 1)
[ x"$result2" == x"2" ] || (echo "error for input2.txt" && exit 1)
[ x"$result3" == x"4" ] || (echo "error for input3.txt" && exit 1)

rm -rf hrt
rm -rf hrt.dSYM
