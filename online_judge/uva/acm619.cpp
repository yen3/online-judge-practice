#include <iostream>
#include <string>
#include <iomanip>
#include <cstdlib>
using namespace std;

void ensure_longnum_length();
void n26_array();
void run_string(string);
void run_number(string);
void output(int [],int answer_size,string &enter_string);
void output(string &output_string_word,string &enter_string_number);


const int longnum=10000;   /* 大數單位數長度為0的個數 */ 
const int N26=20;
unsigned int n26[N26][N26],n26_size[N26],longnum_length=0,string_basic_length=22;

/* 確定大數的位數，預設值為4 */
void ensure_longnum_length(){
   int temp_longnum=longnum;
   while(temp_longnum!=1){
      temp_longnum/=10;
      longnum_length++;
   }
}

/*建立26^1~26^(N-1)次方的陣列 */
void n26_array(){
   
   /* 歸零and 長度等於一 */
   for(unsigned int i=0;i<N26;i++){
      for(unsigned int j=0;j<N26;j++){
         n26[i][j]=0;
      }             
   }    
   for(unsigned int i=0;i<N26;i++) n26[i][0]=n26_size[i]=1;
   
   /* 開始運算 */ 
   for(unsigned int i=0;i<N26;i++){         
      /*為了26的0次方*/
      if(i==0) continue;
      
      /*建立暫存陣列*/
      int t[N26];
      for(unsigned int j=0;j<N26;j++) t[j]=n26[i][j];
      for(unsigned int j=0;j<N26;j++) n26[i][j]=0;
      
      /* 開始進行運算和大數改良式進位 */
      for(unsigned int j=0;j<n26_size[i];j++){
         n26[i][j]+=t[j]*26;
            if(n26[i][j]>=longnum){
               n26[i][j+1]+=n26[i][j]/longnum;
               n26[i][j]%=longnum;
            }
      }
      
      /* 確認位數 */
      for(int j=N26-1;j>=0;j--){
         if(n26[i][j]!=0){
            n26_size[i]=j+1;
            break;
         }
      }
      
      /* 避免使用非法記憶體 */
      if(i+1==N26) break;
      
      /* 複製到下一階陣列 */
      for(unsigned int j=0;j<N26;j++)   n26[i+1][j]=n26[i][j];
      n26_size[i+1]=n26_size[i];
   }
    /* 印出26的n次方列表 */
   /* 
   for(unsigned int i=0;i<N26;i++){
      cout << i << "   ";
      for(int j=n26_size[i]-1;j>=0;j--){
         if(j==n26_size[i]-1)   cout << n26[i][j];     
         else   cout << setw(4) << setfill('0') <<n26[i][j];
      }
      cout << endl;
   }     
   */
}

/* 把字元變成數字 */ 
void run_string(string enter_string){
   /* 字元反轉 */
   string a;
   for(int i=enter_string.length()-1;i>=0;i--)  a+=enter_string[i];
   
   /* 歸零 */
   int n[N26],n_size=1;
   for(unsigned int i=0;i<N26;i++) n[i]=0;
   
   for(unsigned int i=0;i<a.length();i++){
      /* 建立暫存陣列 */
      int temp_times=a[i]-'a'+1;
      int t[N26],t_size=1;
      for(unsigned int j=0;j<N26;j++) t[j]=0;
      
      /* 暫存陣列進行運算 */
      for(unsigned int j=0;j<n26_size[i];j++){
         t[j]+=n26[i][j]*temp_times;
         if(t[j]>=longnum){
            t[j+1]+=t[j]/longnum;
            t[j]%=longnum;                  
         }             
      }
      
      /* 暫存陣列確定長度大小 */
      for(int j=N26-1;i>=0;j--){
         if(t[j]!=0){
           t_size=j+1;
           break;          
         }             
      }
      
      /* 暫存陣列加到解答陣列中 */
      for(unsigned int j=0;j<t_size;j++) n[j]+=t[j];
      
      /* 解答陣列確定沒有溢位 */
      for(unsigned int j=0;j<N26;j++){
         if(n[j]>=longnum){
            n[j+1]+=n[j]/longnum;
            n[j]%=longnum;
         }
      }
   }
   
   /* 確定解答陣列長度大小*/
   for(int i=N26-1;i>=0;i--){
      if(n[i]!=0){
         n_size=i+1;
         break;            
      }        
   }
   
   output(n,n_size,enter_string);
   
   /* 印出解答陣列 */
   /*
   for(int i=n_size-1;i>=0;i--) cout << n[i];
   cout << endl;
   */
}

/* 數字轉字元 */
void run_number(string w){
   string a(N26,char(int('a')-1));  /* 解答字串預設為-1 */
   unsigned int n[N26],n_size=0;
   
   /* 歸零 */
   for(unsigned int i=0;i<N26;i++) n[i]=0;
   
   /* 進行位數大數轉換 */
   for(int i=w.length()-1,p=0,times=0;i>=0;i--){
      /* 
         cmath 中的pow(double,double) 函式使用會錯誤 
         故使用loop來製造pow
      */
      int t=1;
      for(unsigned int j=0;j<times;j++) t*=10;
 
      n[p]+=int(w[i]-'1'+1)*t;
      times++;
         if(times%longnum_length==0){
            times=0;
            p++;
         }
   }

   
   while(1){
      /* 確定轉換後的大數長度 */
      for(int i=N26-1;i>=0;i--){
         if(n[i]!=0){
            n_size=i+1;
            break;
         }
      }
   
      int p_n=0;   /*p_n為指向那一數做的加減*/
   
      /* 確定從那一位開始減 */
      for(int i=N26-1;i>=0;i--){
         if(n_size < n26_size[i])   continue;
         else if(n_size>n26_size[i]){
            p_n=i;
            break;
         }
         else if(n_size == n26_size[i]){
            int t_control=0;
            for(int j=n_size-1;j>=0;j--){
               if(n[j]==n26[i][j])   continue;
               else if(n[j]>n26[i][j]){
                  p_n=i;
                  break;
               }
               else if(n[j]<n26[i][j]){
                  t_control=1;
                  break;
               }
               else continue;
            }
            if(t_control==0){   
               p_n=i;
               break;
            }
            if(t_control==1)   continue;
         }
         else continue;     
      }
     
      /*  確定要減的是26的那一次方 */
      /*
      cout << p_n << "   ";
      for(int i=n26_size[p_n]-1;i>=0;i--) cout << setw(4) << n26[p_n][i];
      cout << "    ";
      for(int i=n_size-1;i>=0;i--) cout << setw(4) << n[i];
      cout << endl;
      */
      /* 避免overflow */
      while(a[p_n]=='z') p_n--;   
      
      /* 進行減法運算 */
      for(unsigned int i=0;i<n26_size[p_n];i++){
         if(n[i]<n26[p_n][i]){
            n[i]+=longnum;
            n[i+1]--;
         }
         n[i]-=n26[p_n][i];
      }
      a[p_n]++;
      
      /* 檢查array是否為 0's arry，若是則跳出，若不是則繼續跑*/
      int control_break=0;
      for(unsigned int i=0;i<n_size;i++){
         if(n[i]!=0){
            control_break=1;
            break;
         }
      }
      if(control_break==0) break;
   }
   
   output(a,w);
   /* 印出解答字串 */
   /*
   for(int i=a.length()-1;i>=0;i--)   cout << a[i];        
   cout << endl;
   */
}


/* 列印word轉number的 run_string*/
void output(int n[],int n_size,string &enter_string){
   string a;
   int temp_times=0;
   
   /* 進行數字轉字串...每三個數字加一個','*/
   for(unsigned int i=0;i<n_size;i++){
      for(unsigned int j=0;j<longnum_length;j++){
         a+=(int('0')+n[i]%10);
         n[i]/=10;
         temp_times++;
         if(temp_times==3){
            a+=',';
            temp_times=0;
         }
      }
   }
   
   
   /*進行字串反轉，同時處理多出來的','*/
   string t_a;
   while(a[a.length()-1]==',' || a[a.length()-1]=='0')   a.erase(a.length()-1);                         
   for(int i=a.length()-1;i>=0;i--) t_a+=a[i];
   a=t_a;
   
   /* 加上所計算的' '且整理成一個陣列，然後把答案印出 */
   int temp_space_times=string_basic_length-enter_string.length();
   for(unsigned int i=0;i<temp_space_times;i++) enter_string+=' ';
   
   enter_string+=a;
   
   cout << enter_string << endl;
}


/* 進行number tranform word之output */
void output(string &w,string &n){
   /* w is word, n is number */
   
   /* 把數字加上, 方法是，先反轉加上,，再反轉回來 */
   string t_n;
   int temp_times=0;
   for(int i=n.length()-1;i>=0;i--){
      t_n+=n[i];
      temp_times++;
      if(temp_times==3){
         t_n+=',';
         temp_times=0;
      }
   }
   while(t_n[t_n.length()-1]==',' || t_n[t_n.length()-1]=='0' ) t_n.erase(t_n.length()-1);
   n.erase();
   for(int i=t_n.length()-1;i>=0;i--) n+=t_n[i];

   /* 刪除word 中多於的符號 */   
   while(w[w.length()-1]==char(int('a')-1)) w.erase(w.length()-1);
   
   /* word 進行字元反轉 */
   string t_w;
   for(int i=w.length()-1;i>=0;i--) t_w+=w[i];
   w=t_w;   
 
   /* 算word後面需要幾個空格，並且加入數字 */
   int temp_space_times = string_basic_length-w.length();
   for(unsigned int i=0;i<temp_space_times;i++) w+=' ';
   w+=n;   /* 這裡就是兩個字串合併 */
   
   /* 印出答案 */
   cout << w << endl;
}

int main(){
   ensure_longnum_length();
   n26_array();   
   string enter_string;
   while(cin >> enter_string){
      if(enter_string=="*")   break;
      if(enter_string[0]>='a' && enter_string[0] <='z')   run_string(enter_string); 
      if(enter_string[0]>='1' && enter_string[0] <='9')   run_number(enter_string);
      
   }
}
