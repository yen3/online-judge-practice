#include <stdio.h>
#define N 1000
struct uesd{
  int x,y;
}tsk[N*N];
char data[N][N];
int height,weight;
int word[26][2];
int times;

int zero(){
  int i,j;
  for(i=0;i<N;i++){
	  for(j=0;j<N;j++){
		data[i][j]=0;
	  }
  }
  for(i=0;i<N*N;i++) tsk[i].x=tsk[i].y=0;
  for(i=0;i<26;i++){ 
	  word[i][0]=0;
	  word[i][1]=i;
  }
  height=weight=0;
return 0;
}

int input(){
 int i;
   
return 0;
}

int run(){
 int i,j,k=0,qt=0;
 char ch;
	for(i=0;i<height;i++){
		for(j=0;j<weight;j++){
			if(data[i][j]>='a' && data[i][j]<='z'){
				ch=data[i][j];
				word[ch-'a'][0]++;
				data[i][j]='0';
				k=0;
			    tsk[k].x=i;
			    tsk[k].y=j;
			    qt=1;
				while(1){
					if(data[tsk[k].x+1][tsk[k].y]==ch){
					   qt++;
					   data[tsk[k].x+1][tsk[k].y]='0';
					   tsk[qt].x=tsk[k].x+1;
					   tsk[qt].y=tsk[k].y;
					}
					if(data[tsk[k].x-1][tsk[k].y]==ch){
					   qt++;
					   data[tsk[k].x-1][tsk[k].y]='0';
					   tsk[qt].x=tsk[k].x-1;
					   tsk[qt].y=tsk[k].y;
					}
					if(data[tsk[k].x][tsk[k].y+1]==ch){
					   qt++;
					   data[tsk[k].x][tsk[k].y+1]='0';
					   tsk[qt].x=tsk[k].x;
					   tsk[qt].y=tsk[k].y+1;
					}
					if(data[tsk[k].x][tsk[k].y-1]==ch){
					   qt++;
					   data[tsk[k].x][tsk[k].y-1]='0';
					   tsk[qt].x=tsk[k].x;
					   tsk[qt].y=tsk[k].y-1;
					}
					if(k>qt) break;
					k++;
				}
				
			}

		}
	}

return 0;
}

int sort(){
  int i,j;
  int tw,tt;
  int tp[26][2],tpn=0;
      for(i=0;i<26;i++){
            if(word[i][0]>0){
                tp[tpn][0]=word[i][0];
                tp[tpn++][1]=word[i][1];
            }
      }
      for(i=0;i<tpn-1;i++){
            for(j=tpn-1;j>i;j--){
                 if(tp[j][0]>tp[j-1][0]){
                    tw=tp[j][0];          tt=tp[j][1];
                    tp[j][0]=tp[j-1][0];  tp[j][1]=tp[j-1][1];
                    tp[j-1][0]=tw;        tp[j-1][1]=tt;
                 }
            }
      }
      for(i=0;i<tpn;i++)   printf("%c:%2d\n",tp[i][1]+'a',tp[i][0]);
      puts("");
return 0;
}

int main(){
  int i,j;
  zero();
  scanf("%d",&times);
   for(i=0;i<times;i++){
      scanf("%d%d",&height,&weight);
         for(j=0;j<height;j++) scanf("%s",data[j]);
      run();
      printf("World #%d\n",i+1);
      sort();
      zero();
  }
return 0;
}

