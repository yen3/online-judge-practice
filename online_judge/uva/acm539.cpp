#include <iostream>
#include <vector>
#define N 30

using namespace std;

int main(){
    unsigned int city=0,relation=0,maxlength=0;
    while(std::cin >> city >> relation){
        if(city==0 && relation==0 ) return 0;
        char run[N][N];
        for(unsigned int i=0;i<city;i++){
            for(unsigned int j=0;j<city;j++){
                run[i][j]=0;
            }     
        }
        for(unsigned int i=0;i<relation;i++){
            int a,b;
            std::cin >> a >> b;
            run[a][b]=run[b][a]=1;
        }
        for(unsigned int i=0;i<city;i++){
            char r[N][N];
            for(unsigned int j=0;j<city;j++){
                for(unsigned int k=0;k<city;k++){
                    r[j][k]=run[j][k];           
                }
            }
            int v[N],vp=0,vj=0,vcontrol=1;
            for(unsigned int j=0;j<N;j++)  v[j]=0;
            v[vp++]=i;
            while(1){
                for(unsigned int j=vj;j<city;j++){
                    if(r[v[vp-1]][j]==1){
                        r[v[vp-1]][j]=r[j][v[vp-1]]=0;   
                        v[vp++]=j;
                        vcontrol=0;
                        vj=0;
                        break;
                    }
                }
                if(vcontrol==1){
                    maxlength=(vp-1>maxlength)?vp-1:maxlength;
                    r[v[vp-1]][v[vp-2]]=r[v[vp-2]][v[vp-1]]=1;
                    vj=v[vp-1]+1;
                    v[--vp]=0;
                    if(vp==0) break;

                }
                vcontrol=1;
            }             
        }
        std::cout << maxlength << endl;
        city=relation=maxlength=0;
    }
    return 0;
}
