#include <stdio.h>
#define P 1000000000
#define N 5100
#define F 200

long f[N][F];
long fibn[N];
int que;

int make_arr(){
  int i,j,max;
	for(i=2;i<N;i++){
		max=(fibn[i-1]>fibn[i-2])?fibn[i-1]:fibn[i-2];
		for(j=0;j<max;j++){
		   f[i][j]+=f[i-1][j]+f[i-2][j];
			if(f[i][j]>=P){
			  f[i][j+1]+=f[i][j]/P;
			  f[i][j]%=P;
			}
		}
	   if(f[i][max]>0) fibn[i]=max+1;
	   else fibn[i]=max;
	}
return 0;
}

int output(){
  int i,j;
  j=que;
    printf("The Fibonacci number for %d is ",que);
    printf("%ld",f[j][fibn[j]-1]);
	for(i=fibn[j]-2;i>=0;i--) printf("%09ld",f[j][i]);
    puts("");
return 0;
}

int main(){
 int i,j;
	for(i=0;i<N;i++){
		for(j=0;j<F;j++){
		   f[i][j]=0;
		}
	   fibn[i]=0;
	}
  fibn[0]=fibn[1]=1;
  f[1][0]=1;
  make_arr();
  
  while(scanf("%d",&que)!=EOF){
     output();
  }

return 0;
}
