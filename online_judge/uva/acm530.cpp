#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int gcd(int x,int y);

int main(){
    int n=0,m=0;
    while(cin >> n >> m){
        if(n==0 && m==0) break;
        int den1=n-m,den2=m;
        if(den1<den2) swap(den1,den2);
        
        vector<int> den,num;   /* den ���� num ���l */
        for(unsigned int i=1;i<=den2;i++) den.push_back(i);
        for(unsigned int i=den1+1;i<=n;i++) num.push_back(i);

        for(unsigned int i=0;i<den.size();i++){
            int j=0;
            while(den[i]!=1){
                int gcd_number=gcd(den[i],num[j]);
                if(gcd_number!=1){
                    den[i]/=gcd_number;
                    num[j]/=gcd_number;
                }
                if(++j==num.size()) j=0;
            }
        }
        unsigned int answer=1;
        for(unsigned int i=0;i<num.size();i++) answer*=num[i];
        cout << answer << endl;        
    }
}

int gcd(int x,int y){
    int r=0;
    while(y!=0){
        r=x%y;
        x=y;
        y=r;    
    }
    return x;
}
