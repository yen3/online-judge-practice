#include <iostream>
#include <string>
#include <vector>
using namespace std;

bool parenthesis_balance(const string& s){
    vector<char> symbol;
    for(unsigned int i=0;i<s.length();i++){
        if(s[i]=='[' || s[i]=='(')   symbol.push_back(s[i]);
        else if(symbol.size()>0 && s[i]==']' && symbol[symbol.size()-1]=='[')   symbol.pop_back();
        else if(symbol.size()>0 && s[i]==')' && symbol[symbol.size()-1]=='(')   symbol.pop_back();
		else   return false;
    }
    if(symbol.empty())   return true;
    else return false;
} 

int main(){
    string s;
    int input_data_sum=0;
    cin >> input_data_sum;
    getline(cin,s);
    for(unsigned int i=0;i<input_data_sum;i++){
        getline(cin,s);
        if(parenthesis_balance(s)) cout << "Yes" << endl;
        else cout << "No" << endl;
    }
}

