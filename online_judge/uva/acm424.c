#include <stdio.h>
#include <assert.h>

//////////////////////////////////////////////////////////////////////////////
// Const value
//////////////////////////////////////////////////////////////////////////////
#define N 256 

//////////////////////////////////////////////////////////////////////////////
// Function delcation
//////////////////////////////////////////////////////////////////////////////
/// Get the length of string @ref s
int string_len(const char* s);

/// reverse string @ref x
void string_rev(char* x);

/// present @ref a += @ref b;
/// @param[in,out] a number array
/// @param[in] b number array
int rev_add_bighum(char* a, int a_size, char* b, int b_size);

/// translate string @ref a to number array
void string_purify(char* a);

/// translate number array a to a string 
void string_decoration(char* a, int a_size);

//////////////////////////////////////////////////////////////////////////////
// Function definition
//////////////////////////////////////////////////////////////////////////////
int string_len(const char* s)
{
  int len = 0;
  for(; *s++ != '\0'; len++) ;
  return len;
}

void string_rev(char* x)
{
  int x_len = string_len(x);

  int b = 0;
  int e = x_len - 1; 

  char temp;
  for(; b < e; b++, e--) {
    temp = x[b];
    x[b] = x[e];
    x[e] = temp;
  }
}

int rev_add_bighum(char* a, int a_size, char* b, int b_size)
{
  int carry = 0;
  int i;
  for(i = 0; i < b_size; i++) {
    a[i] += b[i] + carry;

    carry = a[i] / 10;
    a[i] %= 10;
  }
  
  for(;carry != 0; i++) {
    a[i] += carry;

    carry = a[i] / 10;
    a[i] %= 10;
  }

  if(a_size < i)
    return i;
  else
    return a_size;
}

void string_purify(char* a)
{
  int a_len = string_len(a);
  int i;
  for (i = 0; i < a_len; ++i) {
    a[i] -= '0';
  }
}

void string_decoration(char* a, int a_size)
{
  int i;
  for (i = 0; i < a_size; ++i) {
    a[i] += '0';
  }
  a[a_size] = '\0';
}

void init_char_array(char* a, int a_size)
{
  int i;
  for (i = 0; i < a_size; ++i) {
    a[i] = 0;
  }
}

int string_eq(const char* a, const char* b)
{
  int a_len = string_len(a);
  int b_len = string_len(b);

  if (a_len != b_len)
    return -1;

  int i = 0;
  for (i = 0; i < a_len; ++i) {
    if (a[i] != b[i]) {
      return -1; 
    }
  }

  return 0;
}

void function_test()
{
  {
    assert(string_len("abc") == 3 && "string_len error");
    assert(string_len("a") == 1 && "string_len error");
    assert(string_len("") == 0 && "string_len error");
  }

  {
    char a[4];
    a[0] = 'a'; a[1] = 'b'; a[2] = 'c'; a[3] = '\0';

    string_rev(a);
    assert(string_eq("cba", a) == 0);

    char b[5] = {"abcd"};
    string_rev(b);
    assert(string_eq("dcba", b) == 0);
  }

  {
    char a[6];
    a[0] = 0; a[1] = 1; a[2] = 2; a[3] = 3; a[4] = 0; a[5] = 0;
    char b[5];
    b[0] = 0; b[1] = 1; b[2] = 2; b[3] = 3; b[4] = 0;

    rev_add_bighum(a, 4, b, 4);

    assert(a[0] == 0);
    assert(a[1] == 2);
    assert(a[2] == 4);
    assert(a[3] == 6);

    a[0] = 9; a[1] = 9; a[2] = 9; a[3] = 9; a[4] = 9; a[5] = 0;
    b[0] = 9; b[1] = 9; b[2] = 9; b[3] = 9; b[4] = 0;

    rev_add_bighum(a, 5, b, 4);

    assert(a[0] == 8);
    assert(a[1] == 9);
    assert(a[2] == 9);
    assert(a[3] == 9);
    assert(a[4] == 0);
    assert(a[5] == 1);
  }

  {
    char a[4] = "123";
    string_purify(a);
    assert(a[0] == 1);
    assert(a[1] == 2);
    assert(a[2] == 3);
    assert(a[3] == 0);
  }

  {
    char a[4];
    a[0] = 1; a[1] = 2;  a[2] = 3;  a[3] = '\0';
    string_decoration(a, 3);
    assert(string_eq("123", a) == 0);
  }
}


int main(int argc, char *argv[])
{
  function_test();

  char sum[N];
  int sum_len = 1;
  
  char ins[N];
  int ins_len = 0;

  init_char_array(sum, N);
  init_char_array(ins, N);

  while (scanf("%s", ins) != EOF) {
    // exit condition
    ins_len = string_len(ins);
    if(ins_len == 1 && ins[0] == '0') {
      break;
    }

    string_rev(ins);
    string_purify(ins);
    sum_len = rev_add_bighum(sum, sum_len, ins, ins_len);
  }

  string_decoration(sum, sum_len);
  string_rev(sum);
  printf("%s\n", sum);

  return 0;
}
