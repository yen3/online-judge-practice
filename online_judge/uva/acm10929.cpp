#include <iostream>
#include <string>
using namespace std;
int main(){
    string s;
    while(cin >> s){
        if(s=="0") break;
        int CalNumber=0;
        for(unsigned int i=0;i<s.length();++i){
            if(i%2) CalNumber+=s[i]-48;
            else CalNumber-=s[i]-48;
        }
        if(CalNumber%11==0) cout << s << " is a multiple of 11." << endl;
        else cout << s << " is not a multiple of 11." << endl;
    }
}
