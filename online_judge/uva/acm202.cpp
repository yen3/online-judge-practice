#include <iostream>
#include <vector>
using namespace std;
int main(){
    int numerator=0,denominator=0;
    while(cin >> numerator >> denominator){
        if(numerator==0 && denominator==0) break;
        int num=numerator,den=denominator;
        vector<int> div,quo;  /* div�Q���� quo�� */
        
        int cir_start=0;
        while(1){
            quo.push_back(num/den);
            div.push_back(num);
               
            num=(num%den)*10;
              
            if(div[div.size()-1]==0) break;
                
            for(unsigned int i=0;i<div.size()-1;i++){
                if(div[i]==div[div.size()-1]){
                    cir_start=i;
                    break;
                }
            }
            if(cir_start!=0) break;
        }
        
        /*output the answer*/
        cout << numerator << "/" << denominator << " = ";
        cout << quo[0] << ".";
        if(cir_start!=0){
            bool control=true;
            for(unsigned int i=1;i<quo.size()-1;i++){
                if(i==cir_start) cout << "(";
                cout << quo[i];
                
                if(i-cir_start+1==50){
                    cout << "...)";
                    control=false;
                    break;
                }
            }
            if(control) cout << ")";
            cout << endl;
            cout << "   " << quo.size()-cir_start-1 << " = number of digits in repeating cycle" << endl;
        }
        else{
            for(unsigned int i=1;i<quo.size()-1;i++) cout << quo[i];
            cout << "(0)" << endl;
            cout << "   1 = number of digits in repeating cycle" << endl;  
        }
        cout << endl;
    }
}
