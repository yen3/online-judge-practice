#include <iostream>
#include <string>
using namespace std;
int main(){
    string s;
    while(getline(cin,s)){
        int num=0;
        for(unsigned int i=0;i<s.length();i++){
            if(s[i]=='!' || s[i]=='\n') cout << endl;
            else if(s[i]>='0' && s[i]<='9') num+=(s[i]-'0');
            else if(s[i]=='b'){
                for(unsigned int j=0;j<num;j++) cout << ' ';
                num=0;
            }
            else{
                for(unsigned int j=0;j<num;j++) cout << s[i];
                num=0;
            }
        }
        cout << endl;
    }
}
