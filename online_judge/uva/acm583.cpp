#include <iostream>
#include <cmath>
#define N 6680

using namespace std;

long int p[N];
long int b;
unsigned long int a;

void zero(){
  b=a=0;
}
void make_prime_arrie(){
  long int sqrtp=0,k;
  long int ppoint=7,plusi=0;
      for(unsigned int i=0;i<N;i++) p[i]=0;
  p[0]=2; p[1]=3; p[2]=5; p[3]=7; 
      for(unsigned int i=4;i<N;i++){
            if(plusi%2==0) ppoint+=4;
            if(plusi%2==1) ppoint+=2;
        plusi++;
        p[i]=ppoint;
        sqrtp=int(sqrt(double(p[i])))+1;
              for(unsigned int j=2;p[j]<sqrtp;j++){
                            if(p[i]%p[j]==0){
                              p[i--]=0;
                              break;
                            }
              } 
      }  
}

int run(){
  unsigned int i=0;
    if(b==1){
       cout << "1 = 1" << endl;
       return 0;
    }
    if(b==-1){
      cout << "-1 = -1" << endl;
      return 0;  
    }
    cout << b << " = ";
    if(b<0){
      cout << "-1 x ";
      a=b*-1;
    }
    else a=b;
    while(a!=1){
        if(i==N && a!=1){
           cout << a;
           break;
        }
        if(a%p[i]==0){
           a/=p[i];
           cout << p[i];   
                     if(a!=1) cout << " x ";
           i=0;
           continue;
        }
        i++;
    }
  
cout << endl;
return 0;
}


int main(){
 make_prime_arrie();
 while(cin >> b){
   if(b==0) break;
   run();
   zero();
 }

return 0;
}
