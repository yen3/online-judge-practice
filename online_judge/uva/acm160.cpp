#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
using namespace std;

class prime{
	int make_prime_array(int);
 public:
	vector<int> p;
	void deside_size(int,bool);
};

void prime::deside_size(int end,bool d){
	if(d==0) end=int(sqrt(double(end)))+1;
	prime::make_prime_array(end);
}

int prime::make_prime_array(int n){
      long int point=7,plusi=0,sqrtp,control=0;
	  p.push_back(2);	  p.push_back(3);	  p.push_back(5);	  p.push_back(7);
	  int i=0;
      while(1){
	     if(plusi%2==0) point+=4;
		 if(plusi%2==1) point+=2;
		 if(point>n) break;
			sqrtp=int(sqrt(double(point)))+1;
		 for(unsigned int j=2;p[j]<sqrtp;j++){
			if(point%p[j]==0){
			  control=1;
			  i--;
			  break;
			}
		 }
		 if(control==0) p.push_back(point);
		 plusi++;
		 i++;
		 control=0;
	  }
   return 0;
}

int main(){
   prime a;
       a.deside_size(100,1);
   int times=0;
   while(cin >> times){     
     if(times==0) return 0;
     vector<unsigned int> m(a.p.size(),0);
     for(unsigned int i=2;i<=times;i++){
       int t=i,j=0;
         while(t>1){
            if(t%a.p[j]==0){
               t/=a.p[j];
               m[j]++;
               j=-1;
            }
          j++;
         } 
     }
     printf("%3d!=",times);
     for(unsigned int i=0;i<m.size();i++){ 
        if(m[i]!=0) printf("%3d",m[i]);
        if(i%14==0 && m[i+1]!=0 && i!=0 ) cout << endl <<"     ";
     }
     cout << endl;
     times=0;
   }
   return 0;
} 
