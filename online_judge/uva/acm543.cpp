#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int produce_prime_array(vector<int> &,const int);

int produce_prime_array(vector<int> &p,const int N){
    p.push_back(2);
    p.push_back(3);
    p.push_back(5);
    p.push_back(7);
    for(unsigned int i=0,n=p[3];n<=N;i++){
        if(i%2==0) n+=4;
        else n+=2;

        if(n>N) break;

        bool control = 1;
        for(unsigned int j=0;p[j]<static_cast<int>(sqrt(static_cast<double>(n)))+1;j++){
            if(n%p[j]==0){
                control=0;
                break;
            }
        }

        if(control) p.push_back(n);
    }
    return 0;
}

int main(){
    vector<int> p;
    produce_prime_array(p,65535);
    
    int n=0;
    
    while(cin >> n){
        if(n==0) break;
        for(unsigned int i=0;i<p.size();i++){
            int n1=p[i];
            int n2=n-p[i];
            
            int control=0;
            for(unsigned int j=0;p[j]<static_cast<int>(sqrt(n2))+1;j++){
                if(n2%p[j]==0){
                   control=1;
                   break;
                }
                if(n2%p[j]!=0 && j==p.size()-1) break;
            }
            
            if(control==1) continue;
            if(control==0){
               cout << n << " = " << n1 << " + " << n2 << endl;
               break;
            }
        }
    }    
}
