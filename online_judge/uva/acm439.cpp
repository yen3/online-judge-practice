#include <stdio.h>
#define N 10000
int a[N],b[N],move[8]={12,-12,8,-8,21,-21,19,-19},ran[88];
int i,j,ai=0,bi=0,end,times=0;
char ew[5];

int zero(){
  a[ai]= (ew[0]-'a'+1)*10 + ew[1]-'0'; 
  ai++;
  end = (ew[3]-'a'+1)*10 + ew[4]-'0';
	 for(i=0;i<88;i++) ran[i]=0;

return 0;
}

int run(){
  while(1){
	  for(i=0;i<ai;i++){
		for(j=0;j<8;j++){
		    b[bi]=a[i]+move[j];
			if(b[bi]%10<1 || b[bi]/10<1 || b[bi]%10 >8 || b[bi]/10 >8) continue;
			if(b[bi]==end)  return 0;
			if(ran[b[bi]]>0) continue;
		   ran[b[bi]]++;
		   bi++;
		}
	  }

	ai=0;
	times++;

	  for(i=0;i<bi;i++){
		for(j=0;j<8;j++){
		   a[ai]=b[i]+move[j];
		     if(a[ai]%10<1 || a[ai]/10<1 || a[ai]%10 >8 || a[ai]/10 >8) continue;
		     if(a[ai]==end)  return 0;
		     if(ran[a[ai]]>=1) continue;
		   ran[a[ai]]++;
		   ai++;
		}
	  }
	bi=0;
	times++;
  }

}

int output(){
 times++;
  if(ew[0]==ew[3] && ew[1]==ew[4]) times=0;
 printf("To get from %c%c to %c%c takes %d knight moves.\n",ew[0],ew[1],ew[3],ew[4],times);
return 0;
}

int main(){
	while(gets(ew)){
		zero();
		run();
		output();
        ai=bi=end=times=0;
		for(i=0;i<N;i++) a[i]=b[i]=0;
		for(i=0;i<88;i++) ran[i]=0;
	}
return 0;
}