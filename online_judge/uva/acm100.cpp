#include <iostream>
using namespace std;

int main(){
    int oa=0,ob=0;
    while(cin >> oa >> ob){
        int max_cycle=0,a=oa,b=ob;

        if(a>b){
            int temp=a;
            a=b; 
            b=temp;
        }

        for(unsigned int i=a;i<=b; i++){
             int temp_cycle=1;
             for(int n=i; n!=1; n=(n%2)?n*3+1:n/2, temp_cycle++)  ;
             max_cycle = (max_cycle>temp_cycle) ? max_cycle : temp_cycle;
        }
        cout << oa << " " << ob << " " << max_cycle << endl;
    }
}
