#include <iostream>
#include <vector>
using namespace std;
int main(){
    int testtime=1,n;
    while(cin >> n){
        if(n==0) break;
        int a,ad=0,times=0;
        vector<int> p;
        for(unsigned int i=0;i<n;i++){
            cin >> a;
            p.push_back(a);
        }
        for(unsigned int i=0;i<p.size();i++) ad+=p[i];
        ad/=p.size();
        for(unsigned int i=0;i<p.size();i++)
           if(p[i]>=ad)  times+=(p[i]-ad);
      
        cout << "Set #" << testtime++ << "\n" << "The minimum number of moves is " << times << "." << endl;
    }
}

