#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
using namespace std;

int main(){
    const int N=9;
    vector<int> u(N,0);
    while(cin >> u[0]){
        for(unsigned int i=1;i<u.size();i++) cin >> u[i];
          
        vector<char> symbol(u.size(),0);
        for(unsigned int i=0;i<u.size();i++){
            if(u[i]>=0)  symbol[i]='+';
            else symbol[i]='-';
            
            u[i]=abs(u[i]);
        }
        
        int p=0;
        for(; p<u.size()&& u[p]==0;p++);
        if(p==u.size()) cout << "0" << endl;
        else{
            int counter=0;
            for(unsigned int i=p;i<u.size();i++){
                if(u[i]!=0) counter++;
            }
            
            for(unsigned int i=p,j=counter;i<u.size();i++){
                if(u[i]!=0){
                    if(j==counter  && symbol[i]=='-') cout << symbol[i];
                    else if(j==counter && symbol[i]=='+') ;
                    else cout << " " << symbol[i] << " ";
                    j--;
                    
                    if(i==u.size()-1) cout << u[i];
                    else if(i==u.size()-2){
                        if(u[i]==1) cout << "x";
                        else cout << u[i] << "x";
                    } 
                    else{
                        if(u[i]==1) cout << "x^" << u.size()-i-1;
                        else cout << u[i] << "x^" << u.size()-i-1; 
                    }
                }
            }
        }
        cout << endl;
    }
}
