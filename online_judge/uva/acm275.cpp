#include <iostream>
#include <vector>
using namespace std;
int main(){
    int numerator=0,denominator=0;
    while(cin >> numerator >> denominator){
        if(numerator==0 && denominator==0) break;
        int num=numerator,den=denominator;
        vector<int> div,quo;  /* div�Q���� quo�� */
        
        int cir_start=0;
        while(1){
            quo.push_back(num/den);
            div.push_back(num);
               
            num=(num%den)*10;
              
            if(div[div.size()-1]==0) break;
                
            for(unsigned int i=0;i<div.size()-1;i++){
                if(div[i]==div[div.size()-1]){
                    cir_start=i;
                    break;
                }
            }
            if(cir_start!=0) break;
        }
        
        /* output the answer */
        if(quo[0]==0) cout << ".";
        else cout << quo[0] << ".";
        for(unsigned int i=1;i<quo.size()-1;i++){
            cout << quo[i];
            if((i+1)%50==0 && i!=quo.size()-1) cout << endl;    
        }
        cout << endl;
        if(cir_start==0) cout << "This expansion terminates." << endl;
        else cout << "The last " << div.size()-cir_start-1 <<" digits repeat forever." << endl;
        
        cout << endl;
    }
}
