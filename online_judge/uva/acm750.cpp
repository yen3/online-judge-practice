#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

void back_track(const int n,const int p,const int q,vector<vector<int> >& ans,
               vector<bool> &column,vector<bool> &rus,vector<bool> &lus
              ,vector<int>& queen);

int main(){
   int enter_size=0;
   cin >> enter_size;
   for(unsigned int i=0;i<enter_size;i++){
       int p=0,q=0;
       const int N=8;

       vector<int> queen;
       vector<vector<int> > ans;
       vector<bool> column,rus,lus;


       cin >> p >> q;

       for(unsigned int j=0;j<N;j++){
           column.push_back(true);
           queen.push_back(0);
       }

       for(unsigned int j=0;j<2*N+1;j++){
           rus.push_back(true);
           lus.push_back(true);
       }

       column[p-1]=rus[p+q-2]=lus[q-p+queen.size()-1]=false;
       queen[q-1]=p-1;

       back_track(0,p,q,ans,column,rus,lus,queen);

       cout << "SOLN       COLUMN" << endl;
       cout << " #      1 2 3 4 5 6 7 8" << endl;
       for(unsigned int j=0;j<ans.size();j++){
           cout << setw(2) << j+1 << "     ";
           for(unsigned int k=0;k<ans[j].size();k++){
               cout << setw(2) << ans[j][k]+1;
           }
           cout << endl;
       }
       cout << endl;
   }
}

void back_track(const int n,const int p,const int q,vector<vector<int> >& ans,
               vector<bool> &column,vector<bool> &rus,vector<bool> &lus
              ,vector<int>& queen){
   if(n>queen.size()-1){
       ans.push_back(queen);
   }
   else{
       if(n==q-1){
           back_track(n+1,p,q,ans,column,rus,lus,queen);
       }
       else{
           for(unsigned int i=0;i<queen.size();i++){
               if(column[i]==true && rus[n+i]==true &&
               lus[n-i+queen.size()-1]==true){
                   column[i]=rus[n+i]=lus[n-i+queen.size()-1]=false;
                   queen[n]=i;

                   back_track(n+1,p,q,ans,column,rus,lus,queen);

                   column[i]=rus[n+i]=lus[n-i+queen.size()-1]=true;
               }
           }
       }
   }
}
