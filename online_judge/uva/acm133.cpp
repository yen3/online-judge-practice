#include <iostream>
#include <iomanip>
#include <utility>
#include <vector>
using namespace std;

int main(){
    int n=0,l=0,r=0;
    while(cin >> n >> l >> r){
         if(n==0 && r==0 && l==0) break;
         vector<pair<int,bool> > p;
         for(unsigned int i=0;i<n;i++){
              pair<int,bool> temp(i+1,true);
              p.push_back(temp);
         }
         
         int timer=p.size(),lt=0,rt=p.size()-1;
         while(timer>1){
             int al=0,ar=0;
             if(timer>1){
                 int ltime=0;
                 while(ltime!=l){
                     if(p[lt].second==true){
                         ltime++;
                         if(ltime==l)    al=lt;
                     }
                     lt++;
                     if(lt>=p.size()) lt=0;
                 }
             }
             else break;
             
             if(timer>1){
                 int rtime=0;
                 while(rtime!=r){
                     if(p[rt].second==true){
                         rtime++;
                         if(rtime==r)    ar=rt;
                     }
                     rt--;
                     if(rt<0) rt=p.size()-1;
                 }
             }
             else break;
             
             cout << setw(3) << p[al].first;
             p[al].second=false;
             timer--;
             
             if(al==ar) cout << ",";
             
             if(al!=ar){
                 cout << setw(3) << p[ar].first;
                 p[ar].second=false;
                 timer--;
                 if(timer>1) cout << ","; 
             }
         }
         for(unsigned int i=0;i<p.size();i++){
             if(p[i].second==true){
                 cout << setw(3) << p[i].first;
                 break;
             }
         }
         cout << endl;
    }
}
