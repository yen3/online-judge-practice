#include <stdio.h>
#define N 35
char maze[N+2][N+2][N+2];
int a,b,c,fsp=0,tsp=0,qt=0,qf=0,times=0;
int i,j,k;
struct use{
   int x,y,z;
}sp[N*N*N*N];

int zero(){
  for(k=0;k<N+2;k++){
	for(j=0;j<N+2;j++){
		for(i=0;i<N+2;i++){
			maze[i][j][k]=0;
		}
	}
  }
return 0;
}

int input(){
  for(i=0;i<a;i++){
	for(j=0;j<b;j++){
		scanf("%s",maze[i][j]);
	}
  }
return 0;
}

int start(){
  for(i=0;i<a;i++){
	for(j=0;j<b;j++){
		for(k=0;k<c;k++){
			if(maze[i][j][k]=='S'){
				sp[fsp].x=k;
				sp[fsp].y=j;
				sp[fsp].z=i;
				return 0;
			}
		}
	}
  }
return 0;
}

int run(){
  i=fsp;
  j=tsp;
   while(1){
	if(maze[sp[i].z][sp[i].y][sp[i].x+1]=='E') return 0;
	if(maze[sp[i].z][sp[i].y][sp[i].x+1]=='.'){
	   maze[sp[i].z][sp[i].y][sp[i].x+1]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x+1;
	   sp[j].y=sp[i].y;
	   sp[j].z=sp[i].z;

	}
	if(maze[sp[i].z][sp[i].y][sp[i].x-1]=='E') return 0;
	if(maze[sp[i].z][sp[i].y][sp[i].x-1]=='.'){
	   maze[sp[i].z][sp[i].y][sp[i].x-1]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x-1;
	   sp[j].y=sp[i].y;
	   sp[j].z=sp[i].z;

	}
	if(maze[sp[i].z][sp[i].y+1][sp[i].x]=='E') return 0;
	if(maze[sp[i].z][sp[i].y+1][sp[i].x]=='.'){
	   maze[sp[i].z][sp[i].y+1][sp[i].x]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x;
	   sp[j].y=sp[i].y+1;
	   sp[j].z=sp[i].z;

	}
	if(maze[sp[i].z][sp[i].y-1][sp[i].x]=='E') return 0;
	if(maze[sp[i].z][sp[i].y-1][sp[i].x]=='.'){
	   maze[sp[i].z][sp[i].y-1][sp[i].x]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x;
	   sp[j].y=sp[i].y-1;
	   sp[j].z=sp[i].z;

	}
	if(maze[sp[i].z+1][sp[i].y][sp[i].x]=='E') return 0;
	if(maze[sp[i].z+1][sp[i].y][sp[i].x]=='.'){
	   maze[sp[i].z+1][sp[i].y][sp[i].x]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x;
	   sp[j].y=sp[i].y;
	   sp[j].z=sp[i].z+1;

	}
	if(maze[sp[i].z-1][sp[i].y][sp[i].x]=='E') return 0;
	if(maze[sp[i].z-1][sp[i].y][sp[i].x]=='.'){
	   maze[sp[i].z-1][sp[i].y][sp[i].x]='R';
	   j++;   qt++;
	   sp[j].x=sp[i].x;
	   sp[j].y=sp[i].y;
	   sp[j].z=sp[i].z-1;

	}
	if(qt<=0 && qf==0){
		times=-1;
		return 0;
	}
	if(qf==0){
		qf=qt;
		qt=0;
		times++;
	}
	maze[sp[i].z][sp[i].y][sp[i].x]='R';
	i++;
	qf--;
   }

return 0;
}

int main(){
 zero();
 while(scanf("%d%d%d",&a,&b,&c)){
	 if(a==0 && b==0 && c==0) break; 
	 input();
	 start();
	 run();
	 times++;
		if(times!=0) printf("Escaped in %d minute(s).\n",times);
		else printf("Trapped!\n");
	 fsp=tsp=qt=qf=times=0;
 }
return 0;
}
