#include <iostream>
#include <vector>

class Solution {
 public:
  void coloring(int i, int j, std::vector<std::vector<char>>& grid) {
    if (!((i >= 0 && i < grid.size()) && (j >= 0 && j < grid[0].size())))
      return;

    if (grid[i][j] == '0')
      return;

    grid[i][j] = '0';
    coloring(i+1, j, grid);
    coloring(i-1, j, grid);
    coloring(i, j+1, grid);
    coloring(i, j-1, grid);
  }

  int numIslands(std::vector<std::vector<char>>& grid) {
    int islands = 0;

    for (int i = 0; i < grid.size(); ++i) {
      for (int j = 0; j < grid[i].size(); ++j) {
        if (grid[i][j] == '1') {
          islands++;
          coloring(i, j, grid);
        }
      }
    }

    return islands;
  }
};

int main() {

}
