#include <cassert>
#include <iostream>
#include <vector>

class Solution {
 public:
  bool canJump2(const std::vector<int>& nums) {
    if (nums.empty())
      return false;

    std::vector<bool> arrival(nums.size(), false);
    arrival.back() = true;

    for (int i = arrival.size() - 2; i >= 0; --i) {
      if (nums[i] == 0) continue;

      std::size_t stop_step = i + nums[i] + 1;
      for (std::size_t step = i + 1; step < arrival.size() && step < stop_step;
           ++step) {
        if (arrival[step] == true) {
          arrival[i] = true;
          break;
        }
      }
    }

    return arrival[0];
  }

  bool canJump(const std::vector<int>& nums) {
    if (nums.empty())
      return false;

    int max_index = 0;
    for (int i = 0; i < (int)nums.size(); ++i) {
      if (i > max_index || max_index >= (int)nums.size())
        break;

      max_index = std::max(max_index, i + nums[i]);
    }

    return max_index >= (int)nums.size() - 1;
  }
};

int main() {
  Solution s;
  assert(s.canJump({2,3,1,1,4}) == true);
  assert(s.canJump({3,2,1,0,4}) == false);
}
