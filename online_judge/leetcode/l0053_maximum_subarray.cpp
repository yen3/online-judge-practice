#include <cassert>
#include <cstdint>
#include <iostream>
#include <vector>
#include <algorithm>

class Solution {
 public:
  int findMaxCrossSubArray(const std::vector<int>& nums, uint32_t begin,
                           uint32_t end) {
    uint32_t mid = begin + (end - begin) / 2;
    int left_max_sum = INT32_MIN;
    int left_sum = 0;
    for (int i = mid; i >= 0; i--) {
      left_sum += nums[i];
      if (left_sum > left_max_sum) {
        left_max_sum = left_sum;
      }
    }

    int right_max_sum = INT32_MIN;
    int right_sum = 0;
    for (std::size_t i = mid + 1; i <= end; ++i) {
      right_sum += nums[i];
      if (right_sum > right_max_sum) {
        right_max_sum = right_sum;
      }
    }
    return left_max_sum + right_max_sum;
  }

  int findMaxSubArray(const std::vector<int>& nums, uint32_t begin,
                      uint32_t end) {
    if (begin == end) {
      return nums[begin];
    }

    int cross_sum = findMaxCrossSubArray(nums, begin, end);

    uint32_t mid = begin + (end - begin) / 2;
    int left_sum = findMaxSubArray(nums, begin, mid);
    int right_sum = findMaxSubArray(nums, mid + 1, end);

    int max_sum = (left_sum > right_sum) ? left_sum : right_sum;
    max_sum = (max_sum > cross_sum) ? max_sum : cross_sum;

    return max_sum;
  }

  int isAllNegativeNums(std::vector<int>& nums) {
    int num = nums[0];
    for (std::size_t i = 0; i < nums.size(); ++i) {
      if (nums[i] > 0) {
        return 0;
      }
      num = std::max(nums[i], num);
    }
    return num;
  }

  int maxSubArray(std::vector<int>& nums) {
    if (nums.size() == 1) {
      return nums[0];
    }

    int negative_num = isAllNegativeNums(nums);
    if (negative_num < 0) {
      return negative_num;
    }

    int sum = 0;
    int max_sum = INT32_MIN;

    for (std::size_t i = 0; i < nums.size(); ++i) {
      sum = std::max(nums[i] + sum, 0);
      max_sum = std::max(sum, max_sum);
    }

    return max_sum;
  }
};

int main() {
  Solution s;
  std::vector<int> nums {-2, 1, -3, 4, -1, 2, 1, -5, 4};
  assert(s.maxSubArray(nums) == 6);

  return 0;
}
