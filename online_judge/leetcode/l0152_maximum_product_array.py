#!/usr/bin/env python3


class Solution:
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            raise ValueError("Empty list")

        max_products = [nums[0]]
        min_products = [nums[0]]

        for i, n in enumerate(nums[1:]):
            curr_max = max(max_products[-1] * n, min_products[-1] * n, n)
            curr_min = min(max_products[-1] * n, min_products[-1] * n, n)

            max_products.append(curr_max)
            min_products.append(curr_min)

        print(max_products + min_products)
        return max(*(max_products + min_products))


def main():
    s = Solution()
    # print(s.maxProduct([2, 3, -2, 4]))
    # assert s.maxProduct([2, 3, -2, 4]) == 6
    # assert s.maxProduct([-2, 0, -1]) == 0
    print(s.maxProduct([-4, -3, -2]))
    assert s.maxProduct([-4, -3, -2]) == 12


if __name__ == "__main__":
    main()
