#include <iostream>
#include <vector>
#include <cassert>

class Solution {
 public:
  int myMin(int a, int b) {
    if (a < b)
      return a;
    else
      return b;
  }

  int maxArea(const std::vector<int>& height) {
    if (height.size() < 2)
      return 0;

    int left = 0;
    int right = height.size() - 1;
    int max_area = 0;

    while (left < right) {
      int area = myMin(height[left], height[right]) * (right - left);

      if (area > max_area) {
        max_area = area;
      }

      if (height[left] < height[right]) {
        left++;
      } else {
        right--;
      }
    }

    return max_area;
  }
#if 0
  int maxArea(const std::vector<int>& height) {
    int max_area = myMin(height[0], height[1]);

    for (std::size_t i = 0; i < height.size() - 1; ++i) {
      for (std::size_t j = i + 1; j < height.size(); ++j) {
        int area = (j - i) * myMin(height[i], height[j]);
        if (area > max_area) {
          max_area = area;
        }
      }
    }

    return max_area;
  }
#endif
};

int main() {
  Solution s;
  assert(s.maxArea({1, 3, 5}) == 3);
}
