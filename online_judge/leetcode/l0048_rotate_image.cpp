#include <iostream>
#include <vector>
class Solution {
 public:
  void rotate(std::vector<std::vector<int>>& matrix) {
    // swap each row
    for (auto& row : matrix) {
      for (std::size_t i = 0; i < row.size() / 2; ++i) {
        std::swap(row[i], row[row.size() - 1 - i]);
      }
    }

    // swap triangle
    std::size_t m_idx_size = matrix.size() - 1;
    for (std::size_t i = 0; i < m_idx_size; ++i) {
      for (std::size_t j = 0; j < m_idx_size - i; ++j) {
        std::cout << i << " " << j << std::endl;
        std::swap(matrix[i][j], matrix[m_idx_size - j][m_idx_size - i]);
      }
    }
  }
};

int main() {
  Solution s;

  std::vector<std::vector<int>> matrix{ {1,2,3}, {4,5,6}, {7,8,9} };
  s.rotate(matrix);
}
