class Solution:
    def maxProfit(self, ps):
        """
        :type ps: List[int]
        :rtype: int
        """
        return sum([curr - prev
                    for prev, curr in zip(ps, ps[1:])
                    if curr > prev])


def main():
    pass


if __name__ == "__main__":
    main()
