#include <iostream>
#include <string>
#include <vector>
#include <cassert>

class Solution {
 public:
  bool isValid(std::string s) {
    std::vector<char> str_stack;
    for (std::size_t i = 0; i < s.size(); ++i) {
      if (s[i] == '(' || s[i] == '[' || s[i] == '{') {
        str_stack.push_back(s[i]);
      } else if (s[i] == ')' && str_stack.size() >= 1 && str_stack.back() == '(') {
        str_stack.pop_back();
      } else if (s[i] == ']' && str_stack.size() >= 1 && str_stack.back() == '[') {
        str_stack.pop_back();
      } else if (s[i] == '}' && str_stack.size() >= 1 && str_stack.back() == '{') {
        str_stack.pop_back();
      } else {
        return false;
      }
    }

    return str_stack.empty();
  }
};

int main() {
  Solution s;
  assert(s.isValid("") == true);
  assert(s.isValid("{}") == true);
  assert(s.isValid("{[][][]}") == true);
  assert(s.isValid("{{{{") == false);
  assert(s.isValid("]") == false);
}
