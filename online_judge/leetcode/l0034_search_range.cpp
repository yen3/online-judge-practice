#include <iostream>
#include <vector>

class Solution {
 public:
  int binary_search_shift(int n, const int* nums, int nums_size,
                          int shift = 0) {
    int begin = 0;
    int end = nums_size - 1;

    while (begin <= end) {
      int mid = (end - begin) / 2 + begin;
      int mid_real = (mid + shift) % nums_size;  // shift the index

      if (nums[mid_real] == n)
        return mid_real;
      else if (nums[mid_real] < n)
        begin = mid + 1;
      else
        end = mid - 1;
    }

    return -1;
  }

  int binary_search(int n, const std::vector<int>& nums) {
    return binary_search_shift(n, nums.data(), nums.size());
  }

  std::vector<int> searchRange(std::vector<int>& nums, int target) {
    int mid_index = binary_search(target, nums);

    if (mid_index == -1)
      return {-1, -1};

    int begin_index = mid_index;
    for (; begin_index >= 0 && nums[begin_index] == target; begin_index--) ;

    int end_index = mid_index;
    for (; end_index <= nums.size() - 1 && nums[end_index] == target; end_index++) ;

    return {begin_index+1, end_index-1};
  }
};

int main() {

}
