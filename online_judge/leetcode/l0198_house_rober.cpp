#include <iostream>
#include <vector>

class Solution {
 public:
#if 0
  void findRob(std::size_t index, int sum, int& max_sum,
               std::vector<int>& nums) {
    if (index >= nums.size()) {
      if (sum > max_sum) {
        max_sum = sum;
      }
      return;
    }

    findRob(index + 2, sum + nums[index], max_sum, nums);
    findRob(index + 1, sum, max_sum, nums);
  }

  int rob(std::vector<int>& nums) {
    int max_sum = 0;
    findRob(0, 0, max_sum, nums);

    return max_sum;
  }
#endif

  int rob(std::vector<int>& nums) {
    if (nums.empty()) return 0;
    if (nums.size() == 1) return nums[0];

    std::vector<int> robs{nums[0], std::max(nums[0], nums[1])};
    for (std::size_t i = 2; i < nums.size(); ++i) {
      robs.push_back(std::max(nums[i] + robs[i-2], robs[i-1]));
    }

    return robs.back();
  }
};

int main() {}
