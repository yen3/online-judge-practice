#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>

class Solution {
 public:
  std::vector<std::vector<int>> threeSum(std::vector<int>& nums) {
    if (nums.size() <= 2) {
      return {};
    }

    std::sort(nums.begin(), nums.end());

    const int target = 0;

    std::vector<std::vector<int>> ans;
    for (std::size_t i = 0; i < nums.size() - 2; ++i) {
      if (i > 0 && nums[i] == nums[i-1]) {
        continue;
      }

      std::size_t left = i + 1;
      std::size_t right = nums.size() - 1;

      while (left < right) {
        int sum = nums[i] + nums[left] + nums[right];
        if (sum < target) {
          left++;
        } else if (sum > target) {
          right--;
        } else {
          if (!(ans.size() >= 1 && ans.back()[0] == nums[i]
                && ans.back()[1] == nums[left]
                && ans.back()[2] == nums[right])) {
            ans.push_back({nums[i], nums[left], nums[right]});
          }

          left++;
          right--;
        }
      }
    }

    return ans;
  }
};

int main() {
  Solution s;
  {
    std::vector<int> nums{-1, 0, 1, 2, -1, -4};

    std::vector<std::vector<int>> ans = s.threeSum(nums);

    std::cout << "ans: " << std::endl;
    for (const auto& ls : ans) {
      for (auto i : ls) {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
  }
  {
    std::vector<int> nums{-2, 0, 1, 1, 2};

    std::vector<std::vector<int>> ans = s.threeSum(nums);

    std::cout << "ans: " << std::endl;
    for (const auto& ls : ans) {
      for (auto i : ls) {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
  }
}
