class Solution:
    def wordBreak(self, s, ws):
        """
        :type s: str
        :type ws: List[str]
        :rtype: bool
        """
        valids = [-1 for _ in range(len(s) + 1)]

        for sidx in range(0, len(s)):
            if sidx != 0 and valids[sidx] == -1:
                continue

            for w_idx, w in enumerate(ws):
                if s[sidx:sidx+len(w)] == w:
                    valids[sidx+len(w)] = w_idx

        return valids[-1] != -1


def main():
    s = Solution()
    assert s.wordBreak("leetcode", ["leet", "code"]) is True
    assert s.wordBreak("applepenapple", ["apple", "pen"]) is True
    assert s.wordBreak("nktrsgtwbuzsbptzahomgtgnaoma",
                       ['zsnpgbqh', 'bktrpgdwbu', 'qhuhzxfh',
                        'mxrgmga', 'omgtgnqomb', 'dffttrwlfh']) is False


if __name__ == "__main__":
    main()
