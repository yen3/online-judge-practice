#include <cassert>
#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  std::string convert(std::string s, int numRows) {
    std::vector<std::string> row_strings(numRows, "");

    std::vector<int> loc;
    for (int i = 0; i < numRows; ++i) loc.push_back(i);
    for (int i = numRows - 2; i > 0; --i) loc.push_back(i);

    for (std::size_t i = 0, l = 0; i < s.size();
         ++i, l = (l + 1) % loc.size()) {
      row_strings[loc[l]] += s[i];
    }

    std::string convert_str = "";
    for (std::size_t i = 0; i < row_strings.size(); ++i) {
      convert_str += row_strings[i];
    }

    return convert_str;
  }
};

int main() {
  Solution s;
  assert(s.convert("PAYPALISHIRING", 3) == "PAHNAPLSIIGYIR");
}
