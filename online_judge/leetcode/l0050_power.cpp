#include <iostream>

class Solution {
 public:
  double myPow(double x, int n) {
    if (n == 0)
      return 1;

    if (n == -2147483648)
      return myPow(x, -1) * myPow(x, -2147483647);

    bool negative_power = false;
    if (n < 0) {
      n *= -1;
      negative_power = true;
    }

    double p = 1;
    if (n <= 100) {
      for (int i = 0; i < n; ++i) {
          p *= x;
      }
    } else {
      int remain_power = n % 2;
      int half_power = n / 2;

      double half = myPow(x, half_power);
      p = half * half;
      if (remain_power == 1)
        p *= x;
    }

    return (negative_power == true)? (1/p): p;
  }
};

int main() {
  Solution s;
  std::cout << s.myPow(2, 4) << std::endl;
  std::cout << s.myPow(2, -4) << std::endl;
  std::cout << s.myPow(2, -2147483648) << std::endl;
  std::cout << s.myPow(1.0012, 1024) << std::endl;
}
