#include <cassert>
#include <iostream>
#include <vector>

class Solution {
 public:
  int uniquePathsWithObstacles(std::vector<std::vector<int>>& obstacleGrid) {
    std::vector<std::vector<int>> paths(
        obstacleGrid.size(), std::vector<int>(obstacleGrid[0].size(), 0));

    // Initial paths
    for (std::size_t i = 0, p = 1; i < paths[0].size(); ++i) {
      if (obstacleGrid[0][i] == 1) {
        p = 0;
      }
      paths[0][i] = p;
    }

    for (std::size_t i = 0, p = 1; i < paths.size(); ++i) {
      if (obstacleGrid[i][0] == 1) {
        p = 0;
      }
      paths[i][0] = p;
    }

    // Construct all paths
    for (std::size_t i = 1; i < paths.size(); ++i) {
      for (std::size_t j = 1; j < paths[i].size(); ++j) {
        if (obstacleGrid[i][j] != 0) {
          continue;
        }

        paths[i][j] = paths[i - 1][j] + paths[i][j - 1];
      }
    }

    return paths[paths.size() - 1][paths[0].size() - 1];
  }
};

int main() {
  Solution s;

  std::vector<std::vector<int>> grids{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}};
  assert(s.uniquePathsWithObstacles(grids) == 2);
}
