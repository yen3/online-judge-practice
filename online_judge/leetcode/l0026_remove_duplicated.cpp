#include <iostream>
#include <cassert>
#include <vector>

class Solution {
 public:
  int removeDuplicates(std::vector<int>& nums) {
    if (nums.empty())
      return 0;

    int curr_num = nums[0];
    std::size_t count_size = 1;
    for (std::size_t i = 1; i < nums.size(); ++i) {
      if (curr_num != nums[i]) {
        curr_num = nums[i];
        nums[count_size++] = curr_num;
      }
    }

    while (nums.size() != count_size)
      nums.pop_back();

    return count_size;
  }
};

int main() {
  Solution s;

  {
    std::vector<int> xs{1, 1, 2};
    assert(s.removeDuplicates(xs) == 2);

    assert(xs[0] == 1);
    assert(xs[1] == 2);
  }

}
