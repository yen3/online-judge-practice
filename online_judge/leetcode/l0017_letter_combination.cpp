#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  std::vector<std::string> letterCombinations(std::string digits_str) {
    static const std::vector<std::string> digit_maps{
        " ", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    if (digits_str.empty()) {
      return {};
    }

    std::vector<std::string> cs;

    // Initial case
    for (std::size_t i = 0; i < digit_maps[digits_str[0] - '0'].size(); ++i) {
      cs.emplace_back(1, digit_maps[digits_str[0] - '0'][i]);
    }

    for (std::size_t i = 1; i < digits_str.size(); ++i) {
      const std::string& chars = digit_maps[digits_str[i] - '0'];
      if (chars.size() < 1) {
        continue;
      }

      for (std::size_t i = 0; i < cs.size(); i++) {
        cs[i] = cs[i] + chars[0];
      }

      uint32_t current_size = cs.size();
      for (std::size_t c = 1; c < chars.size(); ++c) {
        for (std::size_t i = 0; i < current_size; ++i) {
          cs.emplace_back(cs[i].substr(0, cs[i].size() - 1) + chars[c]);
        }
      }
    }

    return cs;
  }
};

int main() {
  Solution s;
  std::vector<std::string> strs = s.letterCombinations("23");
  for (const auto& ss : strs) std::cout << ss << std::endl;
}
