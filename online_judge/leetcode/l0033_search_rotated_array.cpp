#include <cassert>
#include <iostream>
#include <vector>

class Solution {
 public:
  int binary_search_shift(int n, const int* nums, int nums_size,
                          int shift = 0) {
    int begin = 0;
    int end = nums_size - 1;

    while (begin <= end) {
      int mid = (end - begin) / 2 + begin;
      int mid_real = (mid + shift) % nums_size;  // shift the index

      if (nums[mid_real] == n)
        return mid_real;
      else if (nums[mid_real] < n)
        begin = mid + 1;
      else
        end = mid - 1;
    }

    return -1;
  }

  int GetShiftFromRotatedSortedArray(int* nums, int nums_size) {
    int shift = 0;
    for (; shift + 1 < nums_size && nums[shift] < nums[shift + 1]; shift++)
      ;

    return (shift == nums_size - 1) ? 0 : (shift + 1);
  }

  int search(std::vector<int>& nums, int target) {
    int shift = GetShiftFromRotatedSortedArray(nums.data(), nums.size());

    return binary_search_shift(target, nums.data(), nums.size(), shift);
  }
};

int main() {
  std::vector<int> nums{4, 5, 6, 7, 0, 1, 2};

  Solution s;
  assert(s.search(nums, 4) == 0);
  assert(s.search(nums, 5) == 1);
  assert(s.search(nums, 6) == 2);
  assert(s.search(nums, 7) == 3);
  assert(s.search(nums, 0) == 4);
  assert(s.search(nums, 1) == 5);
  assert(s.search(nums, 2) == 6);
}
