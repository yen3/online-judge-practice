#include <cstdint>
#include <iostream>
#include <unordered_set>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

struct ListNode {
  explicit ListNode(int x) : val(x), next(NULL) {}
  int val;
  ListNode* next;
};

class Solution {
 public:
  ListNode* detectCycle(ListNode* head) {
    std::unordered_set<uint64_t> nodes;

    if (head == nullptr) return nullptr;

    for (ListNode* curr = head; curr != nullptr; curr = curr->next) {
      uint64_t addr = (uint64_t)curr;
      if (nodes.find(addr) != nodes.end()) return curr;

      nodes.insert(addr);
    }
    return nullptr;
  }
};

int main() {}
