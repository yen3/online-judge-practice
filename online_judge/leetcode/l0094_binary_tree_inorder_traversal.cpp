#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
#include <vector>

struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
 public:
  void inorderBST(TreeNode* root, std::vector<int>& vs) {
    if (root == nullptr) return;

    inorderBST(root->left, vs);
    vs.push_back(root->val);
    inorderBST(root->right, vs);
  }

  std::vector<int> inorderTraversal(TreeNode* root) {
    std::vector<int> vs;
    inorderBST(root, vs);

    return vs;
  }
};

int main() {}
