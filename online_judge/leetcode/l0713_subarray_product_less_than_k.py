class Solution:
    def numSubarrayProductLessThanK(self, xs, k):
        """
        :type xs: List[int]
        :type k: int
        :rtype: int
        """
        xb = 0
        xe = 0

        product = 1
        count = 0
        xb = 0
        for xe in range(len(xs)):
            product *= xs[xe]

            while xb <= xe and product >= k:
                product = product // xs[xb]
                xb += 1

            count += (xe - xb + 1)

        return count
