#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

class Solution {
 public:
  bool exist(std::vector<std::vector<int>>& ans, int num1, int num2, int num3,
             int num4) {
    for (std::size_t i = 0; i < ans.size(); ++i) {
      if (ans[i][0] == num1 && ans[i][1] == num2 && ans[i][2] == num3 &&
          ans[i][3] == num4) {
        return true;
      }
    }
    return false;
  }

  std::vector<std::vector<int>> fourSum(std::vector<int>& nums, int target) {
    if (nums.size() <= 3) {
      return {};
    }

    std::sort(nums.begin(), nums.end());

    std::vector<std::vector<int>> ans;
    for (std::size_t i = 0; i < nums.size() - 3; ++i) {
      for (std::size_t j = i + 1; j < nums.size() - 2; ++j) {
        std::size_t left = j + 1;
        std::size_t right = nums.size() - 1;

        while (left < right) {
          int sum = nums[i] + nums[j] + nums[left] + nums[right];
          if (sum < target) {
            left++;
          } else if (sum > target) {
            right--;
          } else {
            if (!exist(ans, nums[i], nums[j], nums[left], nums[right])) {
              ans.push_back({nums[i], nums[j], nums[left], nums[right]});
            }

            left++;
            right--;
          }
        }
      }
    }

    return ans;
  }
};

int main() {
  Solution s;
  {
    std::vector<int> nums{1, 0, -1, 0, -2, 2};

    std::vector<std::vector<int>> ans = s.fourSum(nums, 0);

    std::cout << "ans: " << std::endl;
    for (const auto& ls : ans) {
      for (auto i : ls) {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
  }
  {
    std::vector<int> nums{0, 0, 0, 0};

    std::vector<std::vector<int>> ans = s.fourSum(nums, 0);

    std::cout << "ans: " << std::endl;
    for (const auto& ls : ans) {
      for (auto i : ls) {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
  }
  {
    std::vector<int> nums{-5, -4, -3, -2, -1, 0, 0, 1, 2, 3, 4, 5};

    std::vector<std::vector<int>> ans = s.fourSum(nums, 0);

    std::cout << "ans: " << std::endl;
    for (const auto& ls : ans) {
      for (auto i : ls) {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
  }
}
