#include <algorithm>
#include <iostream>
#include <vector>

class Solution {
 public:
  void listPermute(bool* used_nums, const std::vector<int>& nums,
                   std::vector<int>& p, std::vector<std::vector<int>>& ps) {
    if (p.size() == nums.size()) {
      ps.emplace_back(p);
      return;
    }

    for (std::size_t i = 0; i < nums.size(); ++i) {
      if (used_nums[i] == true) {
        continue;
      }

      used_nums[i] = true;
      p.push_back(nums[i]);

      listPermute(used_nums, nums, p, ps);

      p.pop_back();
      used_nums[i] = false;
    }
  }
  std::vector<std::vector<int>> permute(std::vector<int>& nums) {
    std::vector<std::vector<int>> ps;
    std::vector<int> p;

    bool* used_nums = new bool[nums.size()];
    std::fill(used_nums, used_nums + nums.size(), false);

    listPermute(used_nums, nums, p, ps);

    delete[] used_nums;
    return ps;
  }
};

int main() {
  Solution s;

  std::vector<int> nums{1, 2, 3};
  std::vector<std::vector<int>> ps = s.permute(nums);

  for (const auto& p : ps) {
    for (auto n : p) {
      std::cout << n << " ";
    }
    std::cout << std::endl;
  }
}
