#include <iostream>

class Solution {
 public:
  double absolute(double x) {
    return (x >= 0)? x : -x;
  }

  int mySqrt(int x) {
    if (x <= 0)
      return 0;

    if (x == 1)
      return 1;

    double guess = x / 2;
    while (absolute(guess * guess - x) > 0.001) {
      guess = ((x/guess) + guess) / 2;
    }

    return (int)guess;
  }
};

int main() {
  Solution s;
  std::cout << s.mySqrt(4) << std::endl;
}
