#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  void listAll(int n, int left, int right, std::string& s, std::vector<std::string>& ans) {
    if (n == left && n == right) {
      ans.push_back(s);
      return;
    }

    if (left < n) {
      s.push_back('(');
      listAll(n, left+1, right, s, ans);
      s.pop_back();
    }

    if (right < left) {
      s.push_back(')');
      listAll(n, left, right+1, s, ans);
      s.pop_back();
    }
  }
  std::vector<std::string> generateParenthesis(int n) {
    std::vector<std::string> ans;
    std::string s;

    listAll(n, 0, 0, s, ans);

    return ans;
  }
};

int main() {
  Solution s;
  std::vector<std::string> ans;
  ans = s.generateParenthesis(3);

  for (const auto& s : ans)
    std::cout << s << std::endl;
}
