#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

class Solution {
 public:
  std::vector<std::vector<std::string>> groupAnagrams(
      std::vector<std::string>& strs) {
    std::unordered_map<std::string, std::vector<std::string>> str_sets;
    for (const auto& s : strs) {
      std::string key(s);
      std::sort(key.begin(), key.end());
      std::cout << key << std::endl;

      str_sets[key].emplace_back(s);
    }

    std::vector<std::vector<std::string>> groups;
    for (auto iter = str_sets.begin(); iter != str_sets.end(); ++iter) {
      groups.emplace_back(iter->second);
    }

    return groups;
  }
};

int main() {
  std::vector<std::string> strs{"eat", "tea", "tan", "ate", "nat", "bat"};
  Solution s;
  s.groupAnagrams(strs);
}
