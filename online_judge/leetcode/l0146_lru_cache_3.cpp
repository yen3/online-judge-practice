#include <iostream>
#include <list>
#include <memory>
#include <unordered_map>
#include <utility>

class LRUCache {
 public:
  typedef int Key;
  typedef int Value;

 public:
  LRUCache(int capacity) : max_capacity_(capacity), cache_(), key_time_() {}

  int get(Key key) {
    auto iter = cache_.find(key);
    if (iter == cache_.end()) return -1;

    touch(iter);

    return iter->second.first;
  }

  void put(Key key, Value value) {
    auto iter = cache_.find(key);

    if (iter != cache_.end()) {
      touch(iter);
    } else {
      if (cache_.size() == max_capacity_) {
        cache_.erase(key_time_.back());
        key_time_.pop_back();
      }
      key_time_.push_front(key);
    }

    cache_[key] = {value, key_time_.begin()};
  }

 private:
  typedef std::list<Key> TimeList;
  typedef std::pair<Value, TimeList::iterator> HashValue;
  typedef std::unordered_map<Key, HashValue> Cache;

  void touch(Cache::iterator iter) {
    key_time_.erase(iter->second.second);
    key_time_.push_front(iter->first);
    iter->second.second = key_time_.begin();
  }

#if 0
  TimeList::iterator touch(TimeList::iterator iter) {
    Key key = *iter;
    key_time_.erase(iter);

    return touch(key);
  }

  TimeList::iterator touch(Key key) {
    key_time_.push_front(key);
    return key_time_.begin();
  }
#endif

  const std::size_t max_capacity_;
  Cache cache_;
  TimeList key_time_;
};

int main() {
  std::unique_ptr<LRUCache> cache(new LRUCache(2));

  cache->put(1, 1);
  cache->put(2, 2);
  std::cout << cache->get(1) << std::endl;
  cache->put(3, 3);
  std::cout << cache->get(2) << std::endl;
  cache->put(4, 4);
  std::cout << cache->get(1) << std::endl;
  std::cout << cache->get(3) << std::endl;
  std::cout << cache->get(4) << std::endl;
}
