#include <iostream>
#include <vector>

class Solution {
 public:
  bool isSameCombination(const std::vector<int>& xs,
                         const std::vector<int>& ys) {
    if (xs.size() != ys.size()) return false;

    for (std::size_t i = 0; i < xs.size(); ++i) {
      if (xs[i] != ys[i]) {
        return false;
      }
    }
    return true;
  }

  void addAnswer(const std::vector<int>& combination,
                 std::vector<std::vector<int>>& cs) {
    for (std::size_t i = 0; i < cs.size(); ++i) {
      if (isSameCombination(combination, cs[i])) return;
    }
    cs.push_back(combination);
  }

  void findCombination(int target, std::size_t index,
                       const std::vector<int>& candidates,
                       std::vector<int>& combination,
                       std::vector<std::vector<int>>& cs) {
    if (target == 0) {
      addAnswer(combination, cs);
      return;
    }

    if (index == candidates.size() || candidates[index] > target) {
      return;
    }

    for (std::size_t j = index;
         j < candidates.size() && candidates[j] <= target; j++) {
      combination.push_back(candidates[j]);
      findCombination(target - candidates[j], j + 1, candidates,
                      combination, cs);
      combination.pop_back();
    }
  }

  std::vector<std::vector<int>> combinationSum2(std::vector<int>& candidates,
                                                int target) {
    std::vector<std::vector<int>> cs;
    std::vector<int> combination;

    std::sort(candidates.begin(), candidates.end());

    findCombination(target, 0, candidates, combination, cs);

    return cs;
  }
};

int main() {
  Solution s;
  std::vector<int> candidates{10, 1, 2, 7, 6, 1, 5};
  std::vector<std::vector<int>> cs = s.combinationSum2(candidates, 8);

  for (const auto& c : cs) {
    for (auto n : c) {
      std::cout << n << " ";
    }
    std::cout << std::endl;
  }
}
