#include <iostream>
#include <memory>
#include <unordered_map>

class LRUCache {
private:
  typedef int Key;

  struct Value {
    Value(int v = 0, int p = -1, int n = -1)
        : value(v), prev_key(p), next_key(n) {}

    int value;

    int prev_key;
    int next_key;
  };

public:
  LRUCache(int capacity):
    max_capacity(capacity), head_key(-1), tail_key(-1), cache()
  {
  }

  int get(int key);
  void put(int key, int value);

private:
  bool isFull() { return cache.size() >= max_capacity; }

  int getValue(int key);
  void addLastest(int key, int value);
  void removeByKey(int key);
  void removeOldest();

  const std::size_t max_capacity;

  int head_key;
  int tail_key;

  std::unordered_map<Key, Value> cache;
};

int LRUCache::getValue(int key) {
  std::unordered_map<Key, Value>::iterator iter = cache.find(key);

  if (iter != cache.end())
    return iter->second.value;
  else
    return -1;
}

void LRUCache::addLastest(int key, int value) {
  if (head_key != -1)
    cache[head_key].prev_key = key;

  cache[key] = Value(value, -1, head_key);
  head_key = key;

  if (tail_key == -1)
    tail_key = key;
}

void LRUCache::removeByKey(int key) {
  int curr_prev_key = cache[key].prev_key;
  int curr_next_key = cache[key].next_key;

  if (curr_prev_key != -1)
    cache[curr_prev_key].next_key = curr_next_key;

  if (curr_next_key != -1)
    cache[curr_next_key].prev_key = curr_prev_key;

  if (key == tail_key)
    tail_key = curr_prev_key;

  if (key == head_key)
    head_key = curr_next_key;

  cache.erase(key);
}

void LRUCache::removeOldest() {
  if (tail_key == -1)
    return;

  removeByKey(tail_key);
}

int LRUCache::get(int key) {
  int value = getValue(key);

  if (value == -1)
    return -1;

  removeByKey(key);
  addLastest(key, value);

  //std::cout << "h: " << head_key << " t:" << tail_key << std::endl;

  return value;
}

void LRUCache::put(int key, int value) {
  if (getValue(key) != -1)
    removeByKey(key);

  if (isFull())
    removeOldest();

  addLastest(key, value);

  //std::cout << "h: " << head_key << " t:" << tail_key << std::endl;
}

int main() {
  std::unique_ptr<LRUCache> cache(new LRUCache(2));

  cache->put(1, 1);
  cache->put(2, 2);
  std::cout << cache->get(1) << std::endl;
  cache->put(3, 3);
  std::cout << cache->get(2) << std::endl;
  cache->put(4, 4);
  std::cout << cache->get(1) << std::endl;
  std::cout << cache->get(3) << std::endl;
  std::cout << cache->get(4) << std::endl;
}
