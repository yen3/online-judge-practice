#include <iostream>
#include <vector>

class Solution {
 public:
  int rob(const std::vector<int>& nums) {
    if (nums.empty()) return 0;
    if (nums.size() == 1) return nums[0];

    return std::max(rob_range(nums, 0, nums.size() - 1),
                    rob_range(nums, 1, nums.size()));
  }

  int rob_range(const std::vector<int>& nums, int begin, int end) {
    if (end - begin <= 1) return nums[begin];

    std::vector<int> robs{nums[begin], std::max(nums[begin], nums[begin + 1])};
    for (int i = 2; i + begin < end; ++i) {
      std::cout << i << std::endl;
      robs.push_back(std::max(nums[i + begin] + robs[i - 2], robs[i - 1]));
    }
    for (auto r : robs)
      std::cout << r << " ";
    std::cout << std::endl;

    return robs.back();
  }
};

int main() {
  Solution s;
  std::cout << s.rob({1,7,9,2}) << std::endl;
}
