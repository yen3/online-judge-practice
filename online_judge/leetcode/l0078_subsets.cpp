#include <iostream>
#include <vector>

class Solution {
 public:
  void combination(std::size_t index, std::size_t k,
                   const std::vector<int>& nums,
                   std::vector<int> curr,
                   std::vector<std::vector<int>>& ps) {
    if (curr.size() == k) {
      ps.push_back(curr);
      return;
    }

    for (std::size_t i = index; i < nums.size(); ++i) {
      curr.push_back(nums[i]);
      combination(i + 1, k, nums, curr, ps);
      curr.pop_back();
    }
  }

  std::vector<std::vector<int>> subsets(const std::vector<int>& nums) {
    std::vector<std::vector<int>> ps;

    for (std::size_t i = 0; i <= nums.size(); ++i) {
      std::vector<int> curr;
      combination(0, i, nums, curr, ps);
    }

    return ps;
  }
};

int main() {
  Solution s;
  std::vector<std::vector<int>> ps;

  ps = s.subsets({1, 2, 3});
  for (const auto& p : ps) {
    for (auto n : p) {
      std::cout << n << " ";
    }
    std::cout << std::endl;
  }
}
