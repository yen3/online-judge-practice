#include <cassert>
#include <iostream>
#include <string>

class Solution {
 public:
  std::string myStripStr(const std::string& str) {
    std::size_t begin = 0;
    std::size_t end = str.size() - 1;

    for(; str[begin] == ' ' || str[begin] == '\t' ||
          str[begin] == '\r' || str[begin] == '\n'; ++begin) ;

    for(; str[end] == ' ' || str[end] == '\t' ||
          str[end] == '\r' || str[end] == '\n'; --end) ;

    if (begin > end) {
      return "";
    } else {
      return str.substr(begin, end - begin + 1);
    }
  }

  std::string removeExtraSpace(const std::string& raw_s) {
    std::string s = myStripStr(raw_s);

    std::size_t copy_index = 0;
    for (std::size_t curr_index = 0; curr_index < s.size(); curr_index++) {
      if (s[curr_index] != ' ') {
        s[copy_index] = s[curr_index];
        copy_index++;
      } else {
        if (curr_index >= 1  && s[curr_index -1 ] == ' ') {
          continue;
        } else {
          s[copy_index] = s[curr_index];
          copy_index++;
        }
      }
    }

    while(s.size() != copy_index)
      s.pop_back();

    return s;
  }

  void reverseString(std::string& s, int begin, int end) {
    int str_len = end - begin;
    if (str_len <= 0)
      return;

    for (int i = 0; i < str_len / 2; ++i) {
      std::swap(s[i+begin], s[end-i-1]);
    }
  }

  void reverseWords(std::string &s) {
    reverseString(s, 0, s.size());
    int sub_str_begin = -1;
    int sub_str_end = -1;

    for (int i = 0; i < (int)s.size(); ++i) {
      if (s[i] != ' ' && sub_str_begin == -1)
        sub_str_begin = i;

      if (s[i] != ' ' && sub_str_begin != -1) {
        sub_str_end = i + 1;
      } else {
        reverseString(s, sub_str_begin, sub_str_end);
        sub_str_begin = -1;
        sub_str_end = -1;
      }
    }

    if (sub_str_begin != -1)
      reverseString(s, sub_str_begin, sub_str_end);

    s = removeExtraSpace(s);
  }
};

bool testStr(std::string input, const std::string& ans) {
  Solution s;
  s.reverseWords(input);
  return input == ans;
}


int main() {
  assert(testStr("the sky is blue", "blue is sky the") == true);
  assert(testStr("  the sky is blue", "blue is sky the") == true);
  assert(testStr("  the sky is blue  ", "blue is sky the") == true);
  assert(testStr("  the   sky is blue  ", "blue is sky the") == true);
  assert(testStr(" ", "") == true);
  assert(testStr("1 ", "1") == true);
  assert(testStr("   a   b ", "b a") == true);
}
