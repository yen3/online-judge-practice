#!/usr/bin/env python3

class Solution(object):
    def multiply(self, num1, num2):
        """
        :type num1: str
        :type num2: str
        :rtype: str
        """
        n1 = [ord(c) - ord('0') for c in num1[::-1]]
        n2 = [ord(c) - ord('0') for c in num2[::-1]]

        p = [0] * (len(n1) + len(n2))

        for xi, x in enumerate(n1):
            product = []
            carry = 0
            for y in n2:
                s = carry + x * y
                carry = s // 10
                s %= 10
                product.append(s)

            if carry != 0:
                product.append(carry)

            for pi, n in enumerate(product):
                index = xi + pi
                p[index] += n

                if p[index] >= 10:
                    p[index + 1] += p[index] // 10;
                    p[index] %= 10

        # Remove trailing zero
        while len(p) >= 1 and p[-1] == 0:
            p.pop()

        p = p[::-1]
        if len(p) == 0:
            return "0"
        return "".join([chr(n + ord('0')) for n in p])


def main():
    s = Solution()
    assert s.multiply("123", "456") == "56088"
    assert s.multiply("999", "999") == "998001"
    assert s.multiply("0", "999") == "0"
    assert s.multiply("9", "9") == "81"


if __name__ == '__main__':
    main()
