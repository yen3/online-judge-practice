#include <iostream>
#include <cassert>

class Solution {
 public:
  bool isUgly(int num) {
    if (num <= 0) return false;

    for (; num % 2 == 0; num /= 2) ;
    for (; num % 3 == 0; num /= 3) ;
    for (; num % 5 == 0; num /= 5) ;

    return num == 1;
  }
};

int main() {
  Solution s;
  assert(s.isUgly(0) == false);
  assert(s.isUgly(-15) == false);
  assert(s.isUgly(2) == true);
  assert(s.isUgly(3) == true);
  assert(s.isUgly(5) == true);
  assert(s.isUgly(15) == true);
  assert(s.isUgly(20) == true);
  assert(s.isUgly(7) == false);
}
