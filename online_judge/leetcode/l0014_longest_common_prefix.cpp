#include <iostream>
#include <string>
#include <vector>
#include <cassert>

class Solution {
 public:
  std::string longestCommonPrefix(const std::vector<std::string>& strs) {
    std::string prefix;
    if (strs.empty()) {
        return "";
    }

    if (strs.size() == 1) {
      return strs[0];
    }

    for (std::size_t i = 0; i < strs[0].size(); ++i) {
      char p = strs[0][i];
      for (std::size_t j = 1; j < strs.size(); ++j) {
        if (i >= strs[j].size() || p != strs[j][i]) {
          return prefix;
        }
      }
      prefix += p;
    }

    return prefix;
  }
};

int main() {
	Solution s;
	assert(s.longestCommonPrefix({"aa", "ab"}) == "a");
	assert(s.longestCommonPrefix({"aa", "a"}) == "a");
}
