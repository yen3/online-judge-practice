#include <iostream>
#include <vector>

class Solution {
 public:
  void sortColors(std::vector<int>& nums) {
    int color[3] = {0};

    for (auto n : nums) {
      color[n]++;
    }

    for (int i = 0, nidx = 0; i < 3; ++i) {
      for (int c = 0; c < color[i]; ++c) {
        nums[nidx++] = i;
      }
    }
  }
};

int main() {

}
