#include <iostream>
#include <vector>
#include <cassert>

class Solution {
 public:
  int uniquePaths(int m, int n) {
    std::vector<std::vector<int>> paths(m, std::vector<int>(n, 0));

    // Init the table
    for (std::size_t i = 0; i < paths[0].size(); ++i) {
      paths[0][i] = 1;
    }
    for (std::size_t i = 0; i < paths.size(); ++i) {
      paths[i][0] = 1;
    }

    for (std::size_t i = 1; i < paths.size(); ++i) {
      for (std::size_t j = 1; j < paths[i].size(); ++j) {
        paths[i][j] = paths[i-1][j] + paths[i][j-1];
      }
    }

    return paths[m-1][n-1];
  }
};

int main() {
  Solution s;
  assert(s.uniquePaths(3, 3) == 6);
  assert(s.uniquePaths(4, 4) == 20);
}
