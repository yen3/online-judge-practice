#include <iostream>
#include <list>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

class LRUCache;
class LeveledLRUCache;

class LRUCache {
 private:
  friend class LeveledLRUCache;

 public:
  typedef int Key;
  typedef int Value;

 public:
  LRUCache(int capacity)
      : max_capacity_(capacity), cache_(), key_time_(), next_lru_(nullptr) {}

  void setNextCahce(LRUCache* next_cache) { next_lru_ = next_cache; }

  Value get(Key key) {
    auto iter = cache_.find(key);
    if (iter == cache_.end()) {
      return -1;
    }

    touch(iter);

    return iter->second.first;
  }

  void put(Key key, Value value) {
    auto iter = cache_.find(key);

    if (iter != cache_.end()) {
      touch(iter);
    } else {
      if (cache_.size() == max_capacity_) {
        Key old_key = key_time_.back();
        Value old_value = cache_[key].first;

        cache_.erase(key_time_.back());
        key_time_.pop_back();

        if (next_lru_ != nullptr) {
          next_lru_->put(old_key, old_value);
        }
      }
      key_time_.push_front(key);
    }

    cache_[key] = {value, key_time_.begin()};
  }

 private:
  Value remove(Key key) {
    auto iter = cache_.find(key);

    if (iter == cache_.end()) return -1;

    Value value = iter->second.first;

    key_time_.erase(iter->second.second);
    cache_.erase(iter);

    return value;
  }

  bool isExist(Key key) { return cache_.find(key) != cache_.end(); }

  typedef std::list<Key> TimeList;
  typedef std::pair<Value, TimeList::iterator> HashValue;
  typedef std::unordered_map<Key, HashValue> Cache;

  void touch(Cache::iterator iter) {
    key_time_.erase(iter->second.second);
    key_time_.push_front(iter->first);
    iter->second.second = key_time_.begin();
  }

  const std::size_t max_capacity_;
  Cache cache_;
  TimeList key_time_;

  LRUCache* next_lru_;
};

class LeveledLRUCache {
 private:
  typedef LRUCache::Key Key;
  typedef LRUCache::Value Value;

 public:
  LeveledLRUCache();

  void addNewLevel(int capacity) {
    level_cache_.emplace_back(new LRUCache(capacity));

    if (level_cache_.size() >= 2) {
      level_cache_[level_cache_.size() - 2]->setNextCahce(
          level_cache_.back().get());
    }
  }

  Value get(Key key);

  void put(Key key, Value value);

  /// Query the cache of the key. The query does not effect the cache priority.
  int getKeyCacheLevel(Key key);

 private:
  std::vector<std::unique_ptr<LRUCache>> level_cache_;
};

LeveledLRUCache::Value LeveledLRUCache::get(Key key) {
  Value value = -1;

  for (auto& cache : level_cache_) {
    value = cache->get(key);
    if (value != -1) {
      cache->remove(key);
      break;
    }
  }

  if (value == -1) return value;

  level_cache_[0]->put(key, value);

  return value;
}

void LeveledLRUCache::put(Key key, Value value) {
  if (level_cache_.empty()) return;

  level_cache_[0]->put(key, value);
}

int LeveledLRUCache::getKeyCacheLevel(Key key) {
  for (std::size_t i = 0; i < level_cache_.size(); ++i) {
    if (level_cache_[i]->isExist(key)) {
      return i;
    }
  }

  return -1;
}

int main() {}
