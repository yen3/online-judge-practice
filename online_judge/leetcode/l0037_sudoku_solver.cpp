#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  std::vector<int> strToTable(const std::string& table_str) {
    std::vector<int> table(table_str.size());

    for (std::size_t i = 0; i < table.size(); ++i) {
      if (table_str[i] != '.')
        table[i] = table_str[i] - '0';
      else
        table[i] = 0;
    }

    return table;
  }

  std::string tableToStr(const std::vector<int>& table) {
    std::string table_str;
    for (std::size_t i = 0; i < table.size(); ++i) {
      if (table[i] == 0)
        table_str += '.';
      else
        table_str += static_cast<char>(table[i] + '0');
    }

    return table_str;
  }

  std::vector<int> boardToTable(const std::vector<std::vector<char>>& board) {
    std::vector<int> table(board.size() * board[0].size(), 0);

    std::size_t t = 0;
    for (std::size_t i = 0; i < board.size(); i++) {
      for (std::size_t j = 0; j < board[i].size(); j++) {
        if (board[i][j] == '.')
          table[t++] = 0;
        else
          table[t++] = board[i][j] - '0';
      }
    }

    return table;
  }

  std::vector<std::vector<char>> strToBoard(const std::string table_str) {
    std::vector<std::vector<char>> board(9, std::vector<char>(9, 0));

    std::size_t t = 0;
    for (std::size_t i = 0; i < board.size(); i++) {
      for (std::size_t j = 0; j < board[i].size(); j++) {
        board[i][j] = table_str[t++];
      }
    }

    return board;
  }

  std::vector<std::vector<char>> tableToBoard(const std::vector<int>& table) {
    std::vector<std::vector<char>> board(9, std::vector<char>(9, 0));

    std::size_t t = 0;
    for (std::size_t i = 0; i < board.size(); i++) {
      for (std::size_t j = 0; j < board[i].size(); j++) {
        if (table[t] == 0)
          board[i][j] = '.';
        else
          board[i][j] = table[t] + '0';

        t++;
      }
    }

    return board;
  }

  std::vector<int> possibleNumbers(std::size_t index,
                                   const std::vector<int>& table) {
    bool number_used[10];  // ignore 0;
    std::fill(number_used, number_used + 10, false);

    // Check row
    for (std::size_t i = index / 9 * 9; i < index / 9 * 9 + 9; ++i) {
      number_used[table[i]] = true;
    }

    // Check column
    for (std::size_t i = index % 9; i < table.size(); i += 9) {
      number_used[table[i]] = true;
    }

    // Check block
    std::size_t block_row = index / 9 / 3;
    std::size_t block_column = index % 9 / 3;
    for (std::size_t r = block_row * 9 * 3; r < (block_row + 1) * 9 * 3;
         r += 9) {
      for (std::size_t c = block_column * 3; c < (block_column + 1) * 3; ++c) {
        number_used[table[r + c]] = true;
      }
    }

    std::vector<int> nums;
    for (int i = 1; i < 10; ++i)
      if (number_used[i] == false) nums.push_back(i);

    return nums;
  }

  void findSudokuSolution(std::size_t index, std::vector<int>& table,
                          std::string& ans) {
    // We have already found a possible solution, ignore the rest solutions.
    if (ans.size() == table.size()) return;

    // Find the next empty field
    for (; index < table.size() && table[index] != 0; ++index) {
    }

    // Check the index, if the index is over the size. It means that an answer
    // is found.
    if (index >= table.size()) {
      ans = tableToStr(table);
      return;
    }

    std::vector<int> nums = possibleNumbers(index, table);

    // No solution for the trace
    if (nums.empty()) return;

    // Find all possible solutions
    for (std::size_t i = 0; i < nums.size(); ++i) {
      table[index] = nums[i];
      findSudokuSolution(index + 1, table, ans);
    }

    // Reset the field
    table[index] = 0;
  }

  void solveSudoku(std::vector<std::vector<char>>& board) {
    std::vector<int> table = boardToTable(board);

    std::string ans("");
    findSudokuSolution(0, table, ans);

    board = strToBoard(ans);
  }

#if 0
  std::string sudokuSolver(const std::string table_str) {
    std::vector<int> table = strToTable(table_str);
    std::string ans;
    findSudokuSolution(0, table, ans);

    return ans;
  }
#endif
};

int main() {
  Solution s;

  std::string board_str =
      "..9748...7.........2.1.9.....7...24..64.1.59..98..."
      "3.....8.3.2.........6...2759..";

  std::vector<std::vector<char>> board = s.strToBoard(board_str);
  s.solveSudoku(board);

  for (const auto& bs : board) {
    for (auto b : bs) {
      std::cout << b << " ";
    }
    std::cout << std::endl;
  }
}
