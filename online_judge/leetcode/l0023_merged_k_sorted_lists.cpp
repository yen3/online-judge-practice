#include <iostream>
#include <queue>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

struct ListNode {
  explicit ListNode(int x) : val(x), next(NULL) {}
  int val;
  ListNode* next;
};

class Solution {
 public:
  ListNode* mergeKLists(std::vector<ListNode*>& lists) {
    auto cmp = [](const ListNode* x, const ListNode* y) {
      return x->val > y->val;
    };

    std::priority_queue<ListNode*, std::vector<ListNode*>, decltype(cmp)>
        prior_lists(cmp);

    for (auto l : lists) {
      if (l != nullptr) {
        prior_lists.push(l);
      }
    }

    ListNode* first = new ListNode(0);
    ListNode* curr = first;
    while (!prior_lists.empty()) {
      ListNode* min_node = prior_lists.top();
      prior_lists.pop();

      curr->next = min_node;
      curr = curr->next;

      min_node = min_node->next;
      if (min_node != nullptr) {
        prior_lists.push(min_node);
      }
    }

    ListNode* new_head = first->next;
    delete first;
    return new_head;
  }
};

int main() {
  ListNode* node1 = new ListNode(1);
  {
    ListNode* node2 = new ListNode(3);
    ListNode* node3 = new ListNode(5);
    ListNode* node4 = new ListNode(7);

    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
  }

  ListNode* node5 = new ListNode(2);
  {
    ListNode* node2 = new ListNode(4);
    ListNode* node3 = new ListNode(6);
    ListNode* node4 = new ListNode(8);

    node5->next = node2;
    node2->next = node3;
    node3->next = node4;
  }

  std::vector<ListNode*> lists{node1, node5};
  Solution s;
  ListNode* new_head = s.mergeKLists(lists);

  for (ListNode* curr = new_head; curr != nullptr; curr = curr->next) {
    std::cout << curr->val << std::endl;
  }
  std::cout << std::endl;
}
