class Solution:
    def maxProfit(self, ps):
        """
        :type ps: List[int]
        :rtype: int
        """
        if not ps:
            return 0

        max_trans = 0
        min_stock = ps[0]

        for p in ps[1:]:
            if p < min_stock:
                min_stock = p
            else:
                max_trans = max(max_trans, p - min_stock)

        return max_trans


def main():
    s = Solution()
    print(s.maxProfit([7, 1, 5, 3, 6, 4]))
    print(s.maxProfit([7, 6, 4, 3, 1]))
    print(s.maxProfit([2, 1, 4]))
    print(s.maxProfit([1, 4, 2]))
    print(s.maxProfit([2, 4, 1]))


if __name__ == "__main__":
    main()
