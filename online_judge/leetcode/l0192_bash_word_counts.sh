#!/usr/bin/env bash

# Read from the file words.txt and output the word frequency list to stdout.
if [[ -n "$1" ]]; then
  readonly INPUT_FILE="$1"
else
  readonly INPUT_FILE="words.txt"
fi

declare -A word_count

for word in $(cat "${INPUT_FILE}"); do
  word_count[${word}]=$((${word_count[${word}]}+1))
done

for key in "${!word_count[@]}"; do
  echo "${key} ${word_count[$key]}"
done |
sort -rn -k2
