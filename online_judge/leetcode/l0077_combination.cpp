#include <iostream>
#include <vector>

class Solution {
 public:
  void list_combin(int index, int n, int k, std::vector<int>& curr,
                   std::vector<std::vector<int>>& cs) {

    if (curr.size() == (std::size_t)k) {
      cs.push_back(curr);
      return;
    }

    for (int i = index; i <= n; ++i) {
      curr.push_back(i);
      list_combin(i + 1, n, k, curr, cs);
      curr.pop_back();
    }
  }

  std::vector<std::vector<int>> combine(int n, int k) {
    std::vector<std::vector<int>> cs;

    std::vector<int> curr;
    list_combin(1, n, k, curr, cs);

    return cs;
  }
};

int main() {
  Solution s;
  std::vector<std::vector<int>> cs;
  cs = s.combine(4, 3);

  for (const auto& c : cs) {
    for (auto n : c) {
      std::cout << n << " ";
    }
    std::cout << std::endl;
  }

  return 0;
}
