#include <iostream>
#include <queue>
#include <vector>

class Solution {
 public:
  void initUglyNums(std::vector<int32_t>& ugly_nums) {
    if (ugly_nums.size() == 1690) {
      return;
    }

    std::priority_queue<int32_t, std::vector<int32_t>, std::greater<int32_t>>
        candiate_nums;
    candiate_nums.push(1);

    while (ugly_nums.size() <= 1690) {
      int n = candiate_nums.top();

      while (candiate_nums.size() >= 1 && candiate_nums.top() == n) {
        candiate_nums.pop();
      }

      ugly_nums.push_back(n);

      if ((uint64_t)n * 2 <= INT_MAX) candiate_nums.push(n * 2);
      if ((uint64_t)n * 3 <= INT_MAX) candiate_nums.push(n * 3);
      if ((uint64_t)n * 5 <= INT_MAX) candiate_nums.push(n * 5);
    }
  }

  int nthUglyNumber(int n) {
    static std::vector<int32_t> ugly_nums;
    initUglyNums(ugly_nums);

    return ugly_nums[n-1];
  }
};

int main() {
  Solution s;
}
