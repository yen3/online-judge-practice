#include <iostream>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

struct ListNode {
  explicit ListNode(int x) : val(x), next(NULL) {}
  int val;
  ListNode* next;
};

class Solution {
 public:
  ListNode* swapPairs(ListNode* head) {
    if (head == nullptr || head->next == nullptr) {
      return head;
    }

    ListNode* first = new ListNode(0);
    first->next = head;

    ListNode* prev = first;
    ListNode* one = first->next;
    ListNode* two = nullptr;
    if (one->next != nullptr)
      two = one->next;

    while (one != nullptr && two != nullptr) {
      one->next = two->next;
      two->next = one;
      prev->next = two;

      prev = one;
      one = one->next;
      if (one != nullptr)
        two = one->next;
    }

    ListNode* new_head = first->next;
    delete first;
    return new_head;
  }

  ListNode* swapPairs_prev(ListNode* head) {
    if (head->next == nullptr) return head;

    ListNode* prev = head;
    ListNode* curr = head;
    ListNode* curr_next = head->next;
    ListNode* new_head = head->next;

    int count = 0;
    while (curr != nullptr && curr_next != nullptr) {
      curr->next = curr_next->next;
      curr_next->next = curr;
      if (count != 0) {
        prev->next = curr_next;
      }

      count++;

      prev = curr;
      curr = curr->next;
      if (curr != nullptr) {
        curr_next = curr->next;
      }
    }

    return new_head;
  }
};

int main() {
  ListNode* node1 = new ListNode(1);
  ListNode* node2 = new ListNode(2);
  ListNode* node3 = new ListNode(3);
  ListNode* node4 = new ListNode(4);

  node1->next = node2;
  node2->next = node3;
  node3->next = node4;

  for (ListNode* curr = node1; curr != nullptr; curr = curr->next) {
    std::cout << curr->val << " ";
  }
  std::cout << std::endl;

  Solution s;
  ListNode* new_head = s.swapPairs(node1);

  for (ListNode* curr = new_head; curr != nullptr; curr = curr->next) {
    std::cout << curr->val << std::endl;
  }
  std::cout << std::endl;
}
