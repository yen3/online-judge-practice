#include <cassert>
#include <iostream>
#include <string>
#include <vector>

class Solution {
 public:
  int lengthOfLongestSubstring(std::string s) {
    std::vector<int> loc(256, -1);

    uint32_t max_length = 0;
    uint32_t curr_length = 0;

    for (std::size_t i = 0; i < s.size(); ++i) {
      if (loc[s[i]] != -1) {
        int new_begin = loc[s[i]];
        for (std::size_t l = 0; l < 256; l++) {
          if (loc[l] != -1 && loc[l] <= new_begin) {
            loc[l] = -1;
            curr_length--;
          }
        }
      }

      loc[s[i]] = i;
      curr_length++;

      if (curr_length > max_length) {
        max_length = curr_length;
      }
    }

    return max_length;
  }
};

int main(){
  Solution s;
  std::cout << s.lengthOfLongestSubstring("abcabcbb") << std::endl;
  assert(s.lengthOfLongestSubstring("abcabcbb") == 3);
  assert(s.lengthOfLongestSubstring("bbbbbb") == 1);
}
