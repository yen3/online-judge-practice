#include <iostream>
#include <cassert>
#include <string>
#include <cstdlib>

class Solution {
 public:
  std::string myStripStr(const std::string& str) {
    std::size_t begin = 0;
    std::size_t end = str.size() - 1;

    for(; str[begin] == ' ' || str[begin] == '\t' ||
          str[begin] == '\r' || str[begin] == '\n'; ++begin) ;

    for(; str[end] == ' ' || str[end] == '\t' ||
          str[end] == '\r' || str[end] == '\n'; --end) ;

    if (begin > end) {
      return "";
    } else {
      return str.substr(begin, end - begin + 1);
    }
  }

  int checkAnswer(int64_t num, bool negative) {
    if (negative == false) {
      if (num >= 2147483647)
        return 2147483647;
      else
        return num;
    } else {
      if (num >= 2147483648)
        return -2147483648;
      else
        return -num;
    }
  }

  int myAtoi(std::string str) {
    str = myStripStr(str);

    bool negative = false;
    if (str[0] == '-') {
      negative = true;
      str = str.substr(1, str.size() - 1);
    } else if (str[0] == '+') {
      negative = false;
      str = str.substr(1, str.size() - 1);
    }

    int64_t num = 0;
    for (std::size_t i = 0; i < str.size(); ++i) {
      if (!(str[i] >= '0' && str[i] <='9')) {
        return checkAnswer(num, negative);
      }
      num = num * 10 + (str[i] - '0');

      if (negative == true && num >= 2147483648)
        return -2147483648;
      else if (negative == false && num >= 2147483647)
        return 2147483647;
    }

    return checkAnswer(num, negative);
  }
};

int main() {
  Solution s;
  assert(s.myStripStr(std::string("")) == std::string(""));
  assert(s.myStripStr(std::string("2")) == std::string("2"));
  assert(s.myStripStr(std::string("  2  ")) == std::string("2"));
  assert(s.myStripStr(std::string("\t\r\n  2  \r\t\n ")) == std::string("2"));

  assert(s.myAtoi("+2") == 2);
  assert(s.myAtoi("-2") == -2);
  assert(s.myAtoi("22") == 22);
  assert(s.myAtoi("") == 0);
  assert(s.myAtoi("+-2") == 0);
  assert(s.myAtoi("-+1") == 0);
  assert(s.myAtoi("-00") == std::atoi("-00"));
  assert(s.myAtoi("001234") == std::atoi("001234"));
  assert(s.myAtoi("+001234") == std::atoi("+001234"));
  assert(s.myAtoi("\t\r\n    010    \t\r\n") ==
         std::atoi("\t\r\n    010    \t\r\n"));
  assert(s.myAtoi("\t\r    010    \t\r") == std::atoi("\t\r    010    \t\r"));
  assert(s.myAtoi("\t\r    010") == std::atoi("\t\r    010"));
  assert(s.myAtoi("\t    010") == std::atoi("\t    010"));
  assert(s.myAtoi("    010") == std::atoi("    010"));
  assert(s.myAtoi("  -0012a42") == std::atoi("  -0012a42"));

  assert(s.myAtoi("2147483648") == 2147483647);
  assert(s.myAtoi("-2147483648") == -2147483648);
  assert(s.myAtoi("-2147483649") == -2147483648);
  assert(s.myAtoi("9223372036854775809") == 2147483647);
}
