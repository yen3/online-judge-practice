#include <iostream>
#include <vector>

class Solution {
 public:
  std::vector<std::vector<int>> generate(int numRows) {
    std::vector<std::vector<int>> tri;
    for (int i = 1; i <= numRows; ++i) {
      tri.push_back(std::vector<int>(i, 0));
      tri.back()[0] = 1;
      tri.back().back() = 1;
    }

    for (std::size_t r = 2; r < tri.size(); ++r) {
      for (std::size_t c = 1; c < tri[r].size() - 1; ++c) {
        tri[r][c] = tri[r-1][c] + tri[r-1][c-1];
      }
    }

    return tri;
  }
};

int main() {

}
