#include <iostream>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

struct ListNode {
  explicit ListNode(int x) : val(x), next(NULL) {}
  int val;
  ListNode* next;
};

class Solution {
 public:
  ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    ListNode* start = nullptr;

    ListNode* p1 = l1;
    ListNode* p2 = l2;
    ListNode* curr = start;

    if (p1 != nullptr && p2 != nullptr) {
      if (p1->val < p2->val) {
        start = p1;
        p1 = p1->next;
      } else {
        start = p2;
        p2 = p2->next;
      }
    } else if (p1 != nullptr) {
      start = p1;
      p1 = p1->next;
    } else if (p2 != nullptr) {
      start = p2;
      p2 = p2->next;
    }
    curr = start;

    while(p1 != nullptr || p2 != nullptr) {
      if (p1 != nullptr && p2 != nullptr) {
        if (p1->val < p2->val) {
          curr->next = p1;
          p1 = p1->next;
        } else {
          curr->next = p2;
          p2 = p2->next;
        }
      } else if (p1 != nullptr) {
        curr->next = p1;
        p1 = p1->next;
      } else if (p2 != nullptr) {
        curr->next = p2;
        p2 = p2->next;
      }
      curr = curr->next;
    }

    return start;
  }
};

int main() {
  ListNode* node1 = new ListNode(1);
  {
    ListNode* node2 = new ListNode(3);
    ListNode* node3 = new ListNode(5);
    ListNode* node4 = new ListNode(7);

    node1->next = node2;
    node2->next = node3;
    node3->next = node4;
  }

  ListNode* node5 = new ListNode(2);
  {
    ListNode* node2 = new ListNode(4);
    ListNode* node3 = new ListNode(6);
    ListNode* node4 = new ListNode(8);

    node5->next = node2;
    node2->next = node3;
    node3->next = node4;
  }


  //for (ListNode* curr = node1; curr != nullptr; curr = curr->next) {
    //std::cout << curr->val << " ";
  //}
  //std::cout << std::endl;

  Solution s;
  ListNode* new_head = s.mergeTwoLists(node1, node5);

  for (ListNode* curr = new_head; curr != nullptr; curr = curr->next) {
    std::cout << curr->val << std::endl;
  }
  std::cout << std::endl;
}
