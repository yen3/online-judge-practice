#include <iostream>
#include <cassert>


class Solution {
 public:
  int climbStairs(int n) {
    if (n == 1)
      return 1;

    int first = 1;
    int second = 2;

    for (int i = 3; i <= n; i++) {
      int temp = first + second;
      first = second;
      second = temp;
    }

    return second;
  }
};


int main() {
  Solution s;

  assert(s.climbStairs(3) == 3);
  assert(s.climbStairs(4) == 5);
  assert(s.climbStairs(5) == 8);
  assert(s.climbStairs(6) == 13);
}
