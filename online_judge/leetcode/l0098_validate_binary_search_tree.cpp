#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
#include <vector>

struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
 public:
  void inorderBST(TreeNode *root, std::vector<int>& vs) {
    if (root == nullptr) return;

    inorderBST(root->left, vs);
    vs.push_back(root->val);
    inorderBST(root->right, vs);
  }

  bool isValidBST(TreeNode *root) {
    std::vector<int> vs;
    inorderBST(root, vs);

    if (vs.empty() || vs.size() == 1)  return true;

    for (std::size_t i = 0; i < vs.size() - 1; ++i) {
      if (vs[i] >= vs[i+1])
        return false;
    }

    return true;
  }
};

TreeNode *makeTree(const std::vector<int> &value) {
  if (value.empty()) return nullptr;

  std::vector<TreeNode *> nodes;

  for (auto v : value) {
    if (v == -1)
      nodes.push_back(nullptr);
    else
      nodes.push_back(new TreeNode(v));
  }

  for (std::size_t nidx = 0, cidx = 0; cidx < nodes.size(); nidx++) {
    if (++cidx < nodes.size()) nodes[nidx]->left = nodes[cidx];
    if (++cidx < nodes.size()) nodes[nidx]->right = nodes[cidx];
  }

  return nodes[0];
}

int main() {
  Solution s;

  assert(s.isValidBST(makeTree({})) == true);
  assert(s.isValidBST(makeTree({3, 1, 5, 0, 2, 4, 6})) == true);
  assert(s.isValidBST(makeTree({2147483647})) == true);
  assert(s.isValidBST(makeTree({-2147483648, -1, 2147483647})) == true);
  assert(s.isValidBST(makeTree({10, 5, 15, -1, -1, 6, 20})) == false);
  assert(s.isValidBST(makeTree({3, 1, 5, 0, 2, 4, 6, -1, -1, -1, 3})) == false);
}
