#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

class Solution {
 public:
  std::vector<int> twoSum(std::vector<int>& nums, int target) {
    int left = 0;
    int right = nums.size() - 1;

    std::vector<std::pair<int, int>> num_loc;
    for (std::size_t i = 0; i < nums.size(); ++i) {
      num_loc.emplace_back(std::make_pair(nums[i], i));
    }

    std::sort(num_loc.begin(), num_loc.end(),
              [](const std::pair<int, int>& x, const std::pair<int, int>& y) {
                return x.first < y.first;
              });

    while (left < right) {
      int sum = num_loc[left].first + num_loc[right].first;
      if (sum < target) {
        left++;
      } else if (sum > target) {
        right--;
      } else {
        return {num_loc[left].second, num_loc[right].second};
      }
    }
  }
};

int main() {
  Solution s;
  {
    std::vector<int> nums{3, 2, 4};
    std::vector<int> ans = s.twoSum(nums, 6);
    std::cout << ans[0] << " " << ans[1] << std::endl;
    assert(ans[0] == 1);
    assert(ans[1] == 2);
  }
  {
    std::vector<int> nums{2, 5, 5, 11};
    std::vector<int> ans = s.twoSum(nums, 10);
    std::cout << "here" << std::endl;
    std::cout << ans[0] << " " << ans[1] << std::endl;
    assert(ans[0] == 1);
    assert(ans[1] == 2);
  }
}
