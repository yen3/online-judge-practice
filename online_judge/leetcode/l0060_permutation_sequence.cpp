#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <cassert>

class Solution {
 public:
  int fact(int n) {
    if (n == 0)  {
      return 1;
    }

    int f = 1;
    for (int i = 1; i <= n; ++i) {
      f *= i;
    }
    return f;
  }

  std::string getPermutation(int n, int k) {
    std::string ans;
    std::vector<std::pair<int, bool>> nums;
    for (int i = 1; i <= n; i++) {
      nums.emplace_back(std::make_pair(i, false));
    }

    k--;
    for (int i = nums.size() - 1, fact_num = fact(i); i >= 0; --i) {
      std::size_t nth = k / fact_num + 1;
      std::size_t nth_loc = 0;
      k = k % fact_num;

      if (i != 0) fact_num /= i;

      for (std::size_t j = 0, nth_count = 0; j < nums.size(); ++j) {
        if (nums[j].second == false) {
          nth_count++;
        }

        if (nth_count == nth) {
          nth_loc = j;
          break;
        }
      }

      ans += static_cast<char>(nums[nth_loc].first + '0');
      nums[nth_loc].second = true;
    }

    return ans;
  }
};

int main() {
  Solution s;
  assert(s.getPermutation(3, 1) == "123");
  assert(s.getPermutation(3, 2) == "132");
  assert(s.getPermutation(3, 3) == "213");
  assert(s.getPermutation(3, 4) == "231");
  assert(s.getPermutation(3, 5) == "312");
  assert(s.getPermutation(3, 6) == "321");
  assert(s.getPermutation(4, 4) == "1342");
}
