#include <cassert>
#include <iostream>

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

struct ListNode {
  explicit ListNode(int x) : val(x), next(NULL) {}
  int val;
  ListNode* next;
};

class Solution {
 public:
  ListNode* removeNthFromEnd(ListNode* head, int n) {
    ListNode* first = new ListNode(0);
    first->next = head;

    ListNode* prev = first;
    ListNode* tail = first;
    for (int i = 0; i < n; ++i) tail = tail->next;

    while (tail->next != nullptr) {
      tail = tail->next;
      prev = prev->next;
    }

    ListNode* del_next = prev->next->next;
    delete prev->next;
    prev->next = del_next;

    ListNode* new_head = first->next;
    delete first;

    return new_head;
  }
};

int main() {
  ListNode* node1 = new ListNode(1);
  ListNode* node2 = new ListNode(2);
  ListNode* node3 = new ListNode(3);
  ListNode* node4 = new ListNode(4);
  ListNode* node5 = new ListNode(5);

  node1->next = node2;
  node2->next = node3;
  node3->next = node4;
  node4->next = node5;

  Solution s;
  ListNode* new_head = s.removeNthFromEnd(node1, 2);

  for (ListNode* curr = new_head; curr != nullptr; curr = curr->next) {
    std::cout << curr->val << std::endl;
  }
  std::cout << std::endl;
}
