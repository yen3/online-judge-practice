-- uglyNumber = 2 : (minimum $ concatMap $ map (\x->[x*2, x*3, x*5]) uglyNumber)

uglyNumber = 2 : 3 : 5 : (minimum $ concatMap (\x -> [x*2, x*3, x*5]) uglyNumber)
