struct Solution;

impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let mut cs: Vec<i32> = vec![-1; 256];
        let mut max_len = 0;
        let mut curr_len = 0;

        for (idx, c) in s.chars().enumerate() {
            let cidx = c as usize;
            if cs[cidx] != -1 {
                let remove_idx = cs[cidx];
                cs = cs
                    .into_iter()
                    .map(|x| if x < remove_idx { -1 } else { x })
                    .collect::<Vec<i32>>();

                curr_len = idx as i32 - cs[cidx];
            } else {
                curr_len += 1;
            }

            cs[cidx] = idx as i32;
            max_len = if curr_len > max_len {
                curr_len
            } else {
                max_len
            };
        }

        max_len = if curr_len > max_len {
            curr_len
        } else {
            max_len
        };
        max_len as i32
    }
}

mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    pub fn test_length_of_longest_substring() {
        assert_eq!(Solution::length_of_longest_substring("abba".to_string()), 2);
        assert_eq!(Solution::length_of_longest_substring("dvdf".to_string()), 3);
        assert_eq!(
            Solution::length_of_longest_substring("123123412345".to_string()),
            5
        );
        assert_eq!(
            Solution::length_of_longest_substring("111111".to_string()),
            1
        );
        assert_eq!(
            Solution::length_of_longest_substring("121212".to_string()),
            2
        );
        assert_eq!(
            Solution::length_of_longest_substring("123451234123121".to_string()),
            5
        );
        assert_eq!(Solution::length_of_longest_substring("".to_string()), 0);
    }
}

fn main() {
    tests::test_length_of_longest_substring()
}
