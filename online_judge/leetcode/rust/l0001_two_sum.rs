#[derive(Debug)]
struct Solution {}

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        let mut idx_nums = nums
            .iter()
            .enumerate()
            .map(|(i, &n)| (i as i32, n))
            .collect::<Vec<(i32, i32)>>();
        idx_nums.sort_by_key(|&(_, n)| n);

        for idx in 0..idx_nums.len() {
            let (fst, fst_val) = idx_nums[idx];
            let find_val = target - fst_val;

            for sidx in idx + 1..idx_nums.len() {
                let (snd, snd_val) = idx_nums[sidx];
                if snd_val == find_val {
                    return vec![fst, snd];
                }
            }
        }

        vec![]
    }
}

mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    pub fn test_two_num() {
        assert_eq!(Solution::two_sum(vec![1, 2, 3, 5], 7), vec![1, 3]);
        assert_eq!(Solution::two_sum(vec![3, 2, 4], 6), vec![1, 2]);
        assert_eq!(Solution::two_sum(vec![3, 3], 6), vec![0, 1]);
    }
}

fn main() {
    tests::test_two_nums();
}
