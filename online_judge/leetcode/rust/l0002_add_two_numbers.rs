// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    pub fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

pub struct Practice {}

impl Practice {
    pub fn new(val: i32) -> Option<Box<ListNode>> {
        Some(Box::new(ListNode::new(val)))
    }

    pub fn new_node(val: i32) -> (Option<Box<ListNode>>, i32) {
        let carry = if val > 10 { 1 } else { 0 };
        let node = Some(Box::new(ListNode::new(if val > 10 {
            val - 10
        } else {
            val
        })));

        (node, carry)
    }

    pub fn basic() {
        // try to add a node
        let head = Practice::new(0);
        if let Some(mut x) = head {
            x.next = Practice::new(5);
        }
    }

    pub fn basic3() {
        // Tranversal linked-list through x
        let mut head: Option<Box<ListNode>> = Practice::new(5);
        let mut x = &mut head;
        loop {
            match x {
                Some(n) => {
                    println!("{:?}", n);
                    x = &mut n.next;
                }
                None => {
                    println!("None");
                    break;
                }
            }
        }

        println!("{:?}", head);
    }

    pub fn basic3_1() {
        // Tranversal linked-list through x
        let head: Option<Box<ListNode>> = Practice::new(5);
        let mut x = &head;
        loop {
            match x {
                Some(n) => {
                    println!("{:?}", n);
                    x = &n.next;
                }
                None => {
                    println!("None");
                    break;
                }
            }
        }

        println!("{:?}", head);
    }

    pub fn basic4_add_nodes() {
        let mut head: Option<Box<ListNode>> = Practice::new(0);
        let mut x = &mut head;

        let nums = vec![5, 6, 7, 8];
        for num in &nums {
            println!("{}", num);
            match x {
                Some(n) => {
                    println!("{:?}", n);
                    n.next = Practice::new(*num);
                    x = &mut n.next;
                }
                None => {
                    println!("None");
                    break;
                }
            }
        }

        println!("{:?}", &head);
    }

    pub fn basic5_add_nodes_without_head_node() {
        let mut head: Option<Box<ListNode>> = None;
        let mut x = &mut head;

        let nums = vec![5, 6, 7, 8];
        for num in &nums {
            println!("{}", num);
            match x {
                Some(n) => {
                    println!("{:?}", n);
                    n.next = Practice::new(*num);
                    x = &mut n.next;
                }
                None => {
                    head = Practice::new(*num);
                    x = &mut head;
                }
            }
        }

        println!("{:?}", &head);
    }

    pub fn basic6_add_nodes_without_vector() {
        let mut head: Option<Box<ListNode>> = None;
        let mut x = &mut head;

        let nums = vec![];
        for num in &nums {
            println!("{}", num);
            match x {
                Some(n) => {
                    println!("{:?}", n);
                    n.next = Practice::new(*num);
                    x = &mut n.next;
                }
                None => {
                    head = Practice::new(*num);
                    x = &mut head;
                }
            }
        }

        match x {
            Some(n) => {
                n.next = Practice::new(9);
            }
            None => {
                head = Practice::new(9);
            }
        }

        println!("{:?}", &head);
    }

    pub fn concat_two_node() {
        let nums = vec![1, 2, 3];
        let node = create_list_nodes(&nums);
        println!("{:?}", node);
        print_list_nodes(&node);
    }
}

pub fn create_list_nodes(nums: &Vec<i32>) -> Option<Box<ListNode>> {
    let mut head: Option<Box<ListNode>> = None;
    let mut x = &mut head;

    for num in nums {
        x = match x {
            Some(n) => {
                n.next = Practice::new(*num);
                &mut n.next
            }
            None => {
                head = Practice::new(*num);
                &mut head
            }
        }
    }

    head
}

pub fn print_list_nodes(mut x: &Option<Box<ListNode>>) {
    loop {
        x = match x {
            Some(n) => {
                println!("{}", n.val);
                &n.next
            }
            None => {
                println!("End of list");
                break;
            }
        }
    }
}

pub struct Solution {}

impl Solution {
    pub fn add_two_numbers(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        fn add_carry_node(val: i32, carry_out: &mut i32) -> Option<Box<ListNode>> {
            let mut new_val = val + *carry_out;
            if new_val >= 10 {
                new_val = new_val - 10;
                *carry_out = 1;
            } else {
                *carry_out = 0;
            }

            Some(Box::new(ListNode::new(new_val)))
        }

        let mut head: Option<Box<ListNode>> = None;
        let mut curr_tail = &mut head;

        let mut curr_n1 = &l1;
        let mut curr_n2 = &l2;
        let mut carry_out: i32 = 0;
        loop {
            let new_node = match (curr_n1, curr_n2) {
                (Some(n1), Some(n2)) => {
                    curr_n1 = &n1.next;
                    curr_n2 = &n2.next;
                    add_carry_node(n1.val + n2.val, &mut carry_out)
                }
                (Some(n1), None) => {
                    curr_n1 = &n1.next;
                    curr_n2 = &None;
                    add_carry_node(n1.val, &mut carry_out)
                }
                (None, Some(n2)) => {
                    curr_n1 = &None;
                    curr_n2 = &n2.next;
                    add_carry_node(n2.val, &mut carry_out)
                }
                (None, None) => {
                    if carry_out != 0 {
                        add_carry_node(0, &mut carry_out)
                    } else {
                        None
                    }
                }
            };

            if let Some(n) = new_node {
                curr_tail = match curr_tail {
                    Some(tail) => {
                        tail.next = Some(n);
                        &mut tail.next
                    }
                    None => {
                        head = Some(n);
                        &mut head
                    }
                }
            } else {
                break;
            }
        }

        head
    }
}

mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    pub fn test_add_two_numbers() {
        assert_eq!(
            Solution::add_two_numbers(
                create_list_nodes(&vec![1, 2, 3]),
                create_list_nodes(&vec![4, 5, 6])
            ),
            create_list_nodes(&vec![5, 7, 9])
        );

        assert_eq!(
            Solution::add_two_numbers(
                create_list_nodes(&vec![1, 2, 3, 4]),
                create_list_nodes(&vec![4, 5, 6])
            ),
            create_list_nodes(&vec![5, 7, 9, 4])
        );

        assert_eq!(
            Solution::add_two_numbers(
                create_list_nodes(&vec![1, 2, 4]),
                create_list_nodes(&vec![4, 5, 6])
            ),
            create_list_nodes(&vec![5, 7, 0, 1])
        );
    }
}

fn main() {
    tests::test_add_two_numbers();
}
