struct Solution;

impl Solution {
    pub fn cal_area(left_height: i32, right_height: i32, width: usize) -> usize {
        let min_height = if left_height > right_height {
            right_height
        } else {
            left_height
        };

        min_height as usize * width
    }

    pub fn max_area(height: Vec<i32>) -> i32 {
        if height.len() < 2 {
            return 0;
        }

        let mut left: usize = 0;
        let mut right: usize = height.len() - 1;

        let mut max_area = 0;
        while left < right {
            let curr_area = Solution::cal_area(height[left], height[right], right - left);
            if curr_area > max_area {
                max_area = curr_area;
            }

            if height[left] < height[right] {
                left = left + 1;
            } else {
                right = right - 1;
            }
        }

        max_area as i32
    }
}
