impl Solution {
    pub fn remove_duplicates(xs: &mut Vec<i32>) -> usize {
        if xs.len() == 0 {
            return 0;
        }

        let mut new_len: usize = 1;
        for i in 1..xs.len() {
            if xs[i] != xs[new_len - 1] {
                xs[new_len] = xs[i];
                new_len += 1;
            }
        }

        new_len
    }
}
