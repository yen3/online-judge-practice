impl Solution {
    pub fn plus_one(digits: Vec<i32>) -> Vec<i32> {
        let mut rev_digits: Vec<i32> = Vec::new();

        let mut carry_out = 1;
        for x in digits.iter().rev() {
            let new_value = x + carry_out;
            carry_out = if new_value >= 10 { 1 } else { 0 };
            rev_digits.push(if new_value >= 10 { 0 } else { new_value });
        }

        if carry_out == 1 {
            rev_digits.push(carry_out);
        }

        rev_digits.iter().rev().map(|&x| x).collect::<Vec<i32>>()
    }
}
