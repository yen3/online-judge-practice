#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

class Solution {
 public:
  std::string numToStr(const int n) {
    std::ostringstream oss;
    oss << n;
    return oss.str();
  }

  std::string normalizeNum(const std::string& s) {
    for (std::size_t i = 0; i < s.size(); ++i) {
      if (s[i] != '0') return s;
    }

    return "0";
  }

  std::string largestNumber(const std::vector<int>& nums) {
    std::vector<std::string> str_nums;

    for (auto n : nums) {
      str_nums.emplace_back(numToStr(n));
    }

    std::sort(str_nums.begin(), str_nums.end(),
              [](const std::string& x, const std::string& y) {
                return x + y > y + x;
              });

    std::string largest_num;
    for (const auto& s : str_nums) {
      std::cout << s << " ";
      largest_num += s;
    }
    std::cout << std::endl;

    return normalizeNum(largest_num);
  }
};

int main() {
  Solution s;
  assert(s.largestNumber({824, 8241}) == "8248241");
  assert(s.largestNumber({3, 30, 34, 5, 9}) == "9534330");
  assert(s.largestNumber({9, 89, 8, 80}) == "989880");
  assert(s.largestNumber({0, 0}) == "0");
  assert(s.largestNumber({121, 12}) == "12121");
  assert(s.largestNumber({12, 121}) == "12121");
  // assert(s.largestNumber({824,938,1399,5607,6973,5703,9609,4398,8247}) ==
  // "9609 938 824 8247 6973 5703 5607 4398 1399");

  assert(s.largestNumber({824, 938, 1399, 5607, 6973, 5703, 9609, 4398,
                          8247}) == "9609938824824769735703560743981399");

  assert(s.largestNumber(
             {4704, 6306, 9385, 7536, 3462, 4798, 5422, 5529, 8070, 6241,
              9094, 7846, 663,  6221, 216,  6758, 8353, 3650, 3836, 8183,
              3516, 5909, 6744, 1548, 5712, 2281, 3664, 7100, 6698, 7321,
              4980, 8937, 3163, 5784, 3298, 9890, 1090, 7605, 1380, 1147,
              1495, 3699, 9448, 5208, 9456, 3846, 3567, 6856, 2000, 3575,
              7205, 2697, 5972, 7471, 1763, 1143, 1417, 6038, 2313, 6554,
              9026, 8107, 9827, 7982, 9685, 3905, 8939, 1048, 282,  7423,
              6327, 2970, 4453, 5460, 3399, 9533, 914,  3932, 192,  3084,
              6806, 273,  4283, 2060, 5682, 2,    2362, 4812, 7032, 810,
              2465, 6511, 213,  2362, 3021, 2745, 3636, 6265, 1518, 8398}) ==
         "989098279685953394569448938591490949026893989378398835381838108107807"
         "079827846760575367471742373217205710070326856680667586744669866365546"
         "511632763066265624162216038597259095784571256825529546054225208498048"
         "124798470444534283393239053846383636993664365036363575356735163462339"
         "932983163308430212970282274527326972465236223622313228122162132060200"
         "01921763154815181495141713801147114310901048");
}
