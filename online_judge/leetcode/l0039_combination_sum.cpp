#include <iostream>
#include <vector>
class Solution {
 public:
  bool isSameCombination(const std::vector<int>& xs,
                         const std::vector<int>& ys) {
    if (xs.size() != ys.size()) return false;

    for (std::size_t i = 0; i < xs.size(); ++i) {
      if (xs[i] != ys[i]) {
        return false;
      }
    }
    return true;
  }

  void addAnswer(std::vector<int> combination,
                 std::vector<std::vector<int>>& cs) {
    std::sort(combination.begin(), combination.end());
    for (std::size_t i = 0; i < cs.size(); ++i) {
      if (isSameCombination(combination, cs[i])) return;
    }
    cs.push_back(combination);
  }

  void findCombination(int target, const std::vector<int>& candidates,
                       std::vector<int>& combination,
                       std::vector<std::vector<int>>& cs) {
    if (target == 0) {
      addAnswer(combination, cs);
      return;
    }

    for (std::size_t c = 0; c < candidates.size(); ++c) {
      for (int i = 1; i * candidates[c] <= target; ++i) {
        for (int j = 0; j < i; j++) combination.push_back(candidates[c]);

        findCombination(target - i * candidates[c], candidates, combination,
                        cs);

        for (int j = 0; j < i; j++) combination.pop_back();
      }
    }
  }

  std::vector<std::vector<int>> combinationSum(std::vector<int>& candidates,
                                               int target) {
    std::vector<std::vector<int>> cs;
    std::vector<int> combination;

    findCombination(target, candidates, combination, cs);

    return cs;
  }
};

int main() {
  Solution s;
  std::vector<int> candidates{2, 3, 6, 7};
  std::vector<std::vector<int>> cs = s.combinationSum(candidates, 7);

  for (const auto& c : cs) {
    for (auto n : c) {
      std::cout << n << " ";
    }
    std::cout << std::endl;
  }
}
