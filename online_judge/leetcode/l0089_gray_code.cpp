#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

class Solution {
 public:
  std::vector<int> grayCode(int n) {
    std::vector<int> cs{0};
    for (int g = 0; g < n; g++) {
      std::size_t curr_size = cs.size();

      for (int i = cs.size() - 1; i >= 0; i--) {
        cs.push_back(cs[i]);
      }

      for (std::size_t i = curr_size; i < cs.size(); ++i) {
        cs[i] |= (1 << g);
      }
    }

    return cs;
  }
};

int main() {
  Solution s;

  std::vector<int> cs = s.grayCode(3);
  for (auto c : cs)
    std::cout << c << std::endl;

}
