#include <iostream>
#include <vector>
#include <cassert>

class Solution {
 public:
  bool searchMatrix(std::vector<std::vector<int>>& matrix, int target) {
    if (matrix.empty())
      return false;

    int column = matrix[0].size() - 1;
    std::size_t row = 0;
    while (column >= 0 && row < matrix.size()) {
      if (matrix[row][column] > target) {
        column--;
      }
      else if (matrix[row][column] < target) {
        row++;
      }
      else
        return true;
    }

    return false;
  }
};

int main() {
  Solution s;

  std::vector<std::vector<int>> matrix = {
    {1,   4,  7, 11, 15},
    {2,   5,  8, 12, 19},
    {3,   6,  9, 16, 22},
    {10, 13, 14, 17, 24},
    {18, 21, 23, 26, 30}
  };

  assert(s.searchMatrix(matrix, 5) == true);
}
