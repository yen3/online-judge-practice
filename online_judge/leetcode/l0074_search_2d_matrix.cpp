#include <cassert>
#include <iostream>
#include <vector>

class Solution {
 public:
  int getElement(std::size_t index,
                 const std::vector<std::vector<int>>& matrix) {
    return matrix[index / matrix[0].size()][index % matrix[0].size()];
  }

  bool searchMatrix(std::vector<std::vector<int>>& matrix, int target) {
    if (matrix.size() == 0)
      return false;

    int left = 0;
    int right = matrix.size() * matrix[0].size() - 1;

    while (left <= right) {
      std::size_t mid = (right - left) / 2 + left;

      int value = getElement(mid, matrix);
      if (value == target)
        return true;
      else if (value > target)
        right = mid - 1;
      else
        left = mid + 1;
    }
    return false;
  }
};

int main() {
  {
    std::vector<std::vector<int>> matrix{
        {1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 50}};

    Solution s;
    for (auto& row : matrix) {
      for (auto e : row) {
        assert(s.searchMatrix(matrix, e) == true);
      }
    }

    assert(s.searchMatrix(matrix, 101) == false);
    assert(s.searchMatrix(matrix, 8) == false);
    assert(s.searchMatrix(matrix, 0) == false);
    assert(s.searchMatrix(matrix, 18) == false);
    assert(s.searchMatrix(matrix, 21) == false);
  }

  {
    std::vector<std::vector<int>> empty_matrix;
    assert(s.searchMatrix(empty_matrix, 0) == false);
  }

  return 0;
}
