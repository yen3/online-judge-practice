plusOne :: [Integer] -> [Integer]
plusOne xs = plus (reverse xs) [] 1
    where plus [] ys carry = if carry == 1 then (1:ys) else ys
          plus (x:xs) ys carry =
              if new >= 10 then
                  plus xs (0:ys) 1
              else plus xs (new:ys) 0
            where new = x + carry
