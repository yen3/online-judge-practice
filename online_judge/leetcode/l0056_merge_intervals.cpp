#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

/**
 *  Definition for an interval.
 *   struct Interval {
 *       int start;
 *       int end;
 *       Interval() : start(0), end(0) {}
 *       Interval(int s, int e) : start(s), end(e) {}
 *   };
 */

struct Interval {
  int start;
  int end;
  Interval() : start(0), end(0) {}
  Interval(int s, int e) : start(s), end(e) {}
};

class Solution {
 public:
  std::vector<Interval> merge(std::vector<Interval>& intervals) {
    if (intervals.size() <= 1) return intervals;

    std::sort(intervals.begin(), intervals.end(),
              [](const Interval& x, const Interval& y) {
                if (x.start == y.start) return x.end < y.end;
                return x.start < y.start;
              });

    std::vector<Interval> merged;
    for (std::size_t i = 0; i < intervals.size();) {
      Interval interval(intervals[i]);
      std::size_t j = i + 1;
      for (; j < intervals.size() && intervals[j].start <= interval.end; ++j) {
        if (intervals[j].end > interval.end) {
          interval.end = intervals[j].end;
        }
      }

      merged.push_back(interval);
      i = j;
    }

    return merged;
  }
};

bool IsSameIntervals(const std::vector<Interval>& xs,
                     const std::vector<Interval>& ys) {
  if (xs.size() != ys.size()) {
    return false;
  }

  for (std::size_t i = 0; i < xs.size(); ++i) {
    if (xs[i].start != ys[i].start) return false;
    if (xs[i].end != ys[i].end) return false;
  }
  return true;
}

int main() {
  Solution s;
  {
    std::vector<Interval> intervals{Interval(1, 3), Interval(2, 6),
                                    Interval(8, 10), Interval(15, 18)};

    std::vector<Interval> merged_intervals = s.merge(intervals);
    std::vector<Interval> ans{Interval(1, 6), Interval(8, 10),
                              Interval(15, 18)};

    assert(IsSameIntervals(merged_intervals, ans) == true);
  }
  {
    std::vector<Interval> intervals{Interval(1, 4), Interval(1, 5)};
    std::vector<Interval> merged_intervals = s.merge(intervals);
    std::vector<Interval> ans{Interval(1, 5)};

    assert(IsSameIntervals(merged_intervals, ans) == true);
  }
  {
    std::vector<Interval> intervals{Interval(1, 4), Interval(4, 5)};
    std::vector<Interval> merged_intervals = s.merge(intervals);
    std::vector<Interval> ans{Interval(1, 5)};

    assert(IsSameIntervals(merged_intervals, ans) == true);
  }
  {
    std::vector<Interval> intervals{Interval(1, 4), Interval(2, 3)};
    std::vector<Interval> merged_intervals = s.merge(intervals);
    std::vector<Interval> ans{Interval(1, 4)};

    assert(IsSameIntervals(merged_intervals, ans) == true);
  }
}
